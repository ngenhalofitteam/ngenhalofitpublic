#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <time.h>
#include <sys/time.h>
#include <sys/resource.h>
#include <sys/types.h>
#include <unistd.h>
#include <signal.h>

#include "allvars.h"
#include "proto.h"

//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

// generate the linear growth factors for the variational runs

//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

// Linear growth factor from solving exact
// evolution of DE eqn of motion

void DenGrowVar(double *Da, double a1, double a0,int ivar)
{
  double grow1, grow0, lna1, lna0;

  lna1=log(a1);
  lna0=log(a0);

  grow1=gsl_spline_eval(GrowVar_spls[ivar].spline, lna1, GrowVar_acc); 
  grow0=gsl_spline_eval(GrowVar_spls[ivar].spline, lna0, GrowVar_acc);
  
  grow1=exp(grow1);
  grow0=exp(grow0);
  
  *Da=(a1*grow1)/(grow0*a0) ;

}

//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

void gen_growthVar(void)
{
  int i, ivar;
  double  aa, ainit, afinal;
  double  q1_0, q2_0, q1_1, q2_1, hstep;
  int nstep;
  
  // allocate memory for the array of splines
  if(!(GrowVar_spls=malloc(sizeof(struct GrowVar_splines)*(NVAR+1))))
    {
      printf("failed to allocate memory for `spls'.\n" );
      endrun(1);
    }

  for (ivar=0; ivar<NVAR+1; ivar++)
    {
      
      printf("Generating growth factors for variation:= %d\n",ivar);
      init_cospar_Daemmerung(ivar);

      // initialize varaibles    
      
  
      // initial and final expansion factors 
      ainit=0.001;
      afinal=1.0;
      
      // initial values 
      q1_0=ainit;
      q2_0=1.0;
      
      // number of time steps = NSTEP; this is in allvars.h
      nstep=NSTEP;

      // step size
      hstep=(afinal-ainit)/(nstep-1.0);
      
      for (i=0;i<nstep;i++)
	{

	  aa=ainit+i*hstep;

	  rk4(aa,q1_0,q2_0,hstep,&q1_1,&q2_1);
	  
	  xGrowVar[ivar][i]=aa;
	  yGrowVar[ivar][i]=q1_0;

	  q1_0=q1_1;
	  q2_0=q2_1;
	  
	}
      
      // divide growth solution by aa to get the growth supression factor
      for (i=0;i<nstep;i++)
	{
	  aa=xGrowVar[ivar][i];
	  xGrowVar[ivar][i]=log(aa);
	  yGrowVar[ivar][i]=log(yGrowVar[ivar][i]/aa);
	}
      
    }
  
  // create spline functions for all of the data
  GrowVar_acc = gsl_interp_accel_alloc();
 
  // generate the splines for the various cosmologies
  for (ivar=0; ivar<NVAR+1; ivar++)
    {
      GrowVar_spls[ivar].spline = gsl_spline_alloc(gsl_interp_cspline, NSTEP);
      gsl_spline_init(GrowVar_spls[ivar].spline, xGrowVar[ivar], yGrowVar[ivar], NSTEP);
    }
}

//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

void free_GrowVarSplines(void)
{

  int i;

  // free the memory for the linear power spectrum splines
  for (i=0;i<NVAR+1; i++)
    gsl_spline_free(GrowVar_spls[i].spline);    
  gsl_interp_accel_free(GrowVar_acc);  
  free(GrowVar_spls);

}

//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

// generate the linear growth factors for the target model

//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

// Linear growth factor from solving exact
// evolution of DE eqn of motion

void DenGrowTarget(double *Da, double a1, double a0)
{
  double grow1, grow0, lna1, lna0;

  lna1=log(a1);
  lna0=log(a0);

  grow1=gsl_spline_eval(GrowTarget_spline, lna1, GrowTarget_acc); 
  grow0=gsl_spline_eval(GrowTarget_spline, lna0, GrowTarget_acc); 
  
  grow1=exp(grow1);
  grow0=exp(grow0);
  
  *Da=(a1*grow1)/(grow0*a0) ;

}

//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

void gen_growthTarget(void)
{
  int i;
  double  aa, ainit, afinal;
  double  q1_0, q2_0, q1_1, q2_1, hstep;
  int nstep;
  
  // initial and final expansion factors 
  ainit=0.001;
  afinal=1.0;

  // initial values 
  q1_0=ainit;
  q2_0=1.0;

  // number of time steps = NSTEP; this is in allvars.h
  nstep=NSTEP;

  // step size
  hstep=(afinal-ainit)/(nstep-1.0);

  for (i=0;i<nstep;i++)
    {

      aa=ainit+i*hstep;

      rk4(aa,q1_0,q2_0,hstep,&q1_1,&q2_1);
	  
      xGrowTarget[i]=aa;
      yGrowTarget[i]=q1_0;

      q1_0=q1_1;
      q2_0=q2_1;

    }
    
  // divide growth solution by aa to get the growth supression factor
  for (i=0;i<nstep;i++)
    {
      aa=xGrowTarget[i];
      xGrowTarget[i]=log(aa);
      yGrowTarget[i]=log(yGrowTarget[i]/aa);
      
    }

  // create spline functions for all of the data
  GrowTarget_acc = gsl_interp_accel_alloc();

  GrowTarget_spline = gsl_spline_alloc(gsl_interp_cspline, nstep);
  gsl_spline_init(GrowTarget_spline, xGrowTarget, yGrowTarget, nstep);

}


//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

void free_GrowTarget_Spline(void)
{
  // free the memory for the linear power spectrum splines
  gsl_spline_free(GrowTarget_spline);
  gsl_interp_accel_free(GrowTarget_acc);  
}

//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

// Fourth order Runge-Kutta solution to 2nd Order Diff

//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

void rk4(double aa0,double q1_0,double q2_0,double hstep,
	    double *ystar_1,double *ystar_2)
{

  // derivatives
  double  k1_1,k2_1,k3_1,k4_1;
  double  k1_2,k2_2,k3_2,k4_2;

  // steps
  double  q1_1,q2_1,q1_2,q2_2;
  double  q1_3,q2_3,q1_4,q2_4;

  k1_1=func1(q1_0,q2_0,aa0);
  k1_2=func2(q1_0,q2_0,aa0);
  q1_1=q1_0+hstep/2.0*k1_1;
  q2_1=q2_0+hstep/2.0*k1_2;

  k2_1 = func1(q1_1,q2_1,aa0+hstep/2.0);    
  k2_2 = func2(q1_1,q2_1,aa0+hstep/2.0);    
  q1_2 = q1_0 + (hstep/2.0)*k2_1;
  q2_2 = q2_0 + (hstep/2.0)*k2_2;

  k3_1 = func1(q1_2,q2_2,aa0+hstep/2.0);
  k3_2 = func2(q1_2,q2_2,aa0+hstep/2.0);
  q1_3 = q1_0 + hstep*k3_1;
  q2_3 = q2_0 + hstep*k3_2;

  k4_1 = func1(q1_3,q2_3,aa0+hstep);
  k4_2 = func2(q1_3,q2_3,aa0+hstep);
  q1_4 = q1_0 + hstep*k4_1;
  q2_4 = q2_0 + hstep*k4_2;

  // rk4 estimate
  *ystar_1 = q1_0 + ((k1_1+2.0*k2_1+2.0*k3_1+k4_1)/6.0)*hstep;
  *ystar_2 = q2_0 + ((k1_2+2.0*k2_2+2.0*k3_2+k4_2)/6.0)*hstep;
}  

//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

double func1(double q1,double q2,double aa)
{
  double func1;
  func1=q2;
  return func1;
}

//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

double func2(double q1,double q2,double aa)
{
  double func2;
  func2=-Gamma1(aa)*q2-Gamma2(aa)*q1;
  return func2;
}

//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

double Gamma1(double aa)
{
  double Gamma1, xx, w;

  w=wEOS(aa);
  xx = xfunc(aa);
  Gamma1=3.0/(2.0*aa)*(1.0-w/(1.0+xx));
  return Gamma1;
}

//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

double Gamma2(double aa)
{
  double Gamma2, xx, w;

  w=wEOS(aa);
  xx = xfunc(aa);
  Gamma2=-3.0/(2.0*aa*aa)*(xx/(1.0+xx));
  return Gamma2;
}

//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

double xfunc(double aa)
{
  double xx;
  xx=cp.om_m0/(1.0-cp.om_m0)*1.0/DEfunc(aa);
  return xx;
}

//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

void CorrectGrowLargeScaleVar(void)
{
  FILE  *fout;
  
  int ivar, nvar, isnap, nsnapMAX, nsnapMIN, iPk;
  double aa1, aa2, Da1, Da2, growF12;
  double Pk1, Pk2, yyFac, nk, rk;
    
  char outfile[180];
  
  nvar = NVAR;
  nsnapMAX = NSNAPMAX;
  nsnapMIN = NSNAPMIN;
  
  for (ivar=0; ivar<=nvar;ivar++)
    {
      for (isnap=nsnapMIN;isnap<=nsnapMAX;isnap++)
	{

	  aa2=atime[ivar][isnap];	  
	  DenGrowVar(&Da2,aa2,1.0,ivar);

	  aa1=atime[ivar][nsnapMIN];	  
	  DenGrowVar(&Da1,aa1,1.0,ivar);
	  
	  growF12 = Da2/Da1;
	  //printf("blah %14.7e %14.7e %14.7e\n", Da2, Da1, Da2/Da1);
	  
	  yyFac=0;
	  nk=0;
	  for (iPk=0;iPk<nPkComp;iPk++)
	    {

	      rk=rkVecComp[iPk];

	      if (rk<0.04) 
		{
		  //determine correction factor
		  Pk2=PkVecVar[ivar][isnap][iPk];
		  Pk1=PkVecVar[ivar][nsnapMIN][iPk];
		  yyFac += (Pk1/Pk2)*growF12*growF12;		  
		  nk += 1.0;
		  //printf("zoo: %14.7e %14.7e %14.7e %14.7e %14.7e\n",rk,Pk2,Pk1,yyFac,nk);
		}
	    }
	  yyFac=yyFac/nk;
	  PkCorrFac[ivar][isnap]=yyFac;
	}
    }
  

#ifdef WRITEALLDATA
  // dump correction factors to file  
  sprintf(outfile,"%s/PkCorrFac.dat",All.OutputDir);
  if((fout = fopen(outfile,"w")))
    {
      fprintf(fout,"%3d  %3d  %3d\n",nvar,nsnapMIN,nsnapMAX);
      for (ivar = 0; ivar<=nvar; ivar++)
	for (isnap=nsnapMIN;isnap<=nsnapMAX;isnap++)
	  fprintf(fout,"%+14.7e  %14.7e\n",atime[ivar][isnap],PkCorrFac[ivar][isnap]);
    }
  else
    {
      printf("\noutfile not found:= %s.\n", outfile);
      endrun(212);	    
    }
  fclose(fout);
#endif

}


//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

void CorrectGrowLargeScaleBigBox(void)
{
  FILE  *fout;
  
  int ivar, nvar, isnap, nsnapMAX, nsnapMIN, iPk;
  double aa1, aa2, Da1, Da2, growF12;
  double Pk1, Pk2, yyFac, nk, rk;
    
  char outfile[180];
  
  nvar = NVAR;
  nsnapMAX = NSNAPMAX;
  nsnapMIN = NSNAPMIN;
  
  for (ivar=0; ivar<=0;ivar++)
    {
      for (isnap=nsnapMIN;isnap<=nsnapMAX;isnap++)
	{

	  aa2=atime_BigBox[ivar][isnap];	  
	  DenGrowVar(&Da2,aa2,1.0,ivar);

	  aa1=atime[ivar][nsnapMIN];	  
	  DenGrowVar(&Da1,aa1,1.0,ivar);
	  
	  growF12 = Da2/Da1;
	  //printf("blah %14.7e %14.7e %14.7e\n", Da2, Da1, Da2/Da1);
	  
	  yyFac=0;
	  nk=0;
	  for (iPk=0;iPk<nPkComp;iPk++)
	    {

	      rk=rkVecComp_BigBox[iPk];

	      if (rk<0.04) 
		{
		  //determine correction factor
		  Pk2=PkVecVar_BigBox[ivar][isnap][iPk];
		  Pk1=PkVecVar_BigBox[ivar][nsnapMIN][iPk];
		  yyFac += (Pk1/Pk2)*growF12*growF12;		  
		  nk += 1.0;
		  //printf("zoo: %14.7e %14.7e %14.7e %14.7e %14.7e\n",rk,Pk2,Pk1,yyFac,nk);
		}
	    }
	  yyFac=yyFac/nk;
	  PkCorrFac_BigBox[ivar][isnap]=yyFac;
	}
    }
  
#ifdef WRITEALLDATA
  // dump correction factors to file  
  sprintf(outfile,"%s/PkCorrFac_BigBox.2.dat",All.OutputDir);
  if((fout = fopen(outfile,"w")))
    {
      fprintf(fout,"%3d  %3d  %3d\n",nvar,nsnapMIN,nsnapMAX);
      for (ivar = 0; ivar<=0; ivar++)
	for (isnap=nsnapMIN;isnap<=nsnapMAX;isnap++)
	  fprintf(fout,"%+14.7e  %14.7e\n",atime_BigBox[ivar][isnap],PkCorrFac_BigBox[ivar][isnap]);
    }
  else
    {
      printf("\noutfile not found:= %s.\n", outfile);
      endrun(212);	    
    }
  fclose(fout);
#endif

}

//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

double CorrectPowGrowEvolveFac(double aaFid, double aaVar, int ivar)
{

  double DaFid, DaVar, growFac;

  // linear growth factor for the variational model at the 
  // expansion factor of the fiducial model 

  DenGrowVar(&DaFid,aaFid,1.0,ivar);
  
  // linear growth factor for the variational model at the 
  // expansion factor of the variational model
  DenGrowVar(&DaVar,aaVar,1.0,ivar);
  
  growFac = DaFid/DaVar;
  
  return growFac;
  
}

//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
