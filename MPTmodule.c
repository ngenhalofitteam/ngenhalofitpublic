#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <time.h>
#include <gsl/gsl_integration.h>

#include "allvars.h"
#include "proto.h"

// GAUSSIAN QUADRATURE   

#define WORKMAX 30000

// CUBA PARAMETERS

#define NDIM 5
#define NCOMP 1
#define USERDATA NULL
#define NVEC 1
#define EPSREL 1e-2
#define EPSABS 1e-12
#define VERBOSE 0
#define LAST 4
#define SEED 0
#define MINEVAL 0
#define MAXEVAL 110000000

#define NSTART 1000
#define NINCREASE 500
#define NBATCH 1000
#define GRIDNO 0
#define STATEFILE NULL
#define SPIN NULL

#define NNEW 1000
#define NMIN 2
#define FLATNESS 25.

#define KEY1 47
#define KEY2 1
#define KEY3 1
#define MAXPASS 5
#define BORDER 0.
#define MAXCHISQ 10.
#define MINDEVIATION .25
#define NGIVEN 0
#define LDXGIVEN NDIM
#define NEXTRA 0

#define KEY 0

//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

struct my_1loop_params 
{
  double ak;
  double rq;
} ;

//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

void init_MPT(void)
{

  int i, irk;
  double aa, zout, Damp, rk1, rk2, dellogrk;
  double Plin, Gprop, Gprop_fk, P1loop, P2loop;
  double Gprop_V2, Gprop_fk_V2;
  
  // set the expansion fator to compute
  aa=1.0;
      
  // redshift
  zout=1.0/aa-1.0;
  
  // generate the new values of omega for this model
  NewOmega(zout);
  
  // calculate the growth factor
  DenGrowTarget(&Damp,aa,1.0);
  pkTarget.Damp=Damp;
      
  // Compute BAO damping and nonlinear density power spectrum 
  rklow_MPT  = 1.0e-3;
  rkhigh_MPT = 10.0;
  get_velocity_dispersion();  // velocity dispersion at z=0.0

  // rk values to compute
  rk1 = 5.0e-3;
  rk2 = 0.3; //rk2 = 1.0/sigV;

  nMPT = 100;
  dellogrk= log(rk2/rk1)/(1.0*nMPT-1.0);
  
  for (i=0; i<nMPT ; i++)
    {
      
      rkMPT_SP[i] = log(rk1)+(i)*dellogrk;
      rk_MPT = exp(rkMPT_SP[i]);

      // max and min k-values for integrals
      rklow_MPT  = 1.0e-3;
      rkhigh_MPT = fmax(20.0*rk_MPT,M_PI);      
      
      // linear power spectrum
      Plin = PlinCDMTarget(rk_MPT);
      
      // get the propagator at z=0.0
      Gprop=get_propagator(rk_MPT);
      Gprop_fk=log(Gprop);

      // get the propagator V2 at z=0.0
      Gprop_V2=get_propagator_V2(rk_MPT);
      Gprop_fk_V2=log(Gprop_V2);

      // get the 1loop contribution at z=0.0
      P1loop=get_oneloop(rk_MPT);
            
      // get the 2loop MPT contribution at z=0.0
      P2loop=get_twoloop(rk_MPT);

      Gprop_fk_SP[i] = Gprop_fk;
      Gprop_fk_V2_SP[i] = Gprop_fk_V2;
      P1loop_SP[i] = log(P1loop);
      P2loop_SP[i] = log(P2loop);
      
      printf("i:= %3d, rk:= %14.7e, Gprop:= %14.7e, Gprop_V2:= %14.7e, P1loop:= %14.7e, P2loop:= %14.7e\n",
	     i, rk_MPT, Gprop, Gprop_V2, P1loop, P2loop);

    }

  //%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

  // spline the MPT functions

  if(!(MPT_spls=malloc(sizeof(struct MPT_splines)*(4))))
    {
      printf("failed to allocate memory for `spls'.\n" );
      endrun(1);
    }
  
  // create spline accelerations
  MPT_Gprop_acc  = gsl_interp_accel_alloc();
  MPT_Gprop_V2_acc  = gsl_interp_accel_alloc();
  MPT_P1loop_acc = gsl_interp_accel_alloc();
  MPT_P2loop_acc = gsl_interp_accel_alloc();
  
  // initialise the spline for the propagator
  MPT_spls[0].spline = gsl_spline_alloc(gsl_interp_cspline, nMPT);
  gsl_spline_init(MPT_spls[0].spline, rkMPT_SP, Gprop_fk_SP, nMPT);

  // initialise the spline for the propagator V2
  MPT_spls[3].spline = gsl_spline_alloc(gsl_interp_cspline, nMPT);
  gsl_spline_init(MPT_spls[3].spline, rkMPT_SP, Gprop_fk_V2_SP, nMPT);

  // initialise the spline for the P1loop
  MPT_spls[1].spline = gsl_spline_alloc(gsl_interp_cspline, nMPT);
  gsl_spline_init(MPT_spls[1].spline, rkMPT_SP, P1loop_SP, nMPT);

  // initialise the spline for the P2loop
  MPT_spls[2].spline = gsl_spline_alloc(gsl_interp_cspline, nMPT);
  gsl_spline_init(MPT_spls[2].spline, rkMPT_SP, P2loop_SP, nMPT);

}

//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

// get the Multi-Point Propagator Theory prediction at 2-loops

void gen_MPT_spectra(void)
{

  FILE *fout;

  char outfile[180];

  int ia, i;

  double rkMPTMIN, rkMPTMAX, aa, Plin;
  double Gprop, GpropSq, P1loop, P2loop, PkMPT, Pprop, Pprop_V2;
  double GpropTMP, P1loopTMP, P2loopTMP, PkMPTTMP, PpropTMP;
  double Gam0, Gam0_V2, Gam1, Gam2, zout, Damp, Gprop_fk, Gprop_fk_V2;

  double x1[NMPTMAX], y1[NMPTMAX], y2[NMPTMAX], y3[NMPTMAX];
  double y4[NMPTMAX], y5[NMPTMAX], y6[NMPTMAX];
  
  rkMPTMIN=0.01;
  rkMPTMAX=0.15;

  // set the expansion fators to compute
  for (ia=0; ia<naexpTarget ; ia++)
    {

      cp.aexp=xaexpTarget[ia];
      aa=cp.aexp;

      // redshift
      zout=1.0/aa-1.0;
      
      // generate the new values of omega for this model
      NewOmega(zout);
      
      // calculate the growth factor
      DenGrowTarget(&Damp,aa,1.0);
      pkTarget.Damp=Damp;

      for (i=0; i<nMPT; i++)
	{
	  
	  x1[i] = log(rkMPTMIN)+i*log(rkMPTMAX/rkMPTMIN)/(nMPT-1.0);
	  x1[i] = exp(x1[i]);

	  rk_MPT = x1[i];
	  
	  // linear power spectrum
	  Plin = PlinCDMTarget(rk_MPT);
	  
	  // max and min k-values for integrals
	  rklow_MPT  = 1.0e-3;
	  rkhigh_MPT = fmax(20.0*rk_MPT,M_PI);   

	  // Gprop fk
	  Gprop_fk = Gprop_fk_Fun(rk_MPT,aa);
	  Gam0 = Gprop_fk*Damp;
	  
	  // Gprop fk_V2
	  Gprop_fk_V2 = Gprop_fk_V2_Fun(rk_MPT,aa);
	  Gam0_V2 = Gprop_fk_V2*Damp;
	  
	  // get Pprop
	  Pprop = (Plin/Damp/Damp)*Gam0*Gam0;

	  // get Pprop_V2
	  Pprop_V2 = (Plin/Damp/Damp)*Gam0_V2*Gam0_V2; 	  
	  
	  // get the 1loop MPT contribution
	  P1loop=P1loopFun(rk_MPT,aa);
	  
	  // get the 2loop MPT contribution
	  P2loop=P2loopFun(rk_MPT,aa);
	  
	  // nonlinear MPT power spectrum

#ifdef MPTPROPAGATORCORR
	  PkMPT=Pprop_V2 + P1loop + P2loop;
#else
	  PkMPT=Pprop + P1loop + P2loop;
#endif	  

	  y1[i] = Plin;
	  y2[i] = Pprop;
	  y3[i] = P1loop;
	  y4[i] = P2loop;
	  y5[i] = PkMPT;
	  y6[i] = Pprop_V2;
 
	}
      
      //%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

#ifdef MPTOUT
      // Dump the MPT spectra to a file

      sprintf(outfile,"%s/%s.%d.dat",All.OutputDir,All.OutputMPTFileBase,ia);
      printf("%s\n",outfile);
      
      if((fout = fopen(outfile,"w")))
	{
	  fprintf(fout,"%d\t%14.7e\n",nMPT,aa);
	  for (i=0;i<nMPT;i++)
	    {
	      fprintf(fout,"%+14.7e\t%+14.7e\t%+14.7e\t%+14.7e\t%+14.7e\t%+14.7e\t%+14.7e\n",
		      x1[i],y1[i],y2[i],y3[i],y4[i],y5[i],y6[i]);
	    }
	}
      else
	{
	  printf("\noutfile not found:= %s.\n", outfile);
	  endrun(333);	    
	}
      fclose(fout);      
#endif
      
    }

}
  
//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

double Gprop_fk_Fun(double ak,double aa)
{

  double logrk, Gprop_fk, Damp;
  
  logrk=log(ak);

  // calculate the growth factor
  DenGrowTarget(&Damp,aa,1.0);

  // calculate the spline fit to the f(k) integral
  Gprop_fk = gsl_spline_eval(MPT_spls[0].spline, logrk, MPT_Gprop_acc);

  Gprop_fk = Damp*Damp*Gprop_fk;

  Gprop_fk = exp(Gprop_fk);  

  return Gprop_fk;

}


//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

double GpropFun(double ak,double aa)
{

  double logrk, Gprop_fk, Damp, Gam1, GpropFun;

  // calculate the growth factor
  DenGrowTarget(&Damp,aa,1.0);
  
  Gprop_fk = Gprop_fk_Fun(ak,aa);

  Gam1 = Damp*Gprop_fk;
  GpropFun = Gam1*Gam1;

  return GpropFun;

}


//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

double Gprop_fk_V2_Fun(double ak,double aa)
{

  double logrk, Gprop_fk_V2, Damp;
  
  logrk=log(ak);

  // calculate the growth factor
  DenGrowTarget(&Damp,aa,1.0);

  // calculate the spline fit to the f(k) integral
  Gprop_fk_V2 = gsl_spline_eval(MPT_spls[3].spline, logrk, MPT_Gprop_V2_acc);

  Gprop_fk_V2 = Damp*Damp*Gprop_fk_V2;

  Gprop_fk_V2 = exp(Gprop_fk_V2);  

  return Gprop_fk_V2;

}


//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

double GpropFun_V2(double ak,double aa)
{

  double logrk, Gprop_fk_V2, Damp, Gam1, GpropFun_V2;

  // calculate the growth factor
  DenGrowTarget(&Damp,aa,1.0);
  
  Gprop_fk_V2 = Gprop_fk_V2_Fun(ak,aa);

  Gam1 = Damp*Gprop_fk_V2;
  GpropFun_V2 = Gam1*Gam1;

  return GpropFun_V2;

}


//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

double P1loopFun(double ak,double aa)
{

  double logrk, P1loop, Damp, Gam2, Gprop_fk;
  
  logrk=log(ak);

  // calculate the growth factor
  DenGrowTarget(&Damp,aa,1.0);

  // calculate the spline fit to the f(k) integral
  P1loop = gsl_spline_eval(MPT_spls[1].spline, logrk, MPT_Gprop_acc);
  P1loop = exp(P1loop);
  
  // time evolution
  Gprop_fk = Gprop_fk_Fun(ak,aa);

  Gam2 = Damp*Damp*Gprop_fk;
  
  P1loop = P1loop*Gam2*Gam2;

  return P1loop;

}

//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

double P2loopFun(double ak,double aa)
{

  double logrk, P2loop, Damp, Gprop_fk, Gam3;
  
  logrk=log(ak);

  // calculate the growth factor
  DenGrowTarget(&Damp,aa,1.0);

  // calculate the spline fit to the f(k) integral
  P2loop = gsl_spline_eval(MPT_spls[2].spline, logrk, MPT_P2loop_acc);
  P2loop = exp(P2loop);

  // time evolution
  Gprop_fk = Gprop_fk_Fun(ak,aa);

  Gam3 = Damp*Damp*Damp*Gprop_fk;
  
  P2loop = P2loop*Gam3*Gam3;

  return P2loop;

}

//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

void get_velocity_dispersion(void)
{

  gsl_integration_workspace *w = gsl_integration_workspace_alloc(WORKMAX);

  double result, error, rkmin, rkmax;
  double expected, alpha;
  double adk, ak1, ak2, eps;
  
  gsl_function F;

  int ikey, nkey;

  alpha=1.0;
  
  // integral k-domain
  //rkmin=rklow_MPT;   // rklow, rkhigh are global variables
  //rkmax=rkhigh_MPT;

  F.function = &func;
  F.params = &alpha;  

  //gsl_integration_qags(&F, rkmin, rkmax, 1.0e-4, 1.0e-7, WORKMAX, w, &result, &error); 
  for (ikey=4;ikey<=4;ikey++)
    {
      gsl_integration_qag(&F,rklow_MPT,rkhigh_MPT,1.0e-4,1.0e-4,
			  WORKMAX, ikey , w, &result, &error);
    }
  
  sigV_MPT = 4.0*M_PI*result/3.0/pow(2.0*M_PI,3);
  sigV_MPT = sqrt(sigV_MPT);

  adk = 0.0050;
  ak1 = 0.0100;
  ak2 = 1.0/sigV_MPT;      
  eps = rklow_MPT;

  gsl_integration_workspace_free(w);

  printf("sigV and knl : %14.7e %14.7e\n", sigV_MPT, ak2);
  
}


//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

double func(double q, void *par)
{
  double alpha = *(double *) par;
  double ak = q;
  double func = PlinCDMTarget(ak);
  return func;
}


//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

double get_propagator(double ak)
{

  gsl_integration_workspace *w = gsl_integration_workspace_alloc(WORKMAX);

  double result, error, rkmin, rkmax;
  double expected, alpha;
  double Gprop, Plin, Pprop, dellogrk, rk1, rk2;

  int i, nbins, ikey;
  
  gsl_function G;
    
  G.function = &gfunc;

  alpha=ak;
  G.params = &alpha;  

  rkmin = rklow_MPT; // rklow, rkhigh are global variables
  rkmax = rkhigh_MPT;
  
  //      gsl_integration_qags(&G, rkmin, rkcut, 1.0e-5, 1.0e-5, WORKMAX, w, &result, &error);
  //printf("i:= %3d, rkcut:= %14.7e, result:= %14.7e\n",i,rkcut,result);
  
  for (ikey=4;ikey<=4;ikey++)
    {
      gsl_integration_qag(&G,rkmin,rkmax,1.0e-4,1.0e-4, WORKMAX, ikey , w, &result, &error);
      Gprop = exp(result);
    }

  gsl_integration_workspace_free(w);
  return Gprop;

}
  	  
//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

double gfunc(double q, void *par)
{
  double alpha = *(double *) par;
  double ak = alpha;
  double gfunc = af(q,ak);

  return gfunc;
}

//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

double af(double q,double ak)
{

  const double pi=M_PI;
  double af, Plin, yy, Pnl2012, Pq, Ph, dfac;
  
  // original function from MPTbreeze
  //af=(4.96031746031746d-4*(4.0*(6.0*k**7*q-7.9d1*k**5*q**
  //   &3+5.d1*k**3*q**5-2.1d1*k*q**7)+3.0*(k**2-q**2)**3*(2.0*
  //   &k**2+7.0*q**2)*log((k-q)**2/(k+q)**2)))/(k**3*q**5)     
  
  af=(4.96031746031746e-4*(4.0*(6.0*pow(ak,7.0)*q-7.9e1*pow(ak,5)*pow(q,3.0)
	  +5.e1*pow(ak,3.0)*pow(q,5.0)-2.1e1*ak*pow(q,7.0))+3.0*pow((ak*ak-q*q),3.0)
	  *(2.0*pow(ak,2.0)+7.0*q*q)
	  *log(pow((ak-q),2.0)/pow((ak+q),2.0))))/(pow(ak,3.0)*pow(q,5.0));

  // Use the Linear theory power spectrum
  Plin = PlinCDMTarget(q);

  af=af*Plin*4.0*pi*q*q/pow((2.0*pi),3.0);

  return af;
  
}

//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

double get_propagator_V2(double ak)
{

  gsl_integration_workspace *w = gsl_integration_workspace_alloc(WORKMAX);

  double result, error, rkmin, rkmax;
  double expected, alpha;
  double Gprop, Plin, Pprop, dellogrk, rk1, rk2;

  int i, nbins, ikey;
  
  gsl_function G;
    
  G.function = &gfunc_V2;

  alpha=ak;
  G.params = &alpha;  

  rkmin = rklow_MPT; // rklow, rkhigh are global variables
  //rkmax = rkhigh_MPT;
  //%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  rkmax = 1.0; // Something of a magic number!
  //%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  
  for (ikey=4;ikey<=4;ikey++)
    {
      gsl_integration_qag(&G,rkmin,rkmax,1.0e-4,1.0e-4, WORKMAX, ikey , w, &result, &error);
      Gprop = exp(result);
    }

  gsl_integration_workspace_free(w);
  return Gprop;

}

//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

double gfunc_V2(double q, void *par)
{
  double alpha = *(double *) par;
  double ak = alpha;
  double gfunc_V2 = af_V2(q,ak);

  return gfunc_V2;
}

//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

double af_V2(double q,double ak)
{

  const double pi=M_PI;
  double af_V2, Plin, yy, Pnl2012, Pq, Ph, dfac;
  
  // original function from MPTbreeze
  //af=(4.96031746031746d-4*(4.0*(6.0*k**7*q-7.9d1*k**5*q**
  //   &3+5.d1*k**3*q**5-2.1d1*k*q**7)+3.0*(k**2-q**2)**3*(2.0*
  //   &k**2+7.0*q**2)*log((k-q)**2/(k+q)**2)))/(k**3*q**5)     
  
  af_V2=(4.96031746031746e-4*(4.0*(6.0*pow(ak,7.0)*q-7.9e1*pow(ak,5)*pow(q,3.0)
	  +5.e1*pow(ak,3.0)*pow(q,5.0)-2.1e1*ak*pow(q,7.0))+3.0*pow((ak*ak-q*q),3.0)
	  *(2.0*pow(ak,2.0)+7.0*q*q)
	  *log(pow((ak-q),2.0)/pow((ak+q),2.0))))/(pow(ak,3.0)*pow(q,5.0));

  Plin = PlinCDMTarget(q);

  // use Halofit2012 for the power spectrum
  
  yy=q/(2.0*pi);
  dfac = 4.0*pi*yy*yy*yy;

  // get halofit2012 (Takahashi et al. 2012)
  halofit2012(q,Plin*dfac,&Pnl2012,&Pq,&Ph);
  Pnl2012=Pnl2012/dfac;
  af_V2=af_V2*Pnl2012*4.0*pi*q*q/pow((2.0*pi),3.0);

  return af_V2;
  
}			
		

//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

double get_oneloop(double ak)
{

  gsl_integration_workspace *w1 = gsl_integration_workspace_alloc(WORKMAX);

  double result, error, rkmin, rkmax, alpha, P1loop;

  int i, nbins, ikey;
  
  gsl_function P1loopA;
    
  P1loopA.function = &P1loopAfunc;
  P1loopA.params = &ak;  

  rkmin = rklow_MPT; // rklow, rkhigh are global variables
  rkmax = rkhigh_MPT;
  
  for (ikey=4;ikey<=4;ikey++)
    {
      gsl_integration_qag(&P1loopA,rkmin,rkmax,1.0e-4,1.0e-4, WORKMAX, ikey , w1, &result, &error);
      P1loop = 4.0*M_PI*result/pow((2.0*M_PI),3.0);
    }  

  gsl_integration_workspace_free(w1);

  return P1loop;

}

//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

double P1loopAfunc(double rq, void *par)
{

  gsl_integration_workspace *w2 = gsl_integration_workspace_alloc(WORKMAX);

  double result, error, alpha, P1loopA;

  struct my_1loop_params MYparams;

  double ak = *(double *) par;

  gsl_function P1loopB;

  int ikey;
    
  MYparams.ak=ak; 
  MYparams.rq=rq; 

  P1loopB.function = &P1loopBfunc;
  P1loopB.params = &MYparams;

  for (ikey=4;ikey<=4;ikey++)
    {
      gsl_integration_qag(&P1loopB,-1.0,1.0,1.0e-4,1.0e-4,
			  WORKMAX, ikey , w2, &result, &error);
    }  

  P1loopA = rq*rq*PlinCDMTarget(rq)*result;

  gsl_integration_workspace_free(w2);

  return P1loopA;

}

//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

double P1loopBfunc(double xmu, void *par)
{

  struct my_1loop_params *params = (struct my_1loop_params *) par;

  double ak = (params->ak);
  double rq = (params->rq);

  double P1loopB, F2Sq, FF, rk1q, a1q;

  rk1q = sqrt(ak*ak+rq*rq-2.0*ak*rq*xmu);
  a1q = (ak*xmu-rq)/rk1q ;
  FF=F2(rk1q,rq,a1q);
  F2Sq = FF*FF;
  P1loopB=PlinCDMTarget(rk1q)*F2Sq;

  return P1loopB;

}

//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

// 2L power spectrum

double get_twoloop(double ak)
{

  int comp, nregions, neval, fail;

  cubareal integral[NCOMP], error[NCOMP], prob[NCOMP];

  double P2loop, pi, rel_percent;
      
  pi = M_PI;

  Vegas(NDIM, NCOMP, P2loopIntegrand, USERDATA, NVEC,
	EPSREL, EPSABS, VERBOSE, SEED,
	MINEVAL, MAXEVAL, NSTART, NINCREASE, NBATCH,
	GRIDNO, STATEFILE, SPIN,
	&neval, &fail, integral, error, prob);

  //printf("VEGAS RESULT:\tneval %d\tfail %d\n",neval, fail);
  for( comp = 0; comp < NCOMP; ++comp )
    {
      integral[comp]= ((double) integral[comp])/pow((2.0*pi),6.0);
      error[comp]= ((double) error[comp])/pow((2.0*pi),6.0);
      rel_percent = 100.0*fabs(((double) error[comp])/((double) integral[comp]));
      //printf("VEGAS RESULT: \t:= %14.7e +- %14.7e ; percent:= %14.7e ; prob:= %14.7e\n",
      //(double)integral[comp], (double)error[comp], rel_percent, (double)prob[comp] );
    }

  P2loop = integral[NCOMP-1];

  return P2loop;
}

//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

static int P2loopIntegrand(const int *ndim,const cubareal xx[],
   const int *ncomp,cubareal ff[], void *userdata) 
{
  double f, F3sSq, ak, q, p, x, y, phi;
  double Plin_kqp, Plin_q, Plin_p;
  double pi,cosxy,kqp,a1,a2,a3;
  double qmin,qmax,pmin,pmax,xmin,xmax;
  double ymin,ymax,phimin,phimax;

  pi     = M_PI;

  ak     = rk_MPT;     // rk is a global variable
  qmin   = rklow_MPT;  // rklow, rkhigh are global variables
  qmax   = rkhigh_MPT;
  pmin   = rklow_MPT;
  pmax   = rkhigh_MPT;
  xmin   = -1.0;
  xmax   = 1.0;
  ymin   = -1.0;
  ymax   = 1.0;
  phimin = 0.0;
  phimax = 2.0*pi;

  q   = qmax*xx[0]+qmin*(1.0-xx[0]);
  p   = pmax*xx[1]+pmin*(1.0-xx[1]);
  x   = xmax*xx[2]+xmin*(1.0-xx[2]);
  y   = ymax*xx[3]+ymin*(1.0-xx[3]);
  phi = phimax*xx[4]+phimin*(1.0-xx[4]);

  cosxy = (x*y+sqrt(1.0-x*x)*sqrt(1.0-y*y)*cos(phi));

  kqp   = sqrt(ak*ak+q*q+p*p-2.*(ak*p*y+ak*q*x-p*q*cosxy));
      
  a1 = (ak*q*x-q*q-p*q*cosxy)/(kqp*q);
  a2 = cosxy;
  a3 = (ak*p*y-p*p-p*q*cosxy)/(kqp*p);

  F3sSq = pow(F3s(kqp,q,p,a1,a2,a3),2.0);

  Plin_kqp = PlinCDMTarget(kqp);
  Plin_q = PlinCDMTarget(q);
  Plin_p = PlinCDMTarget(p);
  
  f=F3sSq*Plin_kqp*Plin_q*Plin_p;

  f=f*q*q*p*p  ;

  ff[0]=f*12.0*pi*(qmax-qmin)*(xmax-xmin)
       *(pmax-pmin)*(ymax-ymin)*(phimax-phimin);

  return 0;
  
}

//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

// ! symmetrized F3 kernel

double F3s(double q1,double q2,double q3,double x12,double x23,double x31)  
{
  double F3s;
      
  F3s =
    F3(q1,q2,q3,x12,x23,x31)+
    F3(q2,q1,q3,x12,x31,x23)+
    F3(q3,q2,q1,x23,x12,x31)+
    F3(q1,q3,q2,x31,x23,x12)+
    F3(q2,q3,q1,x23,x31,x12)+
    F3(q3,q1,q2,x31,x12,x23);

  return F3s/6.0;
 
}

//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

// ! F3 (needs k=q123 as common)

double F3(double q1,double q2,double q3,double x12,double x23,double x31)  
{
  double F3, al1,al2,be1,be2, ak;

  ak = rk_MPT; // rk is global variable
  
  al1 = 1.0+(q2*x12+q3*x31)/q1;
  al2 = 1.0+(q3*q1*x31+q2*q3*x23)/(q1*q1+q2*q2+2.0*q1*q2*x12);
  be1 = 0.50*ak*ak *(q1*q2*x12+q3*q1*x31)/(q1*q1);
  be1 = be1/(q2*q2+q3*q3+2.0*q2*q3*x23);
  be2 = 0.50*ak*ak *(q2*q3*x23+q3*q1*x31)/(q3*q3);
  be2 = be2/(q1*q1+q2*q2+2.0*q1*q2*x12);
      
  F3  = 7.0*al1*F2(q2,q3,x23) + 2.0*be1* G2(q2,q3,x23)+
    (7.0*al2 + 2.0*be2)*G2(q1,q2,x12) ;
      
  F3 =  F3/18.0;

  if(F3!=F3) F3 = 0.0;
  return F3;
}

//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

// F2 kernel

double F2(double q1,double q2,double x12)   
{
  double F2;
  F2 = (5.0/7.0 + x12/2.0 *(q1/q2+q2/q1) + 2.0/7.0 *x12*x12);
  return F2;
}

//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

// G2 kernel

double G2(double q1,double q2,double x12)   
{
  double G2;
  G2 = (3.0/7.0 + x12/2.0 *(q1/q2+q2/q1) + 4.0/7.0 *x12*x12);
  return G2;
}

//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

