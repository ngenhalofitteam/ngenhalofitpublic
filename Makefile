
#----------------------------------------------------------------------
# From the list below, please activate/deactivate the options that     
# apply to your run. If you modify any of these options, make sure     
# that you recompile the whole code by typing "make clean; make".      
#                                                                      
# Look at end of file for a brief guide to the compile-time options.   
#----------------------------------------------------------------------

EXEC   = NGenHalofit.exe

OBJS   = main.o  system.o  allvars.o read_parameters.o \
	read_linear_powspec.o cosmology.o GenGrowthFac.o \
	NGenHalofitMOD.o read_daemmerung_powerspec.o spline_spectra.o \
	apply_smoothing_spline.o MPTmodule.o sigmaMOD.o

INCL   = allvars.h  proto.h  tags.h  Makefile

#--------------------------------------- Basic operation mode of code
#--------------------------------------- Things that are always recommended

OPT   +=  -DTAKAHASHI
OPT   +=  -DFIRSTDERIV
OPT   +=  -DSECONDDERIV
OPT   +=  -DMPT
OPT   +=  -DMPTOUT
OPT   +=  -DMPTPROPAGATORCORR
OPT   +=  -DNOCOSMICVARIANCE
OPT   +=  -DSUPERCOMP
OPT   +=  -DSMOOTHJOIN
OPT   +=  -DSYNCSPECTRA
OPT   +=  -DCORRECTLARGESCALES
OPT   +=  -DKILLTAYLORSTRICT
#OPT   +=  -DKILLTAYLORRELAXED
#OPT   +=  -DWRITEALLDATA


#--------------------------------------- 
#----------------------------------------------------------------------
# Here, select compile environment for the target machine. This may need 
# adjustment, depending on your local system. Follow the examples to add
# additional target platforms, and to get things properly compiled.
#----------------------------------------------------------------------

#--------------------------------------- Select target computer

CC       =  gcc                 # sets the C-compiler
OPTIMIZE =  -O2                 # sets optimization and warning flags

#--------------------------------------- Select target computer

SYSTYPE="MAC"
#SYSTYPE="Ubuntu"

#--------------------------------------- Adjust settings for target computer

#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

ifeq ($(SYSTYPE),"MAC")
CC       =  gcc
OPTIMIZE =  -O3 
GSL_INCL = -I${HOME}/WORK/lib64/GSL/gsl-2.2-install/include
GSL_LIBS = -L${HOME}/WORK/lib64/GSL/gsl-2.2-install/lib -lgsl -lgslcblas
CUBA_INCL = -I${HOME}/WORK/lib64/CUBA/cuba-4.2/ 
CUBA_LIBS = -L${HOME}/WORK/lib64/CUBA/cuba-4.2/ -lcuba
endif

ifeq ($(SYSTYPE),"Ubuntu")
CC       =  gcc
OPTIMIZE =  -O3 
GSL_INCL = -I/usr/include
GSL_LIBS = -L/urs/lib/lib -lgsl -lgslcblas
CUBA_INCL = -I${HOME}/libs/Cuba-4.2/ 
CUBA_LIBS = -L${HOME}/libs/Cuba-4.2/ -lcuba
endif


#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

OPTIONS =  $(OPTIMIZE) $(OPT)

CFLAGS = $(OPTIONS) $(GSL_INCL) $(CUBA_INCL)

LIBS   =   -g  $(GSL_LIBS) $(CUBA_LIBS) -lm 

$(EXEC): $(OBJS) 
	$(CC) $(CFLAGS)  -o  $(EXEC) $(OBJS)   $(LIBS) 

$(OBJS): $(INCL) 


clean:
	rm -f $(OBJS) $(EXEC)


