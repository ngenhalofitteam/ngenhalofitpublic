#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <time.h>
#include <sys/time.h>
#include <sys/resource.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <signal.h>
#include <gsl/gsl_errno.h>
#include <gsl/gsl_spline.h>
#include <gsl/gsl_sf_erf.h>

#include "allvars.h"
#include "proto.h"

//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

void setParTarget(void)
{

  int i, npar;
  npar = NVAR/2;

  ParTarget[0]=cp.w0;         // w0
  ParTarget[1]=cp.w1;         // wa
  ParTarget[2]=cp.om_DE0;     // om_DE0
  ParTarget[3]=cp.om_ch20;    // omch2
  ParTarget[4]=cp.om_bh20;    // ombh2
  ParTarget[5]=cp.pindex;     // ns
  ParTarget[6]=cp.As;         // As
  ParTarget[7]=cp.running;    // running

  printf("\nCosmological parameters of the target run\n");
  for (i=0; i<npar; i++)
    {
      printf("ipar:= %d, ParTarget[%d]:= %+14.7e, ParTarget-PartFid[%d]:= %+14.7e\n",
	     i,i,ParFid[i],i,ParTarget[i]-ParFid[i]);
    }

}


//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

void gen_nonlinear_spectra(void)
{

  FILE *fout;

  char outfile[180];

  int iPk, nPk, ia;

  double pi, aa, zout, Damp, rk, yy, dfac;
  double Plin, Pnl, Pq, Ph, PNGen, Pnl2003, Pnl2012;

  double xmin, xmax, delx;
    
  double xval[NPKOUT], yval1[NPKOUT], yval2[NPKOUT], yval3[NPKOUT], yval4[NPKOUT];

  pi = M_PI;
  
  nPk=hfit.nPkOut; 

  for (ia=0; ia<naexpTarget ; ia++)
    {
      
      // set the expansion fator to compute
      cp.aexp=xaexpTarget[ia];
      aa=cp.aexp;
      
      // redshift
      zout=1.0/aa-1.0;
      
      // generate the new values of omega for this model
      NewOmega(zout);
      
      // calculate the growth factor
      DenGrowTarget(&Damp,aa,1.0);
      pkTarget.Damp=Damp;
      
      // generate the values for the effective spectral index etc
      EffecSpecTargetSP(aa);

      xmin = hfit.rkOutMIN;
      xmax = hfit.rkOutMAX;

      if(hfit.iLogOrLin==1)
	{
	  xmin=log(xmin);
	  xmax=log(xmax);
	}

      delx = (xmax-xmin)/(nPk-1.0);

      for (iPk=0;iPk<nPk;iPk++)
	{
	  
	  xval[iPk]=xmin+(iPk)*delx;
	  
	  if(hfit.iLogOrLin==0)	    
	    rk=xval[iPk];
	  else if(hfit.iLogOrLin==1)	    
	    rk=exp(xval[iPk]);
	  
	  yy=rk/(2.0*pi);
	  dfac = 4.0*pi*yy*yy*yy;
	  
	  Plin = PlinCDMTarget(rk);

	  // NGenHalofit
#ifdef TAKAHASHI	  

	  // get halofit2012 (Takahashi et al. 2012)
	  halofit2012(rk,Plin*dfac,&Pnl2012,&Pq,&Ph);
	  Pnl2012=Pnl2012/dfac;

	  PNGen=NGenHalofit(rk,aa,Plin,Pnl2012,ParTarget);

	  yval1[iPk]=Plin;
	  yval2[iPk]=Pnl2012;
	  yval3[iPk]=PNGen;

#else

	  // get halofit2003 (Smith et al. 2003)
	  halofit2003(rk,Plin*dfac,&Pnl2003,&Pq,&Ph);
	  Pnl2003=Pnl2003/dfac;
	  
	  PNGen=NGenHalofit(rk,aa,Plin,Pnl2003,ParTarget);

	  yval1[iPk]=Plin;
	  yval2[iPk]=Pnl2003;
	  yval3[iPk]=PNGen;

#endif
	  
	}

      // write out the data to a file
      sprintf(outfile,"%s/%s.%d.dat",All.OutputDir,All.OutputFileBase,ia);
      printf("%s\n",outfile);
      
      if((fout = fopen(outfile,"w")))
	{
	  for (iPk=0;iPk<nPk;iPk++)
	    {
	      if(hfit.iLogOrLin==0)
		fprintf(fout,"%+14.7e\t%+14.7e\t%+14.7e\t%+14.7e\t\n",
			xval[iPk],yval1[iPk],yval2[iPk],yval3[iPk]);
	      else if(hfit.iLogOrLin==1)
		fprintf(fout,"%+14.7e\t%+14.7e\t%+14.7e\t%+14.7e\t\n",
			exp(xval[iPk]),yval1[iPk],yval2[iPk],yval3[iPk]);
	    }
	}
      else
	{
	  printf("\noutfile not found:= %s.\n", outfile);
	  endrun(212);	    
	}
      fclose(fout);
      
    }
      
}


//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

double NGenHalofit(double rk, double aa, double Plin, double Phf, double Par[])
{

  int ipar, jpar, npar;
  double DeltaPar1, DeltaPar2, PkDeriv1, PkDeriv2, Wf;
  double PNGen, PNGeninterp, logaa, logrk, logyfid, yfid, deltalogy;
  double PNGeninterpA, PNGeninterpB, rkcut, sigma;
  double Wtot, thetacut[NPAR], sigmaVec[NPAR], WfVec[NPAR];
  double Gprop, GpropSq, P1loop, P2loop, PkMPT, Pprop;
  double GpropTMP, P1loopTMP, P2loopTMP, PkMPTTMP, PpropTMP;
  double Gam0, Gam1, Gam2, zout, Damp, Gprop_fk;
  double rkLowPass, WLowPass, acut;
  double killTaylor[NPAR];

  //number of parameters
  npar = NVAR/2;
  
  // compute the fiducial contribution 
  logaa = log(aa);
  logrk = log(rk);

  // Fiducial model
  yfid=1.0;
  if(rk>rkmin && rk<rkmax) 
    {

#ifdef SUPERCOMP
      logyfid = gsl_spline2d_eval(PkFid_bispl_SuperComp, logaa,
				  logrk, aexp_acc_SuperComp, rkVec_acc_SuperComp);
#else
      logyfid = gsl_spline2d_eval(PkFid_bispl, logaa, logrk, aexp_acc, rkVec_acc);
#endif      
      yfid = exp(logyfid);

    }
      
  // derivatives from the Taylor expansion
  deltalogy = 0;

  if(rk>rkminSPVar && rk<rkmaxSPVar) 
    {

#ifdef FIRSTDERIV
      // compute the first order derivative contribution
      for (ipar=0; ipar<npar;ipar++)
	{
	  
	  DeltaPar1 = Par[ipar]-ParFid[ipar];

	  PkDeriv1 = gsl_spline2d_eval(sp_deriv1[ipar].PkDeriv_bispl, logaa,
					   logrk, sp_deriv1[ipar].aexp_acc, sp_deriv1[ipar].rkVec_acc);
	  
	  deltalogy += PkDeriv1 * DeltaPar1; 
	}

#ifdef SECONDDERIV
      // compute the second order derivative contribution
      for (ipar=0;ipar<npar;ipar++)
	{
	  
	  DeltaPar2 = Par[ipar]-ParFid[ipar];
	      
	  PkDeriv2 = gsl_spline2d_eval(sp_deriv2[ipar].PkDeriv_bispl, logaa,
				       logrk, sp_deriv2[ipar].aexp_acc, sp_deriv2[ipar].rkVec_acc);
	  
	  deltalogy += 0.5 * PkDeriv2 * DeltaPar2 * DeltaPar2; 
	}

#endif	  
#endif	  
      
    }

  //%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  // Use a transition function Wf to kill the Taylor expansion correction 
  // terms and return to halofit2012 when the parameter differences are comparable 
  // to the stepsizes used in the variational simulations

#ifdef KILLTAYLORRELAXED

  Wtot = 1.0;  

  killTaylor[0] = 8.0;
  killTaylor[1] = 8.0;
  killTaylor[2] = 8.0;
  killTaylor[3] = 8.0;
  killTaylor[4] = 8.0;
  killTaylor[5] = 8.0;
  killTaylor[6] = 8.0;
  killTaylor[7] = 8.0;

  for (ipar=0; ipar<npar; ipar++)
    {
      DeltaPar1 = fabs(Par[ipar]-ParFid[ipar]);
      thetacut[ipar]=fabs(ParFid[ipar]+killTaylor[ipar]*ParDelta[ipar]);
      sigmaVec[ipar]=fabs(8.0*ParDelta[ipar])*0.1;
      WfVec[ipar] = WSmoothTH(Par[ipar],thetacut[ipar],sigmaVec[ipar]);
      Wtot = Wtot*WfVec[ipar];      
    }
#endif

#ifdef KILLTAYLORSTRICT

  Wtot = 1.0;  
  
  killTaylor[0] = 0.5;
  killTaylor[1] = 2.0;
  killTaylor[2] = 2.0;
  killTaylor[3] = 2.0;
  killTaylor[4] = 2.0;
  killTaylor[5] = 2.0;
  killTaylor[6] = 2.0;
  killTaylor[7] = 2.0;

  for (ipar=0; ipar<npar; ipar++)
    {
      DeltaPar1 = fabs(Par[ipar]-ParFid[ipar]);
      thetacut[ipar]=fabs(ParFid[ipar]+killTaylor[ipar]*ParDelta[ipar]);
      sigmaVec[ipar]=fabs(0.5*ParDelta[ipar])*0.1;
      WfVec[ipar] = WSmoothTH(Par[ipar],thetacut[ipar],sigmaVec[ipar]);
      Wtot = Wtot*WfVec[ipar];      
    }
#endif

  deltalogy = Wtot*deltalogy;

  //%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
#ifdef MPT  
  // get the Multi-Point Propagator Theory prediction at 2-loops
  rkMPTMIN=0.005;
  rkMPTMAX=0.3;
  
  if(rk>rkMPTMIN && rk<rkMPTMAX)
    {
      
      rk_MPT=rk;	  
  
      // max and min k-values for integrals
      rklow_MPT  = 1.0e-3;
      rkhigh_MPT = fmax(20.0*rk_MPT,M_PI);   

      // redshift
      zout=1.0/aa-1.0;
      
      // generate the new values of omega for this model
      NewOmega(zout);
      
      // calculate the growth factor
      DenGrowTarget(&Damp,aa,1.0);
      pkTarget.Damp=Damp;

      // Gprop fk
#ifdef MPTPROPAGATORCORR
      Gprop_fk = Gprop_fk_V2_Fun(rk_MPT,aa);
#else
      Gprop_fk = Gprop_fk_Fun(rk_MPT,aa);
#endif     
      Gam0 = Gprop_fk*Damp;

      // get Pprop
      Pprop = (Plin/Damp/Damp)*Gam0*Gam0;
      
      // get the 1loop MPT contribution
      P1loop=P1loopFun(rk_MPT,aa);

      // get the 2loop MPT contribution
      P2loop=P2loopFun(rk_MPT,aa);
      
      // nonlinear MPT power spectrum
      PkMPT=Pprop + P1loop + P2loop;

    }
  else if (rk<=rkMPTMIN)
    PkMPT=Phf;
  else if (rk>=rkMPTMAX)
    PkMPT=0.0;
#endif

  //%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  
  // Taylor expansion model
#ifdef MPT
  PNGeninterpA = PkMPT;
#else
  PNGeninterpA =  Phf * (1.0+deltalogy);
#endif
  PNGeninterpB = Phf * yfid* (1.0+deltalogy);

  //%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

  // For expansion factors less than a=0.25 we will transition
  // back to the old halofit2012 prescription for the small-scales.
  
  acut=0.25;
  sigma=0.05;
  Wf = Wfilter(log10(aa),log10(acut),sigma);
  PNGeninterpB = Wf*Phf+(1.0-Wf)*PNGeninterpB ;

  //%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  
  // Use a transition function Wf to go from
  // pure MPT/halofit on large scales to the N-body calibrated
  // Taylor expansion on small scales

  rkcut=0.075;
  sigma=0.05;

  Wf = Wfilter(log10(rk),log10(rkcut),sigma);
  PNGen = Wf*PNGeninterpA+(1.0-Wf)*PNGeninterpB ;

  //%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

  // for k larger than some fraction of the largest k in the
  // simulation we will transition back to the halofit model prediction.

  rkLowPass = 9.0;
  sigma=0.07;

  WLowPass = Wfilter(log10(rk),log10(rkLowPass),sigma);
  if (rk>rkLowPass*0.1)
    PNGen = WLowPass*PNGen +(1.0-WLowPass)*Phf;


  return PNGen;
  
}

//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

double Wfilter(double xx, double xxcut, double sigma)
{

  double yy, erf, Wf;
  yy=(xx-xxcut)/(sqrt(2.)*sigma);
  erf=gsl_sf_erf(yy);
  Wf = 1.0-0.5*(1.0 + erf);
  return Wf;
  
}

//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

double WSmoothTH(double xx, double xxcut, double sigma)
{
  double arg1, arg2, Erf1, Erf2, WSmoothTH;
  arg1 = (xx-xxcut)/(sqrt(2.0)*sigma);
  arg2 = (-xx-xxcut)/(sqrt(2.0)*sigma);
  Erf1 = gsl_sf_erf(arg1);
  Erf2 = gsl_sf_erf(arg2);
  WSmoothTH = -0.5*(Erf1+Erf2);
  return WSmoothTH;
}

//!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

// Smith et al. (2003)

void halofit2003(double rk, double Plin, double *Pnl,
		 double *Pq, double *Ph)
{
  double  om_m, om_DE, rneff, rncur, rknl;
  double  gam,a,b,c,xmu,xnu,alpha,beta,y;
  double  f1,f2,f3,f1a,f2a,f3a,f1b,f2b,f3b,frac;

  om_m=cp.om_m;
  om_DE=cp.om_DE;

  rneff=hfit.rneff;
  rncur=hfit.rncur;
  rknl=hfit.rknl;

  gam=0.86485+0.2989*rneff+0.1631*rncur;
  a=1.4861+1.83693*rneff+1.67618*rneff*rneff+0.7940*rneff*rneff*rneff+ 
    0.1670756*rneff*rneff*rneff*rneff-0.620695*rncur;
  a=pow(10,a) ;     
  b=pow(10,(0.9463+0.9466*rneff+0.3084*rneff*rneff-0.940*rncur));
  c=pow(10,(-0.2807+0.6669*rneff+0.3214*rneff*rneff-0.0793*rncur));
  xmu=pow(10,(-3.54419+0.19086*rneff));
  xnu=pow(10,(0.95897+1.2857*rneff));
  alpha=1.38848+0.3701*rneff-0.1452*rneff*rneff;
  beta=0.8291+0.9854*rneff+0.3400*rneff*rneff;
    
  if(fabs(1.0-om_m)>0.01)
    {
      f1a=pow(om_m,(-0.0732));
      f2a=pow(om_m,(-0.1423));
      f3a=pow(om_m,(0.0725));
      f1b=pow(om_m,(-0.0307));
      f2b=pow(om_m,(-0.0585));
      f3b=pow(om_m,(0.0743));       
      frac=om_DE/(1.-om_m); 
      f1=frac*f1b + (1-frac)*f1a;
      f2=frac*f2b + (1-frac)*f2a;
      f3=frac*f3b + (1-frac)*f3a;
    }
  else         
    {
      f1=1.0;
      f2=1.0;
      f3=1.0;
    }
    
  y=(rk/rknl);
    
  *Ph=a*pow(y,(f1*3))/(1+b*pow(y,(f2))+pow((f3*c*y),(3-gam)));
  *Ph=*Ph/(1+xmu/y+xnu/y/y);
  *Pq=Plin*pow((1+Plin),beta)/(1+Plin*alpha)*exp(-y/4.0-y*y/8.0);
    
  *Pnl=*Pq+*Ph;
    
}

//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

// Takahashi et al. (2012)

void halofit2012(double rk, double Plin, double *Pnl,
		 double *Pq, double *Ph)
{
  double  om_m, om_DE, w0, rneff, rncur, rknl;
  double  gam,a,b,c,xmu,xnu,alpha,beta,y;
  double  f1,f2,f3,f1a,f2a,f3a,f1b,f2b,f3b,frac;
  double rn1, rn2, rn3, rn4;
  
  om_m=cp.om_m;
  om_DE=cp.om_DE;
  w0=cp.w0;
  
  rneff=hfit.rneff;
  rncur=hfit.rncur;
  rknl=hfit.rknl;

  rn1=rneff;
  rn2=pow(rn1,2);
  rn3=pow(rn1,3);
  rn4=pow(rn1,4);
  
  gam = 0.1971-0.0843*rn1+0.8460*rncur;

  a = 1.5222+2.8553*rn1+2.3706*rn2+0.9903*rn3
    +0.2250*rn4-0.6038*rncur+0.1749*om_DE*(1.0+w0);
  a=pow(10.0,a) ;     
  
  b = -0.5642+0.5864*rn1+0.5716*rn2-1.5474*rncur
    + 0.2279*om_DE*(1.0+w0);
  b = pow(10.0,b);
  
  c = 0.3698+2.0404*rn1+0.8161*rn2+0.5869*rncur;
  c = pow(10.0,c);

  xmu = 0.0;
    
  xnu = 5.2105+3.6902*rn1 ;
  xnu = pow(10.0,xnu);

  alpha =  fabs(6.0835+1.3373*rn1-0.1959*rn2-5.5274*rncur);

  beta = 2.0379-0.7354*rn1+0.3157*rn2+1.2490*rn3+0.3980*rn4-0.1682*rncur;

  if(fabs(1.0-om_m)>0.01)
    {
      f1=pow(om_m,(-0.0307));
      f2=pow(om_m,(-0.0585));
      f3=pow(om_m,(0.0743));       
    }
  else         
    {
      f1=1.0;
      f2=1.0;
      f3=1.0;
    }
    
  y=(rk/rknl);
    
  *Ph=a*pow(y,(f1*3))/(1+b*pow(y,(f2))+pow((f3*c*y),(3-gam)));
  *Ph=*Ph/(1+xmu/y+xnu/y/y);
  *Pq=Plin*pow((1+Plin),beta)/(1+Plin*alpha)*exp(-y/4.0-y*y/8.0);
    
  *Pnl=*Pq+*Ph;
    
}


//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

// Subroutine calculates the 
//
// nonlinear wavenumber     (rknl) 
// effective spectral index (rneff) 
// curvature                (rncur)
  
void EffectiveSpecVar(int iVAR)
{

  double  xlogr1, xlogr2, rmid, diff, sig, d1, d2;
  int i, nitmax;
  
  nitmax = 100;
  
  xlogr1=-2.0;
  xlogr2=3.5;

  for (i=0; i<nitmax; i++)
    {
      rmid=(xlogr2+xlogr1)/2.0;   
      rmid=pow(10.0,rmid);
      wintVar(rmid,&sig,&d1,&d2,iVAR);
      diff=sig-1.0;

      if (fabs(diff) <= 0.001) 
	break;
      else if ( (diff > 0.001) && (i<nitmax-1))
	{
	  xlogr1=log10(rmid);
	}
      else if ( (diff < -0.001) && (i<nitmax-1)) 
	{
	  xlogr2=log10(rmid);
	}
      else
	{
	  printf("no effective halofit parameters found:\n");
	  endrun(111);
	}
    }
      
  hfit.rknl=1./rmid;
  hfit.rneff=-3-d1;
  hfit.rncur=-d2;
  
}

//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

// The subroutine wint, finds the effective spectral quantities
// rknl, rneff & rncur. This it does by calculating the radius of 
// the Gaussian filter at which the variance is unity = rknl.
// rneff is defined as the first derivative of the variance, calculated 
// at the nonlinear wavenumber and similarly the rncur is the second
// derivative at the nonlinear wavenumber. 

void wintVar(double r, double *sig, double *d1, double *d2, int iVAR)
{      
  double sum1,sum2,sum3,t,y,x,w1,w2,w3; 
  double rk, dfac, tmpd2, pi_d, Pk;

  int i, maxit;
  
  pi_d = M_PI;

  maxit=3000;
  sum1=0.0;
  sum2=0.0;
  sum3=0.0;

  for(i=1; i<=maxit; i++)
    {
      t=((double) i-0.50)/((double) maxit);
      y=-1.0+1.0/t;
      rk=y;      
      dfac=4.0*pi_d*rk*rk*rk/pow((2.0*pi_d),3);
      Pk=PlinCDMVar(rk,iVAR);
      tmpd2=Pk*dfac;
      x=y*r;
      w1=exp(-x*x);
      w2=2*x*x*w1;
      w3=4*x*x*(1-x*x)*w1;
      sum1=sum1+w1*tmpd2/y/t/t;
      sum2=sum2+w2*tmpd2/y/t/t;
      sum3=sum3+w3*tmpd2/y/t/t;
    }

  sum1=sum1/(((double) maxit) * 1.0);
  sum2=sum2/(((double) maxit) * 1.0);
  sum3=sum3/(((double) maxit) * 1.0);

  *sig=sqrt(sum1);
  *d1=-sum2/sum1;
  *d2=-sum2*sum2/sum1/sum1 - sum3/sum1;

}

//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

// compute halofit effective spectral parameters for the variational
// Daemmerung runs

//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

// generate the halofit parameters

void GenEffectiveSpecVar(void)
{
  double  aa, logaa, Damp, aas, aao;
  int  i, iVAR, neff;

  FILE *fout, *fin;

  char neffFile[120];
  
  printf("iGenEffSpecVar:= %d\n",hfit.iGenEffSpecVar);

  neff = NEFFSPEC;

  hfit.aas=0.25;
  hfit.aao=1.0;

  aas=hfit.aas;
  aao=hfit.aao;
  
  if(hfit.iGenEffSpecVar == 1 ) 
    {
	  
      printf("Generating effective spectral parameters for the daemmerung runs\n");

      for (iVAR=0; iVAR<NVAR+1; iVAR++)
	{

	  // set the cosmological parameters for this model
	  init_cospar_Daemmerung(iVAR);

	  sprintf(neffFile,"DATAEffectiveSpectra/GenerateEffectiveSpec.iVAR_%d.dat",iVAR);
	  
	  printf("%s",neffFile);

	  for(i=0; i<neff; i++)
	    {
	      
	      logaa = log(aas)+(i)*log(aao/aas)/(neff-1.0);
	      xeffVar[iVAR][i] = logaa;
	      
	      aa=exp(logaa);
	      cp.aexp=aa;
	      cp.zout=1.0/cp.aexp-1.0;
	      
	      // generater the new values of omega for this model
	      NewOmega(cp.zout);

	      // calculate the growth factor
	      DenGrowVar(&Damp,aa,1.0,iVAR);
	      
	      pkVar.Damp=Damp;
	      
	      EffectiveSpecVar(iVAR);
	      
	      yrknlVar[iVAR][i]=log(hfit.rknl);
	      yrneffVar[iVAR][i]=hfit.rneff;
	      yrncurVar[iVAR][i]=hfit.rncur;

	    }


	  // write all of the effective spectra data to a file
	  if((fout=fopen(neffFile,"w")) == NULL)
	    {
	      printf("cannot open %s for writing\n",neffFile);
	      endrun(2);
	    }
	  fprintf(fout,"%d\n",neff);
	  for (i=0; i<neff; i++)
	    {
	      fprintf(fout,"%14.7e   %14.7e   %14.7e   %14.7e\n",
		      xeffVar[iVAR][i],yrknlVar[iVAR][i],yrneffVar[iVAR][i],yrncurVar[iVAR][i]);
	      	      
	    }
	  fclose(fout);
	  
	}
    }
  else
    {

      printf("Reading pre-computed effective spectral parameters for the daemmerung runs\n");

      for (iVAR=0; iVAR<NVAR+1; iVAR++)
	{
	  
	  sprintf(neffFile,"DATAEffectiveSpectra/GenerateEffectiveSpec.iVAR_%d.dat",iVAR);
	  
	  // read all of the effective spectra data to a file
	  if((fin=fopen(neffFile,"r")) == NULL)
	    {
	      printf("cannot open %s for reading\n",neffFile);
	      endrun(2);
	    }
	  fscanf(fin,"%d",&neff);
	  for (i=0; i<neff; i++)
	    {
	      fscanf(fin,"%le %le %le %le",
		     &xeffVar[iVAR][i],&yrknlVar[iVAR][i],&yrneffVar[iVAR][i],&yrncurVar[iVAR][i]);
	    }
	  fclose(fin);
	  
	}
    }

  // now generate a spline fit to all of the effective parameters
  splineEffecSpecVar();

}

//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

void splineEffecSpecVar(void)
{

  int iVAR, i;

  printf("Splining effective spectra\n");
  
  // generate the splines of the effective spectral quantities

  if(!(EffSpecVar_spls=malloc(sizeof(struct EffSpec_splines)*(NVAR+1))))
    {
      printf("failed to allocate memory for `EffSpec_spls'.\n" );
      endrun(3);
    }
  
  // create spline functions for all of the data
  EffSpecVar_acc = gsl_interp_accel_alloc();

  // generate the splines for the various cosmologies
  for (iVAR=0;iVAR<NVAR+1; iVAR++)
    {
      // rknl
      EffSpecVar_spls[iVAR].sp_rknl = gsl_spline_alloc(gsl_interp_cspline, NEFFSPEC);
      gsl_spline_init(EffSpecVar_spls[iVAR].sp_rknl, xeffVar[iVAR], yrknlVar[iVAR], NEFFSPEC);

      // neff
      EffSpecVar_spls[iVAR].sp_neff = gsl_spline_alloc(gsl_interp_cspline, NEFFSPEC);
      gsl_spline_init(EffSpecVar_spls[iVAR].sp_neff, xeffVar[iVAR], yrneffVar[iVAR], NEFFSPEC);

      //ncur
      EffSpecVar_spls[iVAR].sp_ncur = gsl_spline_alloc(gsl_interp_cspline, NEFFSPEC);
      gsl_spline_init(EffSpecVar_spls[iVAR].sp_ncur, xeffVar[iVAR], yrncurVar[iVAR], NEFFSPEC);

    }  

}

//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

void EffecSpecVarSP(double aa, int iVAR)
{

  double EffecSpecVarSP, xx, yyA, yyB, yyC;
  double x1, x2, y1A, y2A, y1B, y2B, y1C, y2C, mm, cc;
  
  if (aa > hfit.aas && aa <= hfit.aao)
    {
      xx = log(aa);
      yyA = gsl_spline_eval(EffSpecVar_spls[iVAR].sp_rknl, xx, EffSpecVar_acc);
      yyB = gsl_spline_eval(EffSpecVar_spls[iVAR].sp_neff, xx, EffSpecVar_acc);
      yyC = gsl_spline_eval(EffSpecVar_spls[iVAR].sp_ncur, xx, EffSpecVar_acc);
    }
  else if( aa <= hfit.aas)
    {
      
      // use the first two points of the Pk function to generate a power-law model
      xx= log(aa);
      x1=xeffVar[iVAR][0];
      x2=xeffVar[iVAR][1];

      y1A=yrknlVar[iVAR][0];
      y2A=yrknlVar[iVAR][1];
      mm=(y2A-y1A)/(x2-x1);
      cc=(y1A*x2-y2A*x1)/(x2-x1);      
      yyA = mm*xx+cc;      
      
      y1B=yrneffVar[iVAR][0];
      y2B=yrneffVar[iVAR][1];
      mm=(y2B-y1B)/(x2-x1);
      cc=(y1B*x2-y2B*x1)/(x2-x1);      
      yyB = mm*xx+cc;      

      y1C=yrncurVar[iVAR][0];
      y2C=yrncurVar[iVAR][1];
      mm=(y2C-y1C)/(x2-x1);
      cc=(y1C*x2-y2C*x1)/(x2-x1);      
      yyC = mm*xx+cc;      
      
    }
  else if( aa > hfit.aao)
    {
      // invalid
      printf("invlaid value for aa\n");
      endrun(4);
    }

  hfit.rknl=exp(yyA);
  hfit.rneff=(yyB);
  hfit.rncur=(yyC);

}
  
//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

void free_EffecSpecVar(void)
{

  int iVAR;

  // free the memory for the linear power spectrum splines
  for (iVAR=0;iVAR<NVAR+1; iVAR++)
    {
      gsl_spline_free(EffSpecVar_spls[iVAR].sp_rknl);    
      gsl_spline_free(EffSpecVar_spls[iVAR].sp_neff);    
      gsl_spline_free(EffSpecVar_spls[iVAR].sp_ncur);    
    }
  gsl_interp_accel_free(EffSpecVar_acc);
  free(EffSpecVar_spls);
  
}

//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

// EFFECTIVE SPECTRAL PARAMETERS FOR THE TARGET MODEL

//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

// Subroutine calculates the effective spectral quantites for target model
//
// nonlinear wavenumber     (rknl) 
// effective spectral index (rneff) 
// curvature                (rncur)
  
void EffectiveSpecTarget(void)
{

  double  xlogr1, xlogr2, rmid, diff, sig, d1, d2;
  int i, nitmax;
  
  nitmax = 100;
  
  xlogr1=-2.0;
  xlogr2=3.5;

  for (i=0; i<nitmax; i++)
    {
      rmid=(xlogr2+xlogr1)/2.0;   
      rmid=pow(10.0,rmid);
      wintTarget(rmid,&sig,&d1,&d2);
      diff=sig-1.0;

      if (fabs(diff) <= 0.001) 
	break;
      else if ( (diff > 0.001) && (i<nitmax-1))
	{
	  xlogr1=log10(rmid);
	}
      else if ( (diff < -0.001) && (i<nitmax-1)) 
	{
	  xlogr2=log10(rmid);
	}
      else
	{
	  printf("no effective halofit parameters found:\n");
	  endrun(111);
	}
    }
      
  hfit.rknl=1./rmid;
  hfit.rneff=-3-d1;
  hfit.rncur=-d2;
  
}

//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

// The subroutine wint, finds the effective spectral quantities
// rknl, rneff & rncur. This it does by calculating the radius of 
// the Gaussian filter at which the variance is unity = rknl.
// rneff is defined as the first derivative of the variance, calculated 
// at the nonlinear wavenumber and similarly the rncur is the second
// derivative at the nonlinear wavenumber. 

void wintTarget(double r, double *sig, double *d1, double *d2)
{      
  double sum1,sum2,sum3,t,y,x,w1,w2,w3; 
  double rk, dfac, tmpd2, pi_d, Pk;

  int i, maxit;
  
  pi_d = M_PI;

  maxit=3000;
  sum1=0.0;
  sum2=0.0;
  sum3=0.0;

  for(i=1; i<=maxit; i++)
    {
      t=((double) i-0.50)/((double) maxit);
      y=-1.0+1.0/t;
      rk=y;      
      dfac=4.0*pi_d*rk*rk*rk/pow((2.0*pi_d),3);
      Pk=PlinCDMTarget(rk);
      tmpd2=Pk*dfac;
      x=y*r;
      w1=exp(-x*x);
      w2=2*x*x*w1;
      w3=4*x*x*(1-x*x)*w1;
      sum1=sum1+w1*tmpd2/y/t/t;
      sum2=sum2+w2*tmpd2/y/t/t;
      sum3=sum3+w3*tmpd2/y/t/t;
    }

  sum1=sum1/(((double) maxit) * 1.0);
  sum2=sum2/(((double) maxit) * 1.0);
  sum3=sum3/(((double) maxit) * 1.0);

  *sig=sqrt(sum1);
  *d1=-sum2/sum1;
  *d2=-sum2*sum2/sum1/sum1 - sum3/sum1;

}

//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

// generate the halofit parameters

void GenEffectiveSpecTarget(void)
{
  double  aa, logaa, Damp, aas, aao;
  int  i, iVAR, neff;

  FILE *fout, *fin;

  char neffFile[120];
  
  printf("iGenEffSpecTarget:= %d\n",hfit.iGenEffSpecTarget);

  neff = NEFFSPEC;

  hfit.aas=0.25;
  hfit.aao=1.0;

  aas=hfit.aas;
  aao=hfit.aao;
  
  if(hfit.iGenEffSpecTarget == 1 ) 
    {
	  
      printf("Generating effective spectral parameters for the target cosmology\n");

      // set the cosmological parameters for this model
      //init_cospar_target();
      
      sprintf(neffFile,"DATAEffectiveSpectra/GenerateEffectiveSpec.Target.dat");
      
      printf("DATAEffectiveSpectra/GenerateEffectiveSpec.Target.dat");

      for(i=0; i<neff; i++)
	{
	  
	  logaa = log(aas)+(i)*log(aao/aas)/(neff-1.0);
	  xeffTarget[i] = logaa;
	  
	  aa=exp(logaa);
	  cp.aexp=aa;
	  cp.zout=1.0/cp.aexp-1.0;
	  
	  // generater the new values of omega for this model
	  NewOmega(cp.zout);
	  
	  // calculate the growth factor
	  DenGrowTarget(&Damp,aa,1.0);
	  
	  pkTarget.Damp=Damp;
	  
	  EffectiveSpecTarget();
	  
	  yrknlTarget[i]=log(hfit.rknl);
	  yrneffTarget[i]=hfit.rneff;
	  yrncurTarget[i]=hfit.rncur;
	  
	}
      
      // write all of the effective spectra data to a file

      if((fout=fopen(neffFile,"w")) == NULL)
	{
	  printf("cannot open %s for writing\n",neffFile);
	  endrun(2);
	}
      fprintf(fout,"%d\n",neff);
      for (i=0; i<neff; i++)
	{
	  fprintf(fout,"%14.7e   %14.7e   %14.7e   %14.7e\n",
		  xeffTarget[i],yrknlTarget[i],yrneffTarget[i],yrncurTarget[i]);
	  
	}
      fclose(fout);
      
    }
  else
    {

      printf("Reading pre-computed effective spectral parameters for the daemmerung runs\n");

      sprintf(neffFile,"DATAEffectiveSpectra/GenerateEffectiveSpec.Target.dat");
	  
      // read all of the effective spectra data to a file
      if((fin=fopen(neffFile,"r")) == NULL)
	{
	  printf("cannot open %s for reading\n",neffFile);
	  endrun(2);
	}
      fscanf(fin,"%d",&neff);
      for (i=0; i<neff; i++)
	{
	  fscanf(fin,"%le %le %le %le",
		 &xeffTarget[i],&yrknlTarget[i],&yrneffTarget[i],&yrncurTarget[i]);
	}
      fclose(fin);
      
    }

  // now generate a spline fit to all of the effective parameters
  splineEffecSpecTarget();

}

//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

void splineEffecSpecTarget(void)
{

  int  i;

  printf("Splining effective spectra for Target cosmology\n");
  
  // generate the splines of the effective spectral quantities

  if(!(EffSpecTarget_spls=malloc(sizeof(struct EffSpec_splines)*(1))))
    {
      printf("failed to allocate memory for `EffSpec_spls_target'.\n" );
      endrun(3);
    }
  
  // create spline functions for all of the data
  EffSpecTarget_acc = gsl_interp_accel_alloc();

  // generate the splines for the target model

  // rknl
  EffSpecTarget_spls[0].sp_rknl = gsl_spline_alloc(gsl_interp_cspline, NEFFSPEC);
  gsl_spline_init(EffSpecTarget_spls[0].sp_rknl, xeffTarget, yrknlTarget, NEFFSPEC);

  // neff
  EffSpecTarget_spls[0].sp_neff = gsl_spline_alloc(gsl_interp_cspline, NEFFSPEC);
  gsl_spline_init(EffSpecTarget_spls[0].sp_neff, xeffTarget, yrneffTarget, NEFFSPEC);
  
  //ncur
  EffSpecTarget_spls[0].sp_ncur = gsl_spline_alloc(gsl_interp_cspline, NEFFSPEC);
  gsl_spline_init(EffSpecTarget_spls[0].sp_ncur, xeffTarget, yrncurTarget, NEFFSPEC);

}

//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

void EffecSpecTargetSP(double aa)
{

  double EffecSpecTargetSP, xx, yyA, yyB, yyC;
  double x1, x2, y1A, y2A, y1B, y2B, y1C, y2C, mm, cc;
  
  if (aa > hfit.aas && aa <= hfit.aao)
    {
      xx = log(aa);
      yyA = gsl_spline_eval(EffSpecTarget_spls[0].sp_rknl, xx, EffSpecTarget_acc);
      yyB = gsl_spline_eval(EffSpecTarget_spls[0].sp_neff, xx, EffSpecTarget_acc);
      yyC = gsl_spline_eval(EffSpecTarget_spls[0].sp_ncur, xx, EffSpecTarget_acc);
    }
  else if( aa <= hfit.aas)
    {
      
      // use the first two points of the Pk function to generate a power-law model
      xx= log(aa);
      x1=xeffTarget[0];
      x2=xeffTarget[1];

      y1A=yrknlTarget[0];
      y2A=yrknlTarget[1];
      mm=(y2A-y1A)/(x2-x1);
      cc=(y1A*x2-y2A*x1)/(x2-x1);      
      yyA = mm*xx+cc;      

      y1B=yrneffTarget[0];
      y2B=yrneffTarget[1];
      mm=(y2B-y1B)/(x2-x1);
      cc=(y1B*x2-y2B*x1)/(x2-x1);      
      yyB = mm*xx+cc;      

      y1C=yrncurTarget[0];
      y2C=yrncurTarget[1];
      mm=(y2C-y1C)/(x2-x1);
      cc=(y1C*x2-y2C*x1)/(x2-x1);      
      yyC = mm*xx+cc;      
      
    }
  else if( aa > hfit.aao)
    {
      // invalid
      printf("invlaid value for aa\n");
      endrun(4);
    }

  hfit.rknl=exp(yyA);
  hfit.rneff=(yyB);
  hfit.rncur=(yyC);

}
  
//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

void free_EffecSpecTarget(void)
{

  // free the memory for the linear power spectrum splines
  gsl_spline_free(EffSpecTarget_spls[0].sp_rknl);    
  gsl_spline_free(EffSpecTarget_spls[0].sp_neff);    
  gsl_spline_free(EffSpecTarget_spls[0].sp_ncur);    
  gsl_interp_accel_free(EffSpecTarget_acc);
  free(EffSpecTarget_spls);
  
}

//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
