/*! \file allvars.c
 *  \brief provides instances of all global variables.
 */

#include "allvars.h"

//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

// parameter file
char ParameterFile[MAXLEN_FILENAME]; 

// expansion factor file
char ExpansionFile[MAXLEN_FILENAME]; 

// File object for memory handling
FILE *FdMem;       /*!< file handle for info.txt log-file. */

// max mem used.
double TotalMemAlloc;

//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

// MPT variables
double sigV_MPT ;
double rk_MPT;
double rklow_MPT;
double rkhigh_MPT;
double rkMPTMAX;
double rkMPTMIN;
double rkBigBoxCut;

//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

// an array of splines for the growth factors for the variational runs

struct GrowVar_splines *GrowVar_spls;

double  xGrowVar[NVAR+1][NSTEP], yGrowVar[NVAR+1][NSTEP];

gsl_interp_accel *GrowVar_acc ;

// growth factor spline variables for the target run

gsl_spline *GrowTarget_spline;
gsl_interp_accel *GrowTarget_acc ;

double  xGrowTarget[NSTEP], yGrowTarget[NSTEP];

//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

// linear power spectra arrays for the variational runs

int nPkLinVar;

double xrkVar[NVAR+1][NPOWVAR], xPkVar[NVAR+1][NPOWVAR] ;

// linear power spectra arrays for the taget run

int nPkLinTarget;

double xrkTarget[NPOWTARGET], xPkTarget[NPOWTARGET] ;

// MPT power spectra arrays for the taget run

int nMPT;

double rkMPT_SP[NMPTMAX], Gprop_fk_SP[NMPTMAX], Gprop_fk_V2_SP[NMPTMAX];
double P1loop_SP[NMPTMAX], P2loop_SP[NMPTMAX];

//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

// an array of splines for linear power spectra for the variational runs

struct Pk_splines *PkVar_spls, *PkTarget_spls;
  
gsl_interp_accel *PkVar_acc ;
gsl_interp_accel *PkTarget_acc ;

//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
// an array of splines for linear power spectra for the variational and target runs

struct MPT_splines *MPT_spls;
gsl_interp_accel *MPT_Gprop_acc ;
gsl_interp_accel *MPT_Gprop_V2_acc ;
gsl_interp_accel *MPT_P1loop_acc ;
gsl_interp_accel *MPT_P2loop_acc ;

//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

// array for holding the target expansion factors

double xaexpTarget[NAEXPMAX];    

// number of expansion factors to compute spectra for

int naexpTarget;

//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

// effective spectra variables for the variational runs

double xeffVar[NVAR+1][NEFFSPEC];
double yrknlVar[NVAR+1][NEFFSPEC];
double yrneffVar[NVAR+1][NEFFSPEC];
double yrncurVar[NVAR+1][NEFFSPEC];

// effective spectra variables for the target run

double xeffTarget[NEFFSPEC];
double yrknlTarget[NEFFSPEC];
double yrneffTarget[NEFFSPEC];
double yrncurTarget[NEFFSPEC];

//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

// the super coomposite fiducial spectrum

int nPkSuperComp;
double rkVecSuperComp[2*NPOWSIM];
double PkVecSuperComp[NSNAPMAX+1][2*NPOWSIM];
double PkVecSigSuperComp[NSNAPMAX+1][2*NPOWSIM];

double PkVecScaleSuperComp[NSNAPMAX+1][2*NPOWSIM];
double PkVecSigScaleSuperComp[NSNAPMAX+1][2*NPOWSIM];

double PkVecScaleLinSuperComp[NSNAPMAX+1][2*NPOWSIM];
double PkVecSigScaleLinSuperComp[NSNAPMAX+1][2*NPOWSIM];

// L=3000 Box

int nPkComp_BigBox;

double rkVec_BigBox[NPOWSIM];
double rkVecComp_BigBox[NPOWSIM];

double PkVecVar_BigBox[1][NSNAPMAX+1][NPOWSIM];
double PkVecVarScale_BigBox[1][NSNAPMAX+1][NPOWSIM];
double PkVecVarScaleLin_BigBox[1][NSNAPMAX+1][NPOWSIM];

double PkVecVarSig_BigBox[1][NSNAPMAX+1][NPOWSIM];
double PkVecVarSigScale_BigBox[1][NSNAPMAX+1][NPOWSIM];
double PkVecVarSigScaleLin_BigBox[1][NSNAPMAX+1][NPOWSIM];

double atime_BigBox[1][NSNAPMAX+1];

// L=500 Box

int nPkComp;
double rkVec[NPOWSIM];
double rkVecComp[NPOWSIM];

double PkVecFidRun0[NSNAPMAX+1][NPOWSIM];

double Corr[NVAR+1][NSNAPMAX+1];
double NumCorr[NVAR+1][NSNAPMAX+1];
double PkVecVar[NVAR+1][NSNAPMAX+1][NPOWSIM];
double PkVecVarScale[NVAR+1][NSNAPMAX+1][NPOWSIM];
double PkVecVarScaleLin[NVAR+1][NSNAPMAX+1][NPOWSIM];
double PkVecVarSig[NVAR+1][NSNAPMAX+1][NPOWSIM];
double PkVecVarSigScale[NVAR+1][NSNAPMAX+1][NPOWSIM];
double PkVecVarSigScaleLin[NVAR+1][NSNAPMAX+1][NPOWSIM];

double atime[NVAR+1][NSNAPMAX+1];
double PkCorrFac[NVAR+1][NSNAPMAX+1];
double PkCorrFac_BigBox[NVAR+1][NSNAPMAX+1];

long long npartTOT;

double DiffPkVec[NVAR/2][NSNAPMAX+1][NPOWSIM];
double DiffPkVecScale[NVAR/2][NSNAPMAX+1][NPOWSIM];

double Diff2PkVec[NVAR/2][NSNAPMAX+1][NPOWSIM];
double Diff2PkVecScale[NVAR/2][NSNAPMAX+1][NPOWSIM];

double ParFid[NVAR/2];
double ParTarget[NVAR/2];
double ParDelta[NVAR/2];

double rkmin;
double rkmax;

double rkminSPFid;
double rkmaxSPFid;

double rkminSPFid_LargeScale;
double rkmaxSPFid_LargeScale;

double rkminSPFid_SmallScale;
double rkmaxSPFid_SmallScale;

double rkminSPVar;
double rkmaxSPVar;

double rkminSIM;
double rkmaxSIM;

//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

// splines for derivatives of the spectra

double logrkVec[NPOWSIM];

double logaexp[NSNAPMAX];

double logrkVec_SuperComp[2*NPOWSIM];

double logaexp_SuperComp[NSNAPMAX];

//const gsl_interp2d_type *PkDeriv_bicubic = gsl_interp2d_bicubic;

struct spline_deriv *sp_deriv1, *sp_deriv2;

//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

// splines for the fiducial spectra

double *PkFidData;
  
//const gsl_interp2d_type *PkFid_bicubic = gsl_interp2d_bicubic;

gsl_interp_accel *aexp_acc, *rkVec_acc;

gsl_spline2d *PkFid_bispl;
  

//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

// splines for the fiducial spectra

double *PkFidData_SuperComp;
  
//const gsl_interp2d_type *PkFid_bicubic = gsl_interp2d_bicubic;

gsl_interp_accel *aexp_acc_SuperComp, *rkVec_acc_SuperComp;

gsl_spline2d *PkFid_bispl_SuperComp;
  
//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

struct EffSpec_splines *EffSpecVar_spls, *EffSpecTarget_spls;
  
gsl_interp_accel *EffSpecVar_acc ;
gsl_interp_accel *EffSpecTarget_acc ;

//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

struct cosmopar cp, cp_target;

struct global_data All;

struct powerspec pkVar, pkTarget;

struct halofit hfit;

//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
