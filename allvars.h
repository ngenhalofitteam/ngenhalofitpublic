/*! \file allvars.h
 *  \brief declares global variables.
 *
 *  This file declares all global variables. Further variables should be added here, and declared as
 *  'extern'. The actual existence of these variables is provided by the file 'allvars.c'. To produce
 *  'allvars.c' from 'allvars.h', do the following:
 *
 *     - Erase all #define's, typedef's, and enum's
 *     - add #include "allvars.h", delete the #ifndef ALLVARS_H conditional
 *     - delete all keywords 'extern'
 *     - delete all struct definitions enclosed in {...}
 */

#ifndef ALLVARS_H
#define ALLVARS_H

//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

#include <stdio.h>
#include <gsl/gsl_rng.h>
#include "tags.h"
#include <gsl/gsl_errno.h>
#include <gsl/gsl_spline.h>
#include <gsl/gsl_interp2d.h>
#include <gsl/gsl_spline2d.h>

//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

// cuba includes
#if REALSIZE == 16
#include "cubaq.h"
#elif REALSIZE == 10
#include "cubal.h"
#else
#include "cuba.h"
#endif

//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

// maximum lensgth of any filename
#define  MAXLEN_FILENAME  400    /*!< Maximum number of characters for filenames (including the full path) */

// number of variational runs
#define NVAR 16

// number of cosmological parameters
#define NPAR (NVAR/2)

// max. number of entries in the linear power spectra for variational runs
#define NPOWVAR 1000

// max. number of entries in the linear power spectra for target runs
#define NPOWTARGET 1000

// number of k-bins used to generate effective spectra
#define NEFFSPEC 500

// number of steps used to compute the growth factor
#define NSTEP 1000

// max number of expansion factors for the computation of the power spetra
#define NAEXPMAX 100

// number of output Pk values required
#define NPKOUT 1000

// numer of bins for MPT power spectrum
#define NMPTMAX 200

//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

// daemmerung runs parameters

// number of simulations for the fiducial model
#define NRUN 10

// maximum snapshot number
#define NSNAPMAX 63

// minimum snaapshot number 
#define NSNAPMIN 3

// number of power spectra bins in the daemmerung Pk data
#define NPOWSIM 200

// smoothing spline parameters

/* number of fit coefficients */
#define NCOEFFS  30 // (NPOWSIM/10)

/* nbreak = ncoeffs + 2 - k = ncoeffs - 2 since k = 4 */
#define NBREAK   (NCOEFFS - 2)

/* number of fit coefficients */
#define NCOEFFS_LARGE  20 // 

/* nbreak = ncoeffs + 2 - k = ncoeffs - 2 since k = 4 */
#define NBREAK_LARGE   (NCOEFFS_LARGE - 2)

/* number of fit coefficients */
#define NCOEFFS_SMALL  10 // (NPOWSIM/10)

/* nbreak = ncoeffs + 2 - k = ncoeffs - 2 since k = 4 */
#define NBREAK_SMALL   (NCOEFFS_SMALL - 2)

/* number of fit coefficients */
#define NCOEFFS2  15 // (NPOWSIM/10)

/* nbreak = ncoeffs + 2 - k = ncoeffs - 2 since k = 4 */
#define NBREAK2   (NCOEFFS2 - 2)

//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

// parameter file
extern char ParameterFile[MAXLEN_FILENAME]; 

// expansion factor file
extern char ExpansionFile[MAXLEN_FILENAME]; 

//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

// MPT variables
extern double sigV_MPT ;
extern double rk_MPT;
extern double rklow_MPT;
extern double rkhigh_MPT;
extern double rkMPTMAX;
extern double rkMPTMIN;

extern double rkBigBoxCut;

//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

// an array of splines for the growth factors for the variational runs

extern struct GrowVar_splines
{
  gsl_spline *spline;
} *GrowVar_spls;

extern double  xGrowVar[NVAR+1][NSTEP], yGrowVar[NVAR+1][NSTEP];
extern gsl_interp_accel *GrowVar_acc ;

// growth factor spline variables for the target run

extern gsl_spline *GrowTarget_spline;
extern gsl_interp_accel *GrowTarget_acc ;

extern double  xGrowTarget[NSTEP], yGrowTarget[NSTEP];

//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

// linear power spectra arrays for the variational runs

extern int nPkLinVar;

extern double xrkVar[NVAR+1][NPOWVAR], xPkVar[NVAR+1][NPOWVAR] ;

// linear power spectra arrays for the taget run

extern int nPkLinTarget;

extern double xrkTarget[NPOWTARGET], xPkTarget[NPOWTARGET] ;

// MPT power spectra arrays for the taget run

extern int nMPT;

extern double rkMPT_SP[NMPTMAX], Gprop_fk_SP[NMPTMAX], Gprop_fk_V2_SP[NMPTMAX];
extern double P1loop_SP[NMPTMAX], P2loop_SP[NMPTMAX];

//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

// array for holding the target expansion factors

extern double xaexpTarget[NAEXPMAX];    

// number of expansion factors to compute spectra for

extern int naexpTarget;

//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

// an array of splines for linear power spectra for the variational and target runs

extern struct MPT_splines
{
  gsl_spline *spline;
} *MPT_spls;
  
extern gsl_interp_accel *MPT_Gprop_acc ;
extern gsl_interp_accel *MPT_Gprop_V2_acc ;
extern gsl_interp_accel *MPT_P1loop_acc ;
extern gsl_interp_accel *MPT_P2loop_acc ;

//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

// an array of splines for linear power spectra for the variational and target runs

extern struct Pk_splines
{
  gsl_spline *spline;
} *PkVar_spls, *PkTarget_spls;
  
extern gsl_interp_accel *PkVar_acc ;
extern gsl_interp_accel *PkTarget_acc ;

//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

// effective spectra variables for the variational runs

extern double xeffVar[NVAR+1][NEFFSPEC];
extern double yrknlVar[NVAR+1][NEFFSPEC];
extern double yrneffVar[NVAR+1][NEFFSPEC];
extern double yrncurVar[NVAR+1][NEFFSPEC];

// effective spectra variables for the target run

extern double xeffTarget[NEFFSPEC];
extern double yrknlTarget[NEFFSPEC];
extern double yrneffTarget[NEFFSPEC];
extern double yrncurTarget[NEFFSPEC];

//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

// daemmerung power spectra data

// the super coomposite fiducial spectrum

extern int nPkSuperComp;
extern double rkVecSuperComp[2*NPOWSIM];
extern double PkVecSuperComp[NSNAPMAX+1][2*NPOWSIM];
extern double PkVecSigSuperComp[NSNAPMAX+1][2*NPOWSIM];

extern double PkVecScaleSuperComp[NSNAPMAX+1][2*NPOWSIM];
extern double PkVecSigScaleSuperComp[NSNAPMAX+1][2*NPOWSIM];

extern double PkVecScaleLinSuperComp[NSNAPMAX+1][2*NPOWSIM];
extern double PkVecSigScaleLinSuperComp[NSNAPMAX+1][2*NPOWSIM];

// L=3000 Box

extern int nPkComp_BigBox;

extern double rkVec_BigBox[NPOWSIM];
extern double rkVecComp_BigBox[NPOWSIM];

extern double PkVecVar_BigBox[1][NSNAPMAX+1][NPOWSIM];
extern double PkVecVarScale_BigBox[1][NSNAPMAX+1][NPOWSIM];
extern double PkVecVarScaleLin_BigBox[1][NSNAPMAX+1][NPOWSIM];

extern double PkVecVarSig_BigBox[1][NSNAPMAX+1][NPOWSIM];
extern double PkVecVarSigScale_BigBox[1][NSNAPMAX+1][NPOWSIM];
extern double PkVecVarSigScaleLin_BigBox[1][NSNAPMAX+1][NPOWSIM];

extern double atime_BigBox[1][NSNAPMAX+1];

// L=500 Box

extern int nPkComp;

extern double rkVec[NPOWSIM];
extern double rkVecComp[NPOWSIM];

extern double PkVecFidRun0[NSNAPMAX+1][NPOWSIM];

extern double Corr[NVAR+1][NSNAPMAX+1];
extern double NumCorr[NVAR+1][NSNAPMAX+1];
extern double PkVecVar[NVAR+1][NSNAPMAX+1][NPOWSIM];
extern double PkVecVarScale[NVAR+1][NSNAPMAX+1][NPOWSIM];
extern double PkVecVarScaleLin[NVAR+1][NSNAPMAX+1][NPOWSIM];

extern double PkVecVarSig[NVAR+1][NSNAPMAX+1][NPOWSIM];
extern double PkVecVarSigScale[NVAR+1][NSNAPMAX+1][NPOWSIM];
extern double PkVecVarSigScaleLin[NVAR+1][NSNAPMAX+1][NPOWSIM];
              
extern double atime[NVAR+1][NSNAPMAX+1];
extern double PkCorrFac[NVAR+1][NSNAPMAX+1];
extern double PkCorrFac_BigBox[NVAR+1][NSNAPMAX+1];

extern long long npartTOT;

extern double DiffPkVec[NVAR/2][NSNAPMAX+1][NPOWSIM];
extern double DiffPkVecScale[NVAR/2][NSNAPMAX+1][NPOWSIM];

extern double Diff2PkVec[NVAR/2][NSNAPMAX+1][NPOWSIM];
extern double Diff2PkVecScale[NVAR/2][NSNAPMAX+1][NPOWSIM];

extern double ParFid[NVAR/2];
extern double ParTarget[NVAR/2];
extern double ParDelta[NVAR/2];

extern double rkmin; 
extern double rkmax;

extern double rkminSPFid;
extern double rkmaxSPFid;

extern double rkminSPFid_LargeScale;
extern double rkmaxSPFid_LargeScale;

extern double rkminSPFid_SmallScale;
extern double rkmaxSPFid_SmallScale;

extern double rkminSPVar;
extern double rkmaxSPVar;

extern double rkminSIM;
extern double rkmaxSIM;

//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

// splines for derivatives of the spectra

extern double logrkVec[NPOWSIM];
extern double logrkVec_SuperComp[2*NPOWSIM];

extern double logaexp[NSNAPMAX];
extern double logaexp_SuperComp[NSNAPMAX];

//extern const gsl_interp2d_type *PkDeriv_bicubic = gsl_interp2d_bicubic;

extern struct spline_deriv
{
  double *PkDerivData;
  gsl_spline2d *PkDeriv_bispl;
  gsl_interp_accel *aexp_acc, *rkVec_acc;
} *sp_deriv1, *sp_deriv2;

//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

// splines for the fiducial spectra

extern double *PkFidData_SuperComp;
  
//extern const gsl_interp2d_type *PkFid_bicubic = gsl_interp2d_bicubic;

extern gsl_interp_accel *aexp_acc_SuperComp, *rkVec_acc_SuperComp;

extern gsl_spline2d *PkFid_bispl_SuperComp;

//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

// splines for the fiducial spectra

extern double *PkFidData;
  
//extern const gsl_interp2d_type *PkFid_bicubic = gsl_interp2d_bicubic;

extern gsl_interp_accel *aexp_acc, *rkVec_acc;

extern gsl_spline2d *PkFid_bispl;


//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

extern struct EffSpec_splines
{
  gsl_spline *sp_rknl;
  gsl_spline *sp_neff;
  gsl_spline *sp_ncur;

} *EffSpecVar_spls, *EffSpecTarget_spls;
  
extern gsl_interp_accel *EffSpecVar_acc, *EffSpecTarget_acc ;

//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

extern struct cosmopar
{
  // FLRW model

  int  iFLRW, iCURVE;
  
  // output red; expansion fac  
  
  double  zout, aexp;             
  
  // matter, Dark Energy (DE), baryon, cdm, radiation, neutrino density
  
  double  om_m0, om_DE0, om_b0 ,om_c0, om_r0, om_nu0;
  double  om_m , om_DE , om_b  ,om_c,  om_r,  om_nu;

  double  om_ch20, om_bh20, om_ch2, om_bh2;

  // total density parameter, curvature density, Baryon fraction 
  
  double  om_tot0, om_tot, om_k0, om_k, f_baryon;
  
  // DE Equation of state (EOS) parameters
  
  double  w0, w1;
  
  // hubble parameter @ z=0
  
  double  H0,hh;
  
  // Radius of curvature 
  
  double  Rcur0, Rcur;
  
  // critical density and background density
  
  double  rho_c, rho_0;                
  
  // Newtons Gravitational constant & Speed of light
  
  double  GravCon, cc, rH;
  
  // Spherical model params
  
  double  d_c,a1,a2,a3,a4;
  
  // logaritmic growth of density pertubations
  
  double  fom;

  // power spectral parameters
  double As;
  double pindex;
  double running;
  double Damp;
  
} cp, cp_target;


extern struct global_data
{

  char OutputDir[MAXLEN_FILENAME];            
  char OutputFileBase[MAXLEN_FILENAME];            
  char OutputMPTFileBase[MAXLEN_FILENAME];
  
  char PowDirTarget[MAXLEN_FILENAME];          
  char PowFileTarget[MAXLEN_FILENAME];          

  char PowDirVar[MAXLEN_FILENAME];
  char PowFileBaseVar[MAXLEN_FILENAME];

  char PowFileNameVar[NVAR+1][MAXLEN_FILENAME];
  
} All;



extern struct powerspec
{
  double As;
  double pindex;
  double running;
  double Damp;
  double rkmin;
  double rkmax;

} pkVar, pkTarget;



extern struct halofit
{
  int nPkOut;
  double rkOutMIN;
  double rkOutMAX;
  int iLogOrLin;
  int iGenEffSpecTarget;
  int iGenEffSpecVar;
  double rneff;
  double rncur;
  double rknl;
  double aao;
  double aas;
} hfit;



#endif
