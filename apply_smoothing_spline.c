#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <time.h>

#include <gsl/gsl_bspline.h>
#include <gsl/gsl_multifit.h>
#include <gsl/gsl_rng.h>
#include <gsl/gsl_randist.h>
#include <gsl/gsl_statistics.h>

#include "allvars.h"
#include "proto.h"

//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

void apply_smoothing_spline_fiducial(void)
{

  // number of points in the power spectra data
  size_t n;

  const size_t ncoeffs = NCOEFFS;
  const size_t nbreak = NBREAK;

  size_t i, j, ii, jj;
  gsl_bspline_workspace *bw;
  gsl_vector *B;
  double dy;
  gsl_vector *c, *w;
  gsl_vector *x, *y;
  gsl_matrix *X, *cov;
  gsl_multifit_linear_workspace *mw;
  
  gsl_vector *non_uniform;
  
  int ksp, ivar, isnap, nsnapMAX, nsnapMIN;
  
  double chisq, Rsq, dof, tss, xmin, xmax, rk;
  double xi, yi, wi, Bj, yerr;
  
  ksp = 4; // 4=cubic spline

  xmin = log10(rkminSPFid);
  xmax = log10(rkmaxSPFid);
  
  // Set the range of the data to be BSplined

  n=0;
  for (i = 0; i < nPkComp; ++i)
    {
      rk = rkVecComp[i];
      if((rk>=rkminSPFid) && (rk<=rkmaxSPFid))
	n=n+1;
    }


  /* allocate a cubic bspline workspace */

  bw = gsl_bspline_alloc(ksp, nbreak);

  B = gsl_vector_alloc(ncoeffs);
  x = gsl_vector_alloc(n);
  y = gsl_vector_alloc(n);

  X = gsl_matrix_alloc(n, ncoeffs);
  c = gsl_vector_alloc(ncoeffs);
  w = gsl_vector_alloc(n);

  non_uniform = gsl_vector_alloc(nbreak);

  cov = gsl_matrix_alloc(ncoeffs, ncoeffs);

  mw = gsl_multifit_linear_alloc(n, ncoeffs);
    
  // set the data to be fitted in the x and y vectors
  // set the weight to be 1/sig^2 on each point

  ivar = 0;
  nsnapMAX= NSNAPMAX;
  nsnapMIN= NSNAPMIN;

  for (isnap=nsnapMIN;isnap<=nsnapMAX;isnap++)
    {      

      ii=0;
      for (i = 0; i < nPkComp; ++i)
	{

	  rk = rkVecComp[i];
	  if((rk>=rkminSPFid) && (rk<=rkmaxSPFid))	  
	    {

	      xi = rkVecComp[i];
	      yi = PkVecVarScale[ivar][isnap][i];
	      wi = 1.0/PkVecVarSigScale[ivar][isnap][i];
	      wi = wi*wi;
	      
	      xi = log10(xi);
	      //yi = log10(yi);
	      
	      gsl_vector_set(x, ii, xi);
	      gsl_vector_set(y, ii, yi);
	      gsl_vector_set(w, ii, wi);
	      
	      ii += 1;
	      
	    }
	}

      /* use uniform breakpoints */
      gsl_bspline_knots_uniform(xmin, xmax, bw);
      
      /* construct the fit matrix X */
      for (i = 0; i < n; ++i)
	{

	  xi = gsl_vector_get(x, i);

	  /* compute B_j(xi) for all j */
	  gsl_bspline_eval(xi, B, bw);
	  
	  /* fill in row i of X */
	  for (j = 0; j < ncoeffs; ++j)
	    {
	      Bj = gsl_vector_get(B, j);
	      gsl_matrix_set(X, i, j, Bj);
	    }
	}

      /* do the fit */
      gsl_multifit_wlinear(X, w, y, c, cov, &chisq, mw);
      
      dof = n - ncoeffs;
      tss = gsl_stats_wtss(w->data, 1, y->data, 1, y->size);
      Rsq = 1.0 - chisq / tss;
      
      //fprintf(stderr, "chisq/dof = %e, Rsq = %f\n", 
      //chisq / dof, Rsq);
      
      /* compute the smoothed model of the data */
      {
	
	for (i=0 ; i<nPkComp ; i++)
	  {
	    xi = rkVecComp[i];
	    if( (xi>rkminSPFid) && (xi<rkmaxSPFid) )
	      {
		xi = log10(xi);
		gsl_bspline_eval(xi, B, bw);
		gsl_multifit_linear_est(B, c, cov, &yi, &yerr);
		PkVecVarScale[ivar][isnap][i] = yi;

	      }
	  }
	
      }

    }
      
  gsl_bspline_free(bw);
  gsl_vector_free(B);
  gsl_vector_free(x);
  gsl_vector_free(y);
  gsl_matrix_free(X);
  gsl_vector_free(c);
  gsl_vector_free(w);
  gsl_matrix_free(cov);
  gsl_multifit_linear_free(mw);

}


//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

void apply_smoothing_spline_fiducial_SuperComp_LargeScale(void)
{

  // number of points in the power spectra data
  size_t n;

  const size_t ncoeffs = NCOEFFS_LARGE;
  const size_t nbreak = NBREAK_LARGE;

  size_t i, j, ii, jj;
  gsl_bspline_workspace *bw;
  gsl_vector *B;
  double dy, ytmp;
  gsl_vector *c, *w;
  gsl_vector *x, *y;
  gsl_matrix *X, *cov;
  gsl_multifit_linear_workspace *mw;

  int ksp, ivar, isnap, nsnapMAX, nsnapMIN;
  
  double chisq, Rsq, dof, tss, xmin, xmax, rk;
  double xi, yi, wi, Bj, yerr;
  
  ksp = 4; // 4=cubic spline

  xmin = log10(rkminSPFid_LargeScale);
  xmax = log10(rkmaxSPFid_LargeScale);
  
  // Set the range of the data to be BSplined

  n=0;
  for (i = 0; i < nPkSuperComp; ++i)
    {
      rk = rkVecSuperComp[i];
      if((rk>=rkminSPFid_LargeScale) && (rk<rkmaxSPFid_LargeScale))
	n=n+1;
    }

  /* allocate a cubic bspline workspace */

  bw = gsl_bspline_alloc(ksp, nbreak);

  B = gsl_vector_alloc(ncoeffs);
  x = gsl_vector_alloc(n);
  y = gsl_vector_alloc(n);

  X = gsl_matrix_alloc(n, ncoeffs);
  c = gsl_vector_alloc(ncoeffs);
  w = gsl_vector_alloc(n);

  cov = gsl_matrix_alloc(ncoeffs, ncoeffs);

  mw = gsl_multifit_linear_alloc(n, ncoeffs);
    
  // set the data to be fitted in the x and y vectors
  // set the weight to be 1/sig^2 on each point

  ivar = 0;
  nsnapMAX= NSNAPMAX;
  nsnapMIN= NSNAPMIN;

  for (isnap=nsnapMIN;isnap<=nsnapMAX;isnap++)
    {      

      ii=0;
      for (i = 0; i < nPkSuperComp; ++i)
	{

	  rk = rkVecSuperComp[i];
	  if((rk>=rkminSPFid_LargeScale) && (rk<rkmaxSPFid_LargeScale))	  
	    {
	      
	      xi = rkVecSuperComp[i];
	      yi = PkVecScaleSuperComp[isnap][i];
	      wi = 1.0/PkVecSigScaleSuperComp[isnap][i];
	      wi = wi*wi;
	      
	      xi = log10(xi);
	      //yi = log10(yi);
	      
	      gsl_vector_set(x, ii, xi);
	      gsl_vector_set(y, ii, yi);
	      gsl_vector_set(w, ii, wi);
	      
	      //printf("%3d %3zu %14.7e %14.7e %14.7e\n", isnap, i, xi, yi, wi);
	      
	      ii += 1;
	      
	    }
	}

      /* use uniform breakpoints */
      gsl_bspline_knots_uniform(xmin, xmax, bw);
            
      /* construct the fit matrix X */
      for (i = 0; i < n; ++i)
	{

	  xi = gsl_vector_get(x, i);

	  /* compute B_j(xi) for all j */
	  gsl_bspline_eval(xi, B, bw);
	  
	  /* fill in row i of X */
	  for (j = 0; j < ncoeffs; ++j)
	    {
	      Bj = gsl_vector_get(B, j);
	      gsl_matrix_set(X, i, j, Bj);
	    }
	}

      /* do the fit */
      gsl_multifit_wlinear(X, w, y, c, cov, &chisq, mw);
      
      dof = n - ncoeffs;
      tss = gsl_stats_wtss(w->data, 1, y->data, 1, y->size);
      Rsq = 1.0 - chisq / tss;
      
      //fprintf(stderr, "chisq/dof = %e, Rsq = %f\n", 
      //chisq / dof, Rsq);
      
      /* compute the smoothed model of the data */
      {
	
	for (i=0 ; i<nPkSuperComp ; i++)
	  {
	    xi = rkVecSuperComp[i];
	    if( (xi>=rkminSPFid_LargeScale) && (xi<rkmaxSPFid_LargeScale) )
	      {
		xi = log10(xi);
		ytmp = PkVecScaleSuperComp[isnap][i];
		gsl_bspline_eval(xi, B, bw);
		gsl_multifit_linear_est(B, c, cov, &yi, &yerr);
		PkVecScaleSuperComp[isnap][i] = yi;
		printf("%3zu %14.7e %14.7e %14.7e\n",i,xi,yi,ytmp);

	      }
	  }
	
      }

    }

  gsl_bspline_free(bw);
  gsl_vector_free(B);
  gsl_vector_free(x);
  gsl_vector_free(y);
  gsl_matrix_free(X);
  gsl_vector_free(c);
  gsl_vector_free(w);
  gsl_matrix_free(cov);
  gsl_multifit_linear_free(mw);
  
}


//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

void apply_smoothing_spline_fiducial_SuperComp_SmallScale(void)
{

  FILE *fout;

  char  outfile[180];  

  // number of points in the power spectra data
  size_t n;

  const size_t ncoeffs = NCOEFFS_SMALL;
  const size_t nbreak = NBREAK_SMALL;

  size_t i, j, ii, jj;
  gsl_bspline_workspace *bw;
  gsl_vector *B;
  double dy, ytmp;
  gsl_vector *c, *w;
  gsl_vector *x, *y;
  gsl_matrix *X, *cov;
  gsl_multifit_linear_workspace *mw;

  int ksp, ivar, isnap, nsnapMAX, nsnapMIN;
  
  double chisq, Rsq, dof, tss, xmin, xmax, rk;
  double xi, yi, wi, Bj, yerr;
  
  ksp = 4; // 4=cubic spline

  xmin = log10(rkminSPFid_SmallScale);
  xmax = log10(rkmaxSPFid_SmallScale);
  
  // Set the range of the data to be BSplined

  n=0;
  for (i = 0; i < nPkSuperComp; ++i)
    {
      rk = rkVecSuperComp[i];
      if((rk>=rkminSPFid_SmallScale) && (rk<rkmaxSPFid_SmallScale))
	n=n+1;
    }

  /* allocate a cubic bspline workspace */

  bw = gsl_bspline_alloc(ksp, nbreak);

  B = gsl_vector_alloc(ncoeffs);
  x = gsl_vector_alloc(n);
  y = gsl_vector_alloc(n);

  X = gsl_matrix_alloc(n, ncoeffs);
  c = gsl_vector_alloc(ncoeffs);
  w = gsl_vector_alloc(n);

  cov = gsl_matrix_alloc(ncoeffs, ncoeffs);

  mw = gsl_multifit_linear_alloc(n, ncoeffs);
    
  // set the data to be fitted in the x and y vectors
  // set the weight to be 1/sig^2 on each point

  ivar = 0;
  nsnapMAX= NSNAPMAX;
  nsnapMIN= NSNAPMIN;

  for (isnap=nsnapMIN;isnap<=nsnapMAX;isnap++)
    {      

      ii=0;
      for (i = 0; i < nPkSuperComp; ++i)
	{

	  rk = rkVecSuperComp[i];
	  if((rk>=rkminSPFid_SmallScale) && (rk<rkmaxSPFid_SmallScale))	  
	    {
	      
	      xi = rkVecSuperComp[i];
	      yi = PkVecScaleSuperComp[isnap][i];
	      wi = 1.0/PkVecSigScaleSuperComp[isnap][i];
	      wi = wi*wi;
	      
	      xi = log10(xi);
	      //yi = log10(yi);
	      
	      gsl_vector_set(x, ii, xi);
	      gsl_vector_set(y, ii, yi);
	      gsl_vector_set(w, ii, wi);
	      
	      //printf("%3d %3zu %14.7e %14.7e %14.7e\n", isnap, i, xi, yi, wi);
	      
	      ii += 1;
	      
	    }
	}

      /* use uniform breakpoints */
      gsl_bspline_knots_uniform(xmin, xmax, bw);
            
      /* construct the fit matrix X */
      for (i = 0; i < n; ++i)
	{

	  xi = gsl_vector_get(x, i);

	  /* compute B_j(xi) for all j */
	  gsl_bspline_eval(xi, B, bw);
	  
	  /* fill in row i of X */
	  for (j = 0; j < ncoeffs; ++j)
	    {
	      Bj = gsl_vector_get(B, j);
	      gsl_matrix_set(X, i, j, Bj);
	    }
	}

      /* do the fit */
      gsl_multifit_wlinear(X, w, y, c, cov, &chisq, mw);
      
      dof = n - ncoeffs;
      tss = gsl_stats_wtss(w->data, 1, y->data, 1, y->size);
      Rsq = 1.0 - chisq / tss;
      
      //fprintf(stderr, "chisq/dof = %e, Rsq = %f\n", 
      //chisq / dof, Rsq);
      
      /* compute the smoothed model of the data */
      {
	
	for (i=0 ; i<nPkSuperComp ; i++)
	  {
	    xi = rkVecSuperComp[i];
	    if( (xi>=rkminSPFid_SmallScale) && (xi<rkmaxSPFid_SmallScale) )
	      {
		xi = log10(xi);
		ytmp = PkVecScaleSuperComp[isnap][i];
		gsl_bspline_eval(xi, B, bw);
		gsl_multifit_linear_est(B, c, cov, &yi, &yerr);
		PkVecScaleSuperComp[isnap][i] = yi;
		printf("%3zu %14.7e %14.7e %14.7e\n",i,xi,yi,ytmp);

	      }
	  }
	
      }

    }

  gsl_bspline_free(bw);
  gsl_vector_free(B);
  gsl_vector_free(x);
  gsl_vector_free(y);
  gsl_matrix_free(X);
  gsl_vector_free(c);
  gsl_vector_free(w);
  gsl_matrix_free(cov);
  gsl_multifit_linear_free(mw);
  
  //%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

#ifdef WRITEALLDATA
  
  // Dump the new composite spectra ratio to files:
  
  sprintf(outfile,"%s/PkVecScaleSuperComp_SmoothSplineSmallScale.dat",All.OutputDir);
  printf("%s\n",outfile);
  
  if((fout = fopen(outfile,"w")))
    {
      fprintf(fout,"%d\t%d\t%d\n",nsnapMIN,nsnapMAX,nPkSuperComp);
      for (isnap=nsnapMIN;isnap<=nsnapMAX;isnap++)
	{
	  for (i=0;i<nPkSuperComp;i++)
	    {
	      fprintf(fout,"%+14.7e\t%+14.7e\n",
		      rkVecSuperComp[i],PkVecScaleSuperComp[isnap][i]);
	    }
	}
    }
  else
    {
      printf("\noutfile not found:= %s.\n", outfile);
      endrun(212);	    
    }
  fclose(fout);

#endif

}


//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

void apply_smoothing_spline_deriv_var(void)
{

  // number of points in the power spectra data
  size_t n;

  const size_t ncoeffs = NCOEFFS2;
  const size_t nbreak = NBREAK2;

  size_t i, j, ii, jj;
  gsl_bspline_workspace *bw;
  gsl_vector *B;
  double dy;
  gsl_vector *c, *w;
  gsl_vector *x, *y;
  gsl_matrix *X, *cov;
  gsl_multifit_linear_workspace *mw;

  int ksp, ipar, npar, isnap, nsnapMAX, nsnapMIN;
  
  double chisq, Rsq, dof, tss, xmin, xmax, rk;
  double xi, yi, wi, Bj, yerr;

  ksp = 4; // 4=cubic spline

  xmin = log10(rkminSPVar);
  xmax = log10(rkmaxSPVar);
  
  // Set the range of the data to be BSplined

  n=0;
  for (i = 0; i < nPkComp; ++i)
    {
      rk = rkVecComp[i];
      if((rk>=rkminSPVar) && (rk<=rkmaxSPVar))
	n=n+1;
    }

  /* allocate a cubic bspline workspace */

  bw = gsl_bspline_alloc(ksp, nbreak);

  B = gsl_vector_alloc(ncoeffs);
  x = gsl_vector_alloc(n);
  y = gsl_vector_alloc(n);

  X = gsl_matrix_alloc(n, ncoeffs);
  c = gsl_vector_alloc(ncoeffs);
  w = gsl_vector_alloc(n);

  cov = gsl_matrix_alloc(ncoeffs, ncoeffs);

  mw = gsl_multifit_linear_alloc(n, ncoeffs);
    
  // set the data to be fitted in the x and y vectors
  // set the weight to be 1/sig^2 on each point

  npar=NPAR;
  nsnapMAX= NSNAPMAX;
  nsnapMIN= NSNAPMIN;
  
  for (ipar=0;ipar<npar;ipar++)
    {
      for (isnap=nsnapMIN;isnap<=nsnapMAX;isnap++)
	{      
	  
	  ii=0;
	  for (i = 0; i < nPkComp; ++i)
	    {
	      
	      rk = rkVecComp[i];
	      if((rk>=rkminSPVar) && (rk<=rkmaxSPVar))	  
		{
		  
		  xi = rkVecComp[i];
		  yi = DiffPkVecScale[ipar][isnap][i];
		  wi = 1.0/(0.1);
		  wi = wi*wi;
		  
		  xi = log10(xi);
		  //yi = log10(yi);
		  
		  gsl_vector_set(x, ii, xi);
		  gsl_vector_set(y, ii, yi);
		  gsl_vector_set(w, ii, wi);
		  
		  ii += 1;
		  
		}
	    }
	  
	  /* use uniform breakpoints */
	  gsl_bspline_knots_uniform(xmin, xmax, bw);
	  
	  /* construct the fit matrix X */
	  for (i = 0; i < n; ++i)
	    {
	      
	      xi = gsl_vector_get(x, i);
	      
	      /* compute B_j(xi) for all j */
	      gsl_bspline_eval(xi, B, bw);
	      
	      /* fill in row i of X */
	      for (j = 0; j < ncoeffs; ++j)
		{
		  Bj = gsl_vector_get(B, j);
		  gsl_matrix_set(X, i, j, Bj);
		}
	    }
	  
	  /* do the fit */
	  gsl_multifit_wlinear(X, w, y, c, cov, &chisq, mw);
	  
	  dof = n - ncoeffs;
	  tss = gsl_stats_wtss(w->data, 1, y->data, 1, y->size);
	  Rsq = 1.0 - chisq / tss;
	  
	  //fprintf(stderr, "ipar:= %d, isnap:= %d, chisq/dof = %e, Rsq = %f\n", 
	  //ipar,isnap, chisq / dof, Rsq);
	  
	  /* compute the smoothed model of the data */
	  {
	    
	    for (i=0 ; i<nPkComp ; i++)
	      {
		xi = rkVecComp[i];
		if( (xi>rkminSPVar) && (xi<rkmaxSPVar) )
		  {
		    xi = log10(xi);
		    gsl_bspline_eval(xi, B, bw);
		    gsl_multifit_linear_est(B, c, cov, &yi, &yerr);
		    DiffPkVecScale[ipar][isnap][i] = yi;

		  }
	      }
	  }	
	}
    }  
  gsl_bspline_free(bw);
  gsl_vector_free(B);
  gsl_vector_free(x);
  gsl_vector_free(y);
  gsl_matrix_free(X);
  gsl_vector_free(c);
  gsl_vector_free(w);
  gsl_matrix_free(cov);
  gsl_multifit_linear_free(mw);

}



//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

// smoothing spline to the second order derivatives

void apply_smoothing_spline_deriv2_var(void)
{

  // number of points in the power spectra data
  size_t n;

  const size_t ncoeffs = NCOEFFS2;
  const size_t nbreak = NBREAK2;

  size_t i, j, ii, jj;
  gsl_bspline_workspace *bw;
  gsl_vector *B;
  double dy;
  gsl_vector *c, *w;
  gsl_vector *x, *y;
  gsl_matrix *X, *cov;
  gsl_multifit_linear_workspace *mw;

  int ksp, ipar, npar, isnap, nsnapMAX, nsnapMIN;
  
  double chisq, Rsq, dof, tss, xmin, xmax, rk;
  double xi, yi, wi, Bj, yerr, tmp;

  ksp = 4; // 4=cubic spline

  xmin = log10(rkminSPVar);
  xmax = log10(rkmaxSPVar);
  
  // Set the range of the data to be BSplined

  n=0;
  for (i = 0; i < nPkComp; ++i)
    {
      rk = rkVecComp[i];
      if((rk>=rkminSPVar) && (rk<=rkmaxSPVar))
	n=n+1;
    }

  /* allocate a cubic bspline workspace */

  bw = gsl_bspline_alloc(ksp, nbreak);

  B = gsl_vector_alloc(ncoeffs);
  x = gsl_vector_alloc(n);
  y = gsl_vector_alloc(n);

  X = gsl_matrix_alloc(n, ncoeffs);
  c = gsl_vector_alloc(ncoeffs);
  w = gsl_vector_alloc(n);

  cov = gsl_matrix_alloc(ncoeffs, ncoeffs);

  mw = gsl_multifit_linear_alloc(n, ncoeffs);
    
  // set the data to be fitted in the x and y vectors
  // set the weight to be 1/sig^2 on each point

  npar=NPAR;
  nsnapMAX= NSNAPMAX;
  nsnapMIN= NSNAPMIN;
  
  for (ipar=0;ipar<npar;ipar++)
    {
      for (isnap=nsnapMIN;isnap<=nsnapMAX;isnap++)
	{      
	  
	  ii=0;
	  for (i = 0; i < nPkComp; ++i)
	    {
	      
	      rk = rkVecComp[i];
	      if((rk>=rkminSPVar) && (rk<=rkmaxSPVar))	  
		{
		  
		  xi = rkVecComp[i];
		  yi = Diff2PkVecScale[ipar][isnap][i];
		  wi = 1.0/(0.1);
		  wi = wi*wi;
		  
		  xi = log10(xi);
		  //yi = log10(yi);
		  
		  gsl_vector_set(x, ii, xi);
		  gsl_vector_set(y, ii, yi);
		  gsl_vector_set(w, ii, wi);
		  
		  ii += 1;
		  
		}
	    }
	  
	  /* use uniform breakpoints */
	  gsl_bspline_knots_uniform(xmin, xmax, bw);
	  
	  /* construct the fit matrix X */
	  for (i = 0; i < n; ++i)
	    {
	      
	      xi = gsl_vector_get(x, i);
	      
	      /* compute B_j(xi) for all j */
	      gsl_bspline_eval(xi, B, bw);
	      
	      /* fill in row i of X */
	      for (j = 0; j < ncoeffs; ++j)
		{
		  Bj = gsl_vector_get(B, j);
		  gsl_matrix_set(X, i, j, Bj);
		}
	    }
	  
	  /* do the fit */
	  gsl_multifit_wlinear(X, w, y, c, cov, &chisq, mw);
	  
	  dof = n - ncoeffs;
	  tss = gsl_stats_wtss(w->data, 1, y->data, 1, y->size);
	  Rsq = 1.0 - chisq / tss;
	  
	  /* compute the smoothed model of the data */
	  {
	    
	    for (i=0 ; i<nPkComp ; i++)
	      {
		xi = rkVecComp[i];
		if( (xi>rkminSPVar) && (xi<rkmaxSPVar) )
		  {
		    xi = log10(xi);
		    gsl_bspline_eval(xi, B, bw);
		    gsl_multifit_linear_est(B, c, cov, &yi, &yerr);
		    tmp = Diff2PkVecScale[ipar][isnap][i];
		    Diff2PkVecScale[ipar][isnap][i] = yi;
		  }
	      }
	  }	
	}
    }  
  gsl_bspline_free(bw);
  gsl_vector_free(B);
  gsl_vector_free(x);
  gsl_vector_free(y);
  gsl_matrix_free(X);
  gsl_vector_free(c);
  gsl_vector_free(w);
  gsl_matrix_free(cov);
  gsl_multifit_linear_free(mw);


}



