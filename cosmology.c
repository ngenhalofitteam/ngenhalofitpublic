#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <time.h>
#include <sys/time.h>
#include <sys/resource.h>
#include <sys/types.h>
#include <unistd.h>
#include <signal.h>
#include <gsl/gsl_errno.h>
#include <gsl/gsl_spline.h>


#include "allvars.h"
#include "proto.h"

//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
// cosmology modules
//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

void init_cosmo(void)
{
  double  grow0, grow1;

  // we are assuming a flat cosmological model
  
  // radiation density
  cp.om_r0=0.0;
  cp.om_nu0=0.0;

  // Mater density (assuming flat models)
  cp.om_m0=1.0-cp.om_DE0-cp.om_r0-cp.om_nu0;

  //----------------------
  // Some useful derived cosmological parameters
  cp.hh = sqrt((cp.om_ch20+cp.om_bh20)/cp.om_m0);

  // CDM and baryon density parameter
  cp.om_c0 = cp.om_ch20/cp.hh/cp.hh;
  cp.om_b0 = cp.om_bh20/cp.hh/cp.hh;

  //  total energy density (should total to 1 given flatness)    
  cp.om_tot0 = cp.om_m0 + cp.om_DE0 + cp.om_r0 + cp.om_nu0;
    
  // curvature density parameter (should be zero)
  cp.om_k0 = 1.0 - cp.om_tot0 ;

  // baryon fraction
  cp.f_baryon = cp.om_b0 / cp.om_m0;   

  //  Hubble parameter at z=0       
  cp.H0=100 ;//*hh ;    

  // universal constants
  cp.GravCon=4.301206e-9;  // [km]^(2) [s]^(-1) [Mo]^{-1} [Mpc] 
  cp.cc=3.0e5;             // speed of ligtht [km]/[s]

  // Radius of curvature
  cp.Rcur0=cp.cc/cp.H0*pow((cp.om_tot0-1.0),-0.5);
 
  //----------------------
  // redshift
  cp.zout=1.0/cp.aexp-1.0;

  //------------------------
  // evolution of the matter density, Dark Energy density 
  // and radiation density for present day

  NewOmega(cp.zout);

  //------------------------
  // DENSITY OF THE UNIVERSE    
  cp.rho_c=2.7755e11;   // critical density ([Msol]/[Mpc/h])^3 
  cp.rho_0=cp.om_m*cp.rho_c;  // current density of the Universe   

  //----------------------  
  printf("initialized cosmology\n");
  
  //-------------------------
    
  printf("\n************************************\n");
  printf("COSMOLOGICAL PARAMETERS ARE SET\n");
  printf("**************************************\n");
  printf("They are:\n");
  printf(" (z=0.00) om_m0:= %14.7e, om_DE0:= %14.7e, om_b0:= %14.7e, om_r0:= %14.7e\n",
	 cp.om_m0, cp.om_DE0, cp.om_b0, cp.om_r0);
  printf(" (z=%4.2f)  om_m:= %14.7e,  om_DE:= %14.7e,  om_b:= %14.7e,  om_r:= %14.7e\n",cp.zout,
	 cp.om_m, cp.om_DE, cp.om_b, cp.om_r);
  printf("h:= %14.7e, H0:= %14.7e\n", cp.hh,cp.H0);
  printf("************************************\n");

}

//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

// copy the cosmological parameter structures

void copy_cosmo_target(void)
{

  // equating two structures
  cp_target=cp;

}


//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

// initialise the daemmerung cosmological parameters

void init_cospar_Daemmerung(int iVAR)
{
  
  // fiducial model
  cp.aexp=1.0;
  cp.w0=ParFid[0];
  cp.w1=ParFid[1];
  cp.om_DE0=ParFid[2];
  cp.om_ch20=ParFid[3];
  cp.om_bh20=ParFid[4];
  cp.pindex=ParFid[5];
  cp.As=ParFid[6];
  cp.running=ParFid[7];

  // small hiccup for w0 [pos var was neg var and vice versa]
  if      (iVAR==1)   // w0 Neg variation   
    cp.w0=ParFid[0]-ParDelta[0];    
  else if (iVAR==2)   // w0 Pos variation
    cp.w0=ParFid[0]+ParDelta[0];    
  else if (iVAR==3)   // w1 Neg variation
    cp.w1=ParFid[1]-ParDelta[1];    
  else if (iVAR==4)   // w1 Pos variation
    cp.w1=ParFid[1]+ParDelta[1];    
  else if (iVAR==5)   // w1 Neg variation
    cp.om_DE0=ParFid[2]-ParDelta[2];    
  else if (iVAR==6)   // w1 Pos variation
    cp.om_DE0=ParFid[2]+ParDelta[2];    
  else if (iVAR==7)   // w1 Neg variation
    cp.om_ch20=ParFid[3]-ParDelta[3];    
  else if (iVAR==8)   // w1 Pos variation
    cp.om_ch20=ParFid[3]+ParDelta[3];    
  else if (iVAR==9)   // w1 Neg variation
    cp.om_bh20=ParFid[4]-ParDelta[4];    
  else if (iVAR==10)   // w1 Pos variation
    cp.om_bh20=ParFid[4]+ParDelta[4];    
  else if (iVAR==11)   // w1 Neg variation
    cp.pindex=ParFid[5]-ParDelta[5];    
  else if (iVAR==12)   // w1 Pos variation
    cp.pindex=ParFid[5]+ParDelta[5];    
  else if (iVAR==13)   // w1 Neg variation
    cp.As=ParFid[6]-ParDelta[6];    
  else if (iVAR==14)   // w1 Pos variation
    cp.As=ParFid[6]+ParDelta[6];    
  else if (iVAR==15)   // w1 Neg variation
    cp.running=ParFid[7]-ParDelta[7];    
  else if (iVAR==16)   // w1 Pos variation
    cp.running=ParFid[7]+ParDelta[7];    

  // initialise all derived parameters
  init_cosmo();

}  

//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
//
// EVOLUTION OF THE ENERGY DENSITYS IN MATTER, DARK ENERGY, RADIATION
// AND NEUTRINOS
//
// Equations for cp.omega_m and cp.omega_DE are derived following
// Peacock & Dodds 1996
//
//                        cp.om_{x_i}(a=1) * a^(M_i+3) 
// cp.om_{x_i}(a) =    --------------------------------------------
//                 [sum_j=1^N cp.om_{x_j} a^{M_j+3} - a(1-cp.om_tot0)]
//
//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

// EVOLUTION OF THE MATTER DENSITY

//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

double omega_m(double aa)
{
  double  denom, omega_m;    
  denom = cp.om_m0+cp.om_DE0*DEfunc(aa)
    +(cp.om_r0+cp.om_nu0)/aa-aa*(1.0-cp.om_tot0);
  omega_m=cp.om_m0/denom;
  return omega_m;
  
}
      
//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
      
// EVOLUTION OF THE DARK ENERGY DENSITY

//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

double omega_DE(double aa)
{
  double denom, omega_DE;
  denom = cp.om_m0+cp.om_DE0*DEfunc(aa)
    +(cp.om_r0+cp.om_nu0)/aa-aa*(1.0-cp.om_tot0);
  omega_DE=cp.om_DE0*DEfunc(aa)/denom;
  return omega_DE;
}
    
//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

// EVOLUTION OF THE RADIATION ENERGY DENSITY

//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

double omega_r(double aa)
{    
  double denom, omega_r;
  denom = cp.om_m0+cp.om_DE0*DEfunc(aa)+
    (cp.om_r0+cp.om_nu0)/aa-aa*(1.0-cp.om_tot0);
  omega_r=cp.om_r0/aa/denom;
  return omega_r;
}

//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

// EVOLUTION OF THE NEUTRINO ENERGY DENSITY

//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

double omega_nu(double aa)
{
  double denom, omega_nu;
  denom = cp.om_m0+cp.om_DE0*DEfunc(aa)+
    (cp.om_r0+cp.om_nu0)/aa-aa*(1.0-cp.om_tot0);
  omega_nu=cp.om_nu0/aa/denom;
  return omega_nu;
}    

//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

// VOID TO GENERATE DENSITY PARAMETERS AT DESIRED REDSHIFT

void NewOmega(double zz)
{
  double  aa;

  aa=1.0/(1.+zz) ; 
  cp.om_m=omega_m(aa);
  cp.om_DE=omega_DE(aa);
  cp.om_r=omega_r(aa);
  cp.om_nu=omega_nu(aa);
  cp.om_tot=cp.om_m+cp.om_DE+cp.om_r+cp.om_nu;

  cp.om_b=cp.om_m*cp.f_baryon;
  cp.om_c=cp.om_m*(1.0-cp.f_baryon);
  cp.om_k=1.0-cp.om_m-cp.om_r-cp.om_DE;
    
  cp.om_bh2=cp.om_b*cp.hh*cp.hh;
  cp.om_ch2=cp.om_c*cp.hh*cp.hh;

}

//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

// EVOLUTION OF HUBBLE PARAMETER

//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

double hubble(double aa)
{
  double ind, hubble;
  ind = (cp.om_m0+cp.om_DE0*DEfunc(aa))/aa/aa/aa+(1-cp.om_m0-cp.om_DE0)/aa/aa;
  hubble=cp.H0*pow(ind,0.5);
  return hubble;
}
    
//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

// AUXILLIARY EQUATION FOR EVOLUTION OF HUBBLE AND DE DENSITY

//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

double DEfunc(double aa)
{
  double ind, DEfunc;
  ind =-3.0*(cp.w0+cp.w1);
  DEfunc=pow(aa,ind)*exp(3.0*cp.w1*(aa-1.0));
  return DEfunc;
}

//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

// EVOLUTION OF THE DE EOS

//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

// w(a) = w0 + (1-a) w1

double wEOS(double aa)
{
  double wEOS;
  wEOS=cp.w0+cp.w1*(1.0-aa);
  return wEOS;
}

//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


