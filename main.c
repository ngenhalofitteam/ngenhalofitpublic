#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <time.h>
#include <sys/time.h>
#include <sys/resource.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <signal.h>

#include "allvars.h"
#include "proto.h"

int main(int argc, char **argv)
{

  clock_t t0, t1;
  double CPUTime;
  int i, j, ivar;

  double aa, Da, Da0, Da1, Da2, logaa;
  double Rad, sig8;
  
  if(argc < 3)
    {
      
      printf("Parameters are missing.\n");
      printf("Call with <ParameterFile> and <ExpansionFile>\n");
      endrun(0);

    }

  // timing stats
  t0 = clock();

  //%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  
  // set some key k-ranges
  
  rkmin=0.05;      // k-range over which halofit3 takes information from simulations
  rkmax=10.0;      // on larger scales the model is given by theoretical predictions
  
  rkminSPFid=0.05;    // max/min k-cut for the spline functions 
  rkmaxSPFid=10.0;

  rkminSPFid_LargeScale=0.05;    // max/min k-cut for the spline functions 
  rkmaxSPFid_LargeScale=0.4;

  rkminSPFid_SmallScale=0.4;    // max/min k-cut for the spline functions 
  rkmaxSPFid_SmallScale=10.0;

  rkminSPVar=0.02;    // max/min k-cut for the spline functions 
  rkmaxSPVar=10.0;
  
  rkminSIM=0.0;    // max/min k-cut for loading the simulated power spectra
  rkmaxSIM=10.5;  

  rkBigBoxCut = 0.5; //
  
  //%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  // READ PARAMETER FILE AND OPEN IO FILES

  // get the name of the parameter file
  strcpy(ParameterFile, argv[1]);

  // get the name of the expansion factor file
  strcpy(ExpansionFile, argv[2]);

  // read parameters for the target model
  read_parameter_file(ParameterFile);

  // read list of expansion factors for which power spectra are required
  read_expansion_factors(ExpansionFile);
  
  //%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

  // create some directories that are needed if they are not already there:

  mkdir(All.OutputDir, 02775);    
  mkdir("DATAEffectiveSpectra",02755);

  //%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  // TARGET MODEL INITIALISATION

  // initialize the target cosmological parameters
  init_cosmo();

  // create a copy of the target cosmological parameters
  cp_target=cp;
  
  //  generate growth factors for target model
  gen_growthTarget();

  // read the target linear power sepctrum 
  read_linear_powerspecTarget();

   // compute sigma8 for this cosmological model
  Rad=8.0;
  sig8=compute_sigma(Rad);
  printf("\n*****************************************\n");
  printf("Sigma8 for the target cosmology is: %8.4e\n",sig8);
  printf("*****************************************\n");
 
  // compute the halofit effective spectral parameters for the target model
  GenEffectiveSpecTarget();

#ifdef MPT  
  // compute the Multi-point Propagator Theory (MPT) power spectrum
  init_MPT();
#endif

  //%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  // DAEMMERUNG DATA
  // set fiducial cosmological parameters for simulations and their variations
  setFidParAndDeltaPar();

  // load the linear power spectra data of the Daemmerung runs
  read_linear_powerspecVar();

  // generate growth factors for all the daemmerung simulations
  gen_growthVar();

  // load in the simulated Daemmerung power spectra data
  read_daemmerung_powerspec();
  read_daemmerung_powerspec_BigBox();

  generate_super_composite();

  // calculate how well linear theory is observed on large scales 
  CorrectGrowLargeScaleVar();
  CorrectGrowLargeScaleBigBox();
  
  //%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  // SCALE THE SIMULATED SPECTRA BY HALOFIT
  
  // compute the nonlinear power spectra using halofit
  // for the fiducial and variational simulations
  GenEffectiveSpecVar();

  // rescale all of the nonlinear spectra by halofit
#ifdef SUPERCOMP
  rescale_daemmerung_nonlin_spec_SuperComp();
#endif
  rescale_daemmerung_nonlin_spec();
  rescale_daemmerung_nonlin_spec_BigBox();

#ifdef FIRSTDERIV
  // compute the derivative matrix dy_i/dtheta_j}_theta_0
  printf("\nIN: nonlin_deriv_matrix\n");
  nonlin_deriv_matrix();

#ifdef SECONDDERIV  
  // compute the derivative matrix dy_i/dtheta_j}_theta_0
  printf("\nIN: nonlin_deriv_matrix2\n");
  nonlin_deriv2_matrix();
#endif

#endif

  // %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

  // Apply a smoothing spline to the fiducial data
  printf("\nIN: apply_smoothing_spline\n");
#ifdef SUPERCOMP
  apply_smoothing_spline_fiducial_SuperComp_LargeScale();
  apply_smoothing_spline_fiducial_SuperComp_SmallScale();
#else
  apply_smoothing_spline_fiducial();
#endif

#ifdef FIRSTDERIV
  // Apply a smoothing spline to the derivative data
  printf("\nIN: apply_smoothing_spline to first order derivatives\n");
  apply_smoothing_spline_deriv_var();

#ifdef SECONDDERIV
  // Apply a smoothing spline to the second derivative data
  printf("\nIN: apply_smoothing_spline to second order derivatives\n");
  apply_smoothing_spline_deriv2_var();
#endif

#endif

  //%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  
  // bicubic spline fit to the Fiducial model evolution
  printf("\nIN: spline_spectra_fiducial\n");
#ifdef SUPERCOMP
  spline_spectra_fiducial_SuperComp();
#else  
  spline_spectra_fiducial();
#endif
  
#ifdef FIRSTDERIV
  // bicubic spline fit to the power spectra derivatives
  printf("\nIN: spline_spectra_derivatives\n");
  spline_spectra_derivatives();

#ifdef SECONDDERIV
  // bicubic spline fit to the power spectra second order derivatives
  printf("\nIN: spline_spectra_derivatives2\n");
  spline_spectra_derivatives2();
#endif

#endif

  //%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  // GENERATE NONLINEAR SPECTRA FOR TARGET MODEL

  // set the cosmological parameters to the target ones
  cp=cp_target;

  // set the target vector of parameters
  setParTarget();

  // generate the nonlinear MPT spectra and dump them to a file
#ifdef MPT
  gen_MPT_spectra();
#endif    
    
  // generate the nonlinear spectra and dump to file
  gen_nonlinear_spectra();

  //%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  // clean ending of the program

#ifdef FIRSTDERIV
  // free the memory for spline of the derivative functions
  free_PkDerivSpline();

#ifdef SECONDDERIV
  // free the memory for spline of the derivative functions
  free_PkDeriv2Spline();
#endif
#endif

  // free the memory for spline of fiducial spectra
#ifdef SUPERCOMP
  free_PkFidSpline_SuperComp();
#else
  free_PkFidSpline();
#endif  
  
  // free the memory for the effective spectra splines
  free_EffecSpecVar();
  
  // free memory used in splines of growth factors
  free_GrowVarSplines();

  // free the memory used in the variational power spectra
  free_linear_powerspecVar();

  // free the memory for the effective spectra splines
  free_EffecSpecTarget();
  
  // free the memory used in the target power spectra
  free_linear_powerspecTarget();

  // free memory used in splines of growth factors
  free_GrowTarget_Spline();

  // timing stats
  t1 = clock();

  CPUTime = timediff(t0, t1);

  printf("Running Time:= %e\n",CPUTime);
  
}

//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
