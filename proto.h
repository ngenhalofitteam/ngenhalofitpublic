/*! \file proto.h
 *  \brief this file contains all function prototypes of the code
 */

#ifndef ALLVARS_H
#include "allvars.h"
#endif

//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

// system.c
double timediff(clock_t t0, clock_t t1);
double second(void);
void endrun(int);

//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

// read_parameterfile.c
void read_parameter_file(char *fname);
void read_expansion_factors(char *file);

//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

// cosmology.c
void init_cosmo(void);
void copy_cosmo_target(void);
void init_cospar_Daemmerung(int iVAR);
double omega_m(double aa);
double omega_DE(double aa);
double omega_r(double aa);
double omega_nu(double aa);
void NewOmega(double zz);
double hubble(double aa);
double DEfunc(double aa);
double wEOS(double aa);

//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

// GenGrowthFac.c
void DenGrowVar(double *Da, double a1, double a0,int ivar);
void gen_growthVar(void);
void free_GrowVarSplines(void);
void DenGrowTarget(double *Da, double a1, double a0);
void gen_growthTarget(void);
void free_GrowTarget_Spline(void);
void rk4(double a0,double q1_0,double q2_0,double hstep,
	    double *ystar_1,double *ystar_2);
double func1(double q1,double q2,double aa);
double func2(double q1,double q2,double aa);
double Gamma1(double aa);
double Gamma2(double aa);
double xfunc(double aa);
void CorrectGrowLargeScaleVar(void);
void CorrectGrowLargeScaleBigBox(void);
double CorrectPowGrowEvolveFac(double aaFid, double aaVar, int ivar);

//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

// read_linear_powerspec.c
void read_linear_powerspecVar(void);
double PlinCDMVar(double rk, int iVAR);
void free_linear_powerspecVar(void);
void read_linear_powerspecTarget(void);
double PlinCDMTarget(double rk);
void free_linear_powerspecTarget(void);

//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

//NGenHalofit.c
void setParTarget(void);
void gen_nonlinear_spectra(void);
double NGenHalofit(double rk, double aa, double Plin, double Phfit, double *par);
double Wfilter(double xx, double xxcut, double sigma);
double WSmoothTH(double xx, double xxcut, double sigma);
void halofit2003(double rk, double Plin, double *Pnl, double *Pq, double *Ph);
void halofit2012(double rk, double Plin, double *Pnl, double *Pq, double *Ph);
void EffectiveSpecVar(int iVAR);
void wintVar(double r, double *sig, double *d1, double *d2, int iVAR);
void GenEffectiveSpecVar(void);
void splineEffecSpecVar(void);
void EffecSpecVarSP(double aa, int iVAR);
void free_EffecSpecVar(void);
void EffectiveSpecTarget(void);
void wintTarget(double r, double *sig, double *d1, double *d2);
void GenEffectiveSpecTarget(void);
void splineEffecSpecTarget(void);
void EffecSpecTargetSP(double aa);
void free_EffecSpecTarget(void);

//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

// read_daemmerung_powerspec.c
void read_daemmerung_powerspec(void);
void read_daemmerung_powerspec_BigBox(void);
void generate_super_composite(void);
void rescale_daemmerung_nonlin_spec(void);
void rescale_daemmerung_nonlin_spec_BigBox(void);
void rescale_daemmerung_nonlin_spec_SuperComp(void);
void nonlin_deriv_matrix(void);
void nonlin_deriv2_matrix(void);
void setFidParAndDeltaPar(void);
void correct_sim_spec(void);

//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

// spline_spectra.c
void spline_spectra_fiducial(void);
void spline_spectra_fiducial_SuperComp(void);
void spline_spectra_derivatives(void);
void spline_spectra_derivatives2(void);
void free_PkFidSpline(void);
void free_PkFidSpline_SuperComp(void);
void free_PkDerivSpline(void);
void free_PkDeriv2Spline(void);

//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

// apply_smoothing_spline.c
void apply_smoothing_spline_fiducial(void);
void apply_smoothing_spline_fiducial_SuperComp_LargeScale(void);
void apply_smoothing_spline_fiducial_SuperComp_SmallScale(void);
void apply_smoothing_spline_deriv_var(void);
void apply_smoothing_spline_deriv2_var(void);

//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

// MPTmodule.c
void init_MPT(void);
void gen_MPT_spectra(void);
double Gprop_fk_Fun(double ak,double aa);
double GpropFun(double ak,double aa);
double Gprop_fk_V2_Fun(double ak,double aa);
double GpropFun_V2(double ak,double aa);
double P1loopFun(double ak,double aa);
double P2loopFun(double ak,double aa);
void get_velocity_dispersion(void);
double func(double q, void *par);
double get_propagator(double rk);
double gfunc(double q, void *par);
double af(double q, double rk);
double get_propagator_V2(double ak);
double gfunc_V2(double q, void *par);
double af_V2(double q,double ak);
double get_oneloop(double rk);
double P1loopAfunc(double rq, void *par);
double P1loopBfunc(double xmu, void *par);
double get_twoloop(double rk);
static int P2loopIntegrand(const int *ndim,const cubareal xx[],
			   const int *ncomp,cubareal ff[], void *userdata);
double F3s(double q1,double q2,double q3,double x12,double x23,double x31);
double F3(double q1,double q2,double q3,double x12,double x23,double x31);
double F2(double q1,double q2,double x12);
double G2(double q1,double q2,double x12);

//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

//sigmaMOD.c

double compute_sigma(double R);
double sigmaFunc(double yy, void *par);
double WTopHat(double yy);
