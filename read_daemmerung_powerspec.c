#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <time.h>
#include <sys/time.h>
#include <sys/resource.h>
#include <sys/types.h>
#include <unistd.h>
#include <signal.h>
#include <gsl/gsl_errno.h>
#include <gsl/gsl_spline.h>

#include "allvars.h"
#include "proto.h"

//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

void read_daemmerung_powerspec(void)
{

  FILE *fPk, *fout;

  char root[180], prefile[180], infile[180], outfile[180];

  int nrun, irun, isnap, nPk, iPk, nPk2, iPk2;
  int ivar, nvar, num, nsnapMAX, nsnapMIN,index;
  
  double mass, mass2, pi, dfac, aa, zz;

  double Pk1, Pk2, rk,d2,shot,ModePk,ModeCount,D2PowUnCorr,ModePowUnCorr;
  double SpecShape,SumPower,ConvFac, Plin;

  double Pk1NEW, Pk2NEW, Pk1INIT, Pk2INIT, PlinINIT, DampINIT, aaINIT;
  double rkcut, sigma, Wf;
  int indexINIT;

  double rkNy, rksplit;

  double *PkVecFid1;
  double *PkVecFid2;

  double *PkVecVar1;
  double *PkVecVar2;

  double PkVecFidBar1[NSNAPMAX+1][NPOWSIM];
  double PkVecFidBar2[NSNAPMAX+1][NPOWSIM];

  double PkVecFidSumSq1[NSNAPMAX+1][NPOWSIM];
  double PkVecFidSumSq2[NSNAPMAX+1][NPOWSIM];

  double PkVecFidVar1[NSNAPMAX+1][NPOWSIM];
  double PkVecFidVar2[NSNAPMAX+1][NPOWSIM];

  double PkVecFidRun0tmp[NSNAPMAX+1][NPOWSIM];

  double atimeFid[NRUN][NSNAPMAX+1];

  double growFacCorr, growFacCorr2, aaFid, aaVar;
   
  //%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

  // read the fiducial power spectra

  printf("\nReading the daemmerung power spectra data\n");
  printf("Load: Fiducial Spectra\n");

  pi = M_PI;
  
  nrun = NRUN;
  nsnapMAX = NSNAPMAX;
  nsnapMIN = NSNAPMIN;
  nPk = NPOWSIM;

  // allocate memory arrays

  num=nrun*(nsnapMAX+1)*nPk;

  if(!(PkVecFid1 = (double *) malloc(sizeof(double)*num)))
    {
      printf("failed to allocate memory for PkVecFid1\n");
      endrun(11);
    }

  if(!(PkVecFid2 = (double *) malloc(sizeof(double)*num)))
    {
      printf("failed to allocate memory for PkVecFid1\n");
      endrun(12);
    }
      
  //%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

  ivar = 0;
  for (irun=0; irun < nrun; irun++)
    {

      sprintf(prefile,"DATADaemmerungPowerSpec/Planck2013-Npart_2048_Box_500-Fiducial/run%d/",irun+1);
      
      printf("opening:= %s\n",prefile);
	  
      for (isnap=nsnapMIN ; isnap<=nsnapMAX ; isnap++)
	{

	  if(isnap<10)
	    sprintf(infile,"%spowerspec_00%d.txt",prefile,isnap);
	  else
	    sprintf(infile,"%spowerspec_0%d.txt",prefile,isnap);

	  if((fPk = fopen(infile, "r"))==NULL)
	    {
	      printf("\nPower spectrum file not found %s.\n", infile);
	      endrun(2);	    
	    }

	  // get the first chaining mesh
	  fscanf(fPk,"%le",&aa);
	  fscanf(fPk,"%d",&nPk);
	  fscanf(fPk,"%le",&mass);
	  fscanf(fPk,"%lld",&npartTOT);

	  if(irun==0)
	    atime[ivar][isnap]=aa;
	  
	  atimeFid[irun][isnap]=aa;
	  
	  for (iPk=0; iPk<nPk; iPk++)
	    {
	      
	      fscanf(fPk,"%lf %lf %lf %le %le %lf %le %le %le %le",
		     &rk,&d2,&shot,&ModePk,&ModeCount,&D2PowUnCorr,&ModePowUnCorr,
		     &SpecShape,&SumPower,&ConvFac);

	      rkVec[iPk]=rk;

	      dfac = 4.0*pi*rk*rk*rk/(2.0*pi)/(2.0*pi)/(2.0*pi);

	      Plin = d2/dfac;

	      index=irun*(nsnapMAX+1)*nPk+isnap*nPk+iPk;

	      PkVecFid1[index]=Plin;

	    }

	  // get the second chaining mesh
	  fscanf(fPk,"%le",&aa);
	  fscanf(fPk,"%d",&nPk2);
	  fscanf(fPk,"%le",&mass2);
	  fscanf(fPk,"%lld",&npartTOT);

	  //printf("%14.7le  %3d  %14.7le  %lld\n",atime[ivar][isnap],nPk2,mass2,npartTOT);
	  
	  for (iPk=0; iPk<nPk2; iPk++)
	    {
	      
	      fscanf(fPk,"%lf %lf %lf %le %le %lf %le %le %le %le",
		     &rk,&d2,&shot,&ModePk,&ModeCount,&D2PowUnCorr,&ModePowUnCorr,
		     &SpecShape,&SumPower,&ConvFac);

	      rkVec[iPk]=rk;

	      dfac = 4.0*pi*rk*rk*rk/(2.0*pi)/(2.0*pi)/(2.0*pi);

	      Plin = d2/dfac;

	      index=irun*(nsnapMAX+1)*nPk+isnap*nPk+iPk;
	      PkVecFid2[index]=Plin;

	      if(irun==0) 
		PkVecFidRun0tmp[isnap][iPk]=Plin;
		
	    }
	  
	  fclose(fPk);

	}
    }
  
  //%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  // rescale the large-scale modes to remove the cosmic variance
  
#ifdef NOCOSMICVARIANCE

  ivar=0;
  for (irun=0; irun<nrun; irun++)
    for (isnap=nsnapMIN+1;isnap<=nsnapMAX;isnap++)
      for (iPk=0;iPk<nPk;iPk++)
	{
	  
	  // set the expansion factor of the currnet snapshot and the initial file
	  aa = atimeFid[irun][isnap];
	  aaINIT = atimeFid[irun][nsnapMIN];

	  // wavenumber
	  rk = rkVec[iPk];
	  
	  // compute the linear theory spectra
	  cp.aexp=aa;
	  cp.zout=1.0/cp.aexp-1.0;
	  
	  // calculate the growth factor for the power spectrum at aaINIT
	  DenGrowVar(&DampINIT,aaINIT,1.0,ivar);

	  pkVar.Damp=DampINIT;	  	  

	  // get the linear power spectrum
	  PlinINIT = PlinCDMVar(rk,ivar);
		  
	  index=irun*(nsnapMAX+1)*nPk+isnap*nPk+iPk;
	  indexINIT=irun*(nsnapMAX+1)*nPk+nsnapMIN*nPk+iPk;

	  Pk1INIT = PkVecFid1[indexINIT];
	  Pk2INIT = PkVecFid2[indexINIT];

	  Pk1 = PkVecFid1[index] ;
	  Pk2 = PkVecFid2[index] ;

	  // Use a transition function Wf 	  
	  rkcut=0.2;
	  sigma=0.05;

	  Wf = Wfilter(log10(rk),log10(rkcut),sigma);

	  // rescale the power spectra	    
	  if (Pk1INIT<=0.0) Pk1INIT=PlinINIT;
	  
	  //if(Pk1INIT>0.0 && Pk1>0.0) 
	  {
	    Pk1NEW = Pk1 * (PlinINIT/Pk1INIT) * Wf + Pk1 * (1.0-Wf);
	    PkVecFid1[index]=Pk1NEW;
	  }
	  
	  if (Pk2INIT<=0.0) Pk2INIT=PlinINIT;
	  //if(Pk2INIT>0.0 && Pk2>0.0)
	  {
	    Pk2NEW = Pk2 * (PlinINIT/Pk2INIT) * Wf + Pk2 * (1.0-Wf);
	    PkVecFid2[index]=Pk2NEW;
	  }

	}

#endif

  //%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  // correct all of the vatiational spectra to the same output redshifts
  // in this case we use the run1 fiducial data as a reference

#ifdef SYNCSPECTRA
  
  ivar=0;
  for (isnap=nsnapMIN;isnap<=nsnapMAX;isnap++)
    for (iPk=0;iPk<nPk;iPk++)
      for (irun=0; irun<nrun; irun++)
	{
	  
	  aaFid = atime[0][isnap];
	  aaVar = atimeFid[irun][isnap];
	  
	  growFacCorr=CorrectPowGrowEvolveFac(aaFid,aaVar,ivar);
	  growFacCorr2 = growFacCorr*growFacCorr;
	  
	  index=irun*(nsnapMAX+1)*nPk+isnap*nPk+iPk;
	  PkVecFid1[index] = growFacCorr2*PkVecFid1[index];
	  PkVecFid2[index] = growFacCorr2*PkVecFid2[index];
	  
	}

#endif
  
  //%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  // compute the ensemble average of the power spectra

  for (isnap=nsnapMIN;isnap<=nsnapMAX;isnap++)
  for (iPk=0;iPk<nPk;iPk++)
    {
      PkVecFidBar1[isnap][iPk]=0;
      PkVecFidBar2[isnap][iPk]=0;
      PkVecFidSumSq1[isnap][iPk]=0;
      PkVecFidSumSq2[isnap][iPk]=0;
      PkVecFidVar1[isnap][iPk]=0;
      PkVecFidVar2[isnap][iPk]=0;
    }
  

  for (isnap=nsnapMIN;isnap<=nsnapMAX;isnap++)
    for (iPk=0;iPk<nPk;iPk++)
      {

	for (irun=0; irun<nrun; irun++)
	  {
	    
	    index=irun*(nsnapMAX+1)*nPk+isnap*nPk+iPk;
	    Pk1 = PkVecFid1[index];
	    Pk2 = PkVecFid2[index];

	    // mean
	    PkVecFidBar1[isnap][iPk]=PkVecFidBar1[isnap][iPk]+Pk1;
	    PkVecFidBar2[isnap][iPk]=PkVecFidBar2[isnap][iPk]+Pk2;

	    // sum of squares
	    PkVecFidSumSq1[isnap][iPk]=PkVecFidSumSq1[isnap][iPk]+Pk1*Pk1;
	    PkVecFidSumSq2[isnap][iPk]=PkVecFidSumSq2[isnap][iPk]+Pk2*Pk2;
	  }
	
      }
  
  for (isnap=nsnapMIN;isnap<=nsnapMAX;isnap++)
  for (iPk=0;iPk<nPk;iPk++)
    {
      PkVecFidBar1[isnap][iPk]=PkVecFidBar1[isnap][iPk]/(1.0*nrun);
      PkVecFidBar2[isnap][iPk]=PkVecFidBar2[isnap][iPk]/(1.0*nrun);
      // sum of squares
      PkVecFidSumSq1[isnap][iPk]=PkVecFidSumSq1[isnap][iPk]/(1.0*nrun);
      PkVecFidSumSq2[isnap][iPk]=PkVecFidSumSq2[isnap][iPk]/(1.0*nrun);
      // variance 
      PkVecFidVar1[isnap][iPk]=PkVecFidSumSq1[isnap][iPk]-pow(PkVecFidBar1[isnap][iPk],2.0);
      PkVecFidVar2[isnap][iPk]=PkVecFidSumSq2[isnap][iPk]-pow(PkVecFidBar2[isnap][iPk],2.0);
    }

  //%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  // create the composite spectrum

  printf("Generating fiducial Composite Spectrum\n");

  rkNy = pi*2048/500.0;
  rksplit= rkNy/2.0;   // scale where aliasing is an issue
  rksplit= 1000.0*rkNy;   // scale where aliasing is an issue
 
  ivar=0;
  irun = 1;
  
  iPk2=0;
  for (isnap=nsnapMIN;isnap<=nsnapMAX;isnap++)
    {
      for (iPk=0;iPk<nPk;iPk++)
	{

	  rk = rkVec[iPk];
	  Pk2 = PkVecFidBar2[isnap][iPk];
	  Pk1 = PkVecFidBar1[isnap][iPk];
	  
	  if( (Pk2>0) && (rk<rkmaxSIM))
	    {

	      rkVecComp[iPk2]=rk;

	      if(rk<rksplit)
		{
		  PkVecVar[ivar][isnap][iPk2]=Pk2;		  

		  if(PkVecFidVar2[isnap][iPk]>0.0)
		    PkVecVarSig[ivar][isnap][iPk2]=sqrt(PkVecFidVar2[isnap][iPk]);
		  else
		    PkVecVarSig[ivar][isnap][iPk2]=0.0;
		}
	      else
		{
		  PkVecVar[ivar][isnap][iPk2]=Pk1;
		  if(PkVecFidVar1[isnap][iPk]>0.0)
		    PkVecVarSig[ivar][isnap][iPk2]=sqrt(PkVecFidVar1[isnap][iPk]);
		  else
		    PkVecVarSig[ivar][isnap][iPk2]=0.0;
		}
	      
	      PkVecFidRun0[isnap][iPk2]=PkVecFidRun0tmp[isnap][iPk];
	      
	      iPk2=iPk2+1; 
	      
	    }
	}  
      nPkComp=iPk2;
      iPk2=0;

    }

  free(PkVecFid2);
  free(PkVecFid1);

  //%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

#ifdef WRITEALLDATA
  // Dump the fiducial composite spectra to files:
  sprintf(outfile,"%s/PkFidComp.dat",All.OutputDir);
  printf("%s\n",outfile);
 
  ivar=0;
  if((fout = fopen(outfile,"w")))
    {
      fprintf(fout,"%d\t%d\t%d\n",nsnapMIN,nsnapMAX,nPkComp);
      for (isnap=nsnapMIN;isnap<=nsnapMAX;isnap++)
      for (iPk=0;iPk<nPkComp;iPk++)
	{
	  fprintf(fout,"%+14.7e\t%+14.7e\t%+14.7e\n",
		  rkVecComp[iPk],PkVecVar[ivar][isnap][iPk],PkVecVarSig[ivar][isnap][iPk]);
	}
    }
  else
    {
      printf("\noutfile not found:= %s.\n", outfile);
      endrun(212);	    
    }
  fclose(fout);
#endif
  
  //%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

  printf("output redshifts:=\n");
  for (isnap=nsnapMIN; isnap<=nsnapMAX; isnap++)
    printf("isnap:= %3d , aexp:= %+14.7e, zout:= %+14.7e\n",
	   isnap,atime[0][isnap],1.0/atime[0][isnap]-1.0);
  
  //%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

  // read the variatonal power spectra

  printf("Load: Variational Spectra\n");

  nvar=NVAR;

  // allocate memory arrays

  num=(nvar+1)*(nsnapMAX+1)*nPk;

  if(!(PkVecVar1 = (double *) malloc(sizeof(double)*num)))
    {
      printf("failed to allocate memory for PkVecFid1\n");
      endrun(13);
    }

  if(!(PkVecVar2 = (double *) malloc(sizeof(double)*num)))
    {
      printf("failed to allocate memory for PkVecFid1\n");
      endrun(14);
    }

  // load data      
  /*
    par(1)=w0
    par(2)=wa
    par(3)=om_DE
    par(4)=omch2
    par(5)=ombh2
    par(6)=spectral index
    par(7)=Scalar amp
    par(8)=Scalar Running
  */

  for (ivar=0; ivar<=nvar; ivar++)
    {
      
      if(ivar==0)
        sprintf(root,"Planck2013-Npart_2048_Box_500-Fiducial/");
      else if(ivar==1)
        sprintf(root,"Planck2013-Npart_2048_Box_500_w0-NegVar/");
      else if(ivar==2)
        sprintf(root,"Planck2013-Npart_2048_Box_500_w0-PosVar/");
      else if(ivar==3)
        sprintf(root,"Planck2013-Npart_2048_Box_500_w1-NegVar/");
      else if(ivar==4)
        sprintf(root,"Planck2013-Npart_2048_Box_500_w1-PosVar/");
      else if(ivar==5) 
	sprintf(root,"Planck2013-Npart_2048_Box_500_om_DE-NegVar/");
      else if(ivar==6)
        sprintf(root,"Planck2013-Npart_2048_Box_500_om_DE-PosVar/");
      else if(ivar==7)
        sprintf(root,"Planck2013-Npart_2048_Box_500_omch2-NegVar/");
      else if(ivar==8)
        sprintf(root,"Planck2013-Npart_2048_Box_500_omch2-PosVar/");
      else if(ivar==9)
        sprintf(root,"Planck2013-Npart_2048_Box_500_ombh2-NegVar/");
      else if(ivar==10)
        sprintf(root,"Planck2013-Npart_2048_Box_500_ombh2-PosVar/");
      else if(ivar==11)
        sprintf(root,"Planck2013-Npart_2048_Box_500_ns-NegVar/");
      else if(ivar==12)
        sprintf(root,"Planck2013-Npart_2048_Box_500_ns-PosVar/");
      else if(ivar==13)
        sprintf(root,"Planck2013-Npart_2048_Box_500_As-NegVar/");
      else if(ivar==14)
        sprintf(root,"Planck2013-Npart_2048_Box_500_As-PosVar/");
      else if(ivar==15)
        sprintf(root,"Planck2013-Npart_2048_Box_500_running-NegVar/");
      else if(ivar==16)
        sprintf(root,"Planck2013-Npart_2048_Box_500_running-PosVar/");
      
      sprintf(prefile,"DATADaemmerungPowerSpec/%s/run1/",root);
      printf("opening:= %s\n",prefile);	  
      
      for (isnap=nsnapMIN ; isnap<=nsnapMAX ; isnap++)
	{
	  
	  if(isnap<10)
	    sprintf(infile,"%spowerspec_00%d.txt",prefile,isnap);
	  else
	    sprintf(infile,"%spowerspec_0%d.txt",prefile,isnap);
	  
	  if((fPk = fopen(infile, "r"))==NULL)
	    {
	      printf("\nPower spectrum file not found %s.\n", infile);
	      endrun(2);	    
	    }
	  
	  // get the first chaining mesh
	  fscanf(fPk,"%le",&atime[ivar][isnap]);
	  fscanf(fPk,"%d",&nPk);
	  fscanf(fPk,"%le",&mass);
	  fscanf(fPk,"%lld",&npartTOT);
	  
	  for (iPk=0; iPk<nPk; iPk++)
	    {
	      
	      fscanf(fPk,"%lf %lf %lf %le %le %lf %le %le %le %le",
		     &rk,&d2,&shot,&ModePk,&ModeCount,&D2PowUnCorr,&ModePowUnCorr,
		     &SpecShape,&SumPower,&ConvFac);
	      
	      rkVec[iPk]=rk;
	      dfac = 4.0*pi*rk*rk*rk/(2.0*pi)/(2.0*pi)/(2.0*pi);

	      index=ivar*(nsnapMAX+1)*nPk+isnap*nPk+iPk;
	      PkVecVar1[index]=d2/dfac;
	      
	    }
	  
	  // get the second chaining mesh
	  fscanf(fPk,"%le",&atime[ivar][isnap]);
	  fscanf(fPk,"%d",&nPk2);
	  fscanf(fPk,"%le",&mass2);
	  fscanf(fPk,"%lld",&npartTOT);
	  
	  //printf("%14.7le  %3d  %14.7le  %lld\n",atime[ivar][isnap],nPk2,mass2,npartTOT);
	  
	  for (iPk=0; iPk<nPk2; iPk++)
	    {
	      
	      fscanf(fPk,"%lf %lf %lf %le %le %lf %le %le %le %le",
		     &rk,&d2,&shot,&ModePk,&ModeCount,&D2PowUnCorr,&ModePowUnCorr,
		     &SpecShape,&SumPower,&ConvFac);
	      
	      rkVec[iPk]=rk;
	      dfac = 4.0*pi*rk*rk*rk/(2.0*pi)/(2.0*pi)/(2.0*pi);
	      
	      index=ivar*(nsnapMAX+1)*nPk+isnap*nPk+iPk;
	      PkVecVar2[index]=d2/dfac;

	    }
	  
	  fclose(fPk);
	  
	}
      
    }


  //%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  // rescale the large-scale modes to remove the cosmic variance
  
#ifdef NOCOSMICVARIANCE

  for (ivar=0;ivar<=nvar;ivar++)
    for (isnap=nsnapMIN+1;isnap<=nsnapMAX;isnap++)
      for (iPk=0;iPk<nPk;iPk++)
	{

	  // set the expansion factor of the currnet snapshot and the initial file
	  aa = atime[ivar][isnap];
	  aaINIT = atime[ivar][nsnapMIN];

	  // wavenumber
	  rk = rkVec[iPk];
	  
	  // compute the linear theory spectra
	  cp.aexp=aa;
	  cp.zout=1.0/cp.aexp-1.0;
	  
	  // calculate the growth factor for the power spectrum at aaINIT
	  DenGrowVar(&DampINIT,aaINIT,1.0,ivar);

	  pkVar.Damp=DampINIT;	  	  

	  // get the linear power spectrum
	  PlinINIT = PlinCDMVar(rk,ivar);
		 
	  index=ivar*(nsnapMAX+1)*nPk+isnap*nPk+iPk;
	  indexINIT=ivar*(nsnapMAX+1)*nPk+nsnapMIN*nPk+iPk;

	  Pk1INIT = PkVecVar1[indexINIT];
	  Pk2INIT = PkVecVar2[indexINIT];

	  Pk1 = PkVecVar1[index] ;
	  Pk2 = PkVecVar2[index] ;

	  // Use a transition function Wf 	  
	  rkcut=0.2;
	  sigma=0.05;

	  Wf = Wfilter(log10(rk),log10(rkcut),sigma);

	  if (Pk1INIT<=0.0) Pk1INIT=PlinINIT;
	  // rescale the power spectra	    
	  //if(Pk1INIT>0.0 && Pk1>0.0) 
	    {
	      Pk1NEW = Pk1 * (PlinINIT/Pk1INIT) * Wf + Pk1 * (1.0-Wf);
	      PkVecVar1[index]=Pk1NEW;
	    }
	    
	  if (Pk2INIT<=0.0) Pk2INIT=PlinINIT;
	  //if(Pk2INIT>0.0 && Pk2>0.0)
	    {
	      Pk2NEW = Pk2 * (PlinINIT/Pk2INIT) * Wf + Pk2 * (1.0-Wf);
	      PkVecVar2[index]=Pk2NEW;
	    }
	}

#endif

  //%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  // correct all of the vatiational spectra to the same output redshifts
  // in this case we use the run1 fiducial data as a reference

#ifdef SYNCSPECTRA

  for (ivar=0;ivar<=nvar;ivar++)
    for (isnap=nsnapMIN;isnap<=nsnapMAX;isnap++)
      for (iPk=0;iPk<nPk;iPk++)
	{
	  aaFid = atime[0][isnap];
	  aaVar = atime[ivar][isnap];

	  growFacCorr=CorrectPowGrowEvolveFac(aaFid,aaVar,ivar);
	  growFacCorr2 = growFacCorr*growFacCorr;
	    	    
	  index=ivar*(nsnapMAX+1)*nPk+isnap*nPk+iPk;
	  
	  PkVecVar1[index] = growFacCorr2*PkVecVar1[index];
	  PkVecVar1[index] = growFacCorr2*PkVecVar2[index];

	}

#endif
  
  //%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  // create the composite spectrum

  printf("Generating variational Composite Spectrum\n");
      
  rkNy = pi*2048/500.0;
  rksplit= 1000*rkNy/2.0;   // scale where aliasing is an issue
  
  iPk2=0;
  for (ivar=0;ivar<=nvar;ivar++)
  for (isnap=nsnapMIN;isnap<=nsnapMAX;isnap++)
    {
      for (iPk=0;iPk<nPk;iPk++)
	{
	  
	  index=ivar*(nsnapMAX+1)*nPk+isnap*nPk+iPk;

	  rk = rkVec[iPk];
	  Pk1=PkVecVar1[index];
	  Pk2=PkVecVar2[index];
	  
	  if( (Pk2>0.0) && (rk<rkmaxSIM))        
	    {
	      
	      rkVecComp[iPk2]=rk;
	  
	      if(rk<rksplit)
		PkVecVar[ivar][isnap][iPk2]=Pk2;
	      else
		PkVecVar[ivar][isnap][iPk2]=Pk1;
	      
	      iPk2=iPk2+1;

	    }
	}
      nPkComp=iPk2;
      iPk2=0;

    }

  //%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

#ifdef CORRECTLARGESCALES
  // make a small correction to the simulated spectra to
  // ensure that the modes on large scales from the variational
  // runs are in agreement with linear theory. 
  correct_sim_spec();
#endif
  
  //%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  
  // Dump the variational composite spectra to files:
  
#ifdef WRITEALLDATA
  for (ivar=0; ivar<=nvar;ivar++)
    {

      sprintf(outfile,"%s/PkVarComp.%d.dat",All.OutputDir,ivar);
      printf("%s\n",outfile);
  
      if((fout = fopen(outfile,"w")))
	{
	  fprintf(fout,"%d\t%d\t%d\n",nsnapMIN,nsnapMAX,nPkComp);
	  for (isnap=nsnapMIN;isnap<=nsnapMAX;isnap++)
	  for (iPk=0;iPk<nPkComp;iPk++)
	    {
	      fprintf(fout,"%+14.7e\t%+14.7e\t%+14.7e\n",
		      rkVecComp[iPk],PkVecVar[ivar][isnap][iPk],PkVecVarSig[ivar][isnap][iPk]);
	    }
	}
      else
	{
	  printf("\noutfile not found:= %s.\n", outfile);
	  endrun(212);	    
	}
      fclose(fout);
    }
#endif

  //%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

#ifdef WRITEALLDATA
  // write out isnap, atime[ivar][isnap], zout[isnap]
  sprintf(outfile,"%s/SNAP_a_z.dat",All.OutputDir);
  printf("%s\n",outfile);
  
  if((fout = fopen(outfile,"w")))
    {
      fprintf(fout,"%d\t%d\n",nsnapMIN,nsnapMAX);
      for (isnap=nsnapMIN;isnap<=nsnapMAX;isnap++)
	{
	  //printf("%4d\t",isnap);
	  fprintf(fout,"%4d\t",isnap);
	  for (ivar=0;ivar<=nvar;ivar++)
	    {
	      aa = atime[ivar][isnap];
	      zz = 1.0/aa-1.0;
	      //printf("%+14.7e\t",aa);
      	      fprintf(fout,"%+14.7e\t",aa);
	    }
	  //printf("\n");
	  fprintf(fout,"\n");
	}
    }
  else
    {
      printf("\noutfile not found:= %s.\n", outfile);
      endrun(211);	    
    }
  fclose(fout);
#endif

  //%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

  free(PkVecVar1);
  free(PkVecVar2);

}

//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

void read_daemmerung_powerspec_BigBox(void)
{

  FILE *fPk, *fout;

  char root[180], prefile[180], infile[180], outfile[180];

  int nrun, irun, isnap, nPk, iPk, nPk2, iPk2;
  int ivar, nvar, num, nsnapMAX, nsnapMIN,index;
  
  double mass, mass2, pi, dfac, aa, zz, WW;

  double Pk1, Pk2, rk,d2,shot,ModePk,ModeCount,D2PowUnCorr,ModePowUnCorr;
  double SpecShape,SumPower,ConvFac, Plin;
  double rkNy, rksplit, BoxSize;
  double Pk, DeltaLog10k, rkLo, rkHi, Deltak, yy, Vk, Nk, rkf, nbar;
  double aaFid, aaVar, growFacCorr, growFacCorr2;

  double Pk1NEW, Pk2NEW, Pk1INIT, Pk2INIT, PlinINIT, DampINIT, aaINIT;
  double rkcut, sigma, Wf;
  int indexINIT;
  
  double *PkVecFid1;
  double *PkVecFid2;

  double PkVecFidBar1[NSNAPMAX+1][NPOWSIM];
  double PkVecFidBar2[NSNAPMAX+1][NPOWSIM];

  double PkVecFidSumSq1[NSNAPMAX+1][NPOWSIM];
  double PkVecFidSumSq2[NSNAPMAX+1][NPOWSIM];

  double PkVecFidVar1[NSNAPMAX+1][NPOWSIM];
  double PkVecFidVar2[NSNAPMAX+1][NPOWSIM];

  //%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

  // read the fiducial power spectra

  printf("\nReading the daemmerung power spectra data for the L=3000 Box\n");
  printf("Load: Fiducial Spectra\n");

  pi = M_PI;

  BoxSize = 3000.0;
  
  nrun = 1;
  nsnapMAX = NSNAPMAX;
  nsnapMIN = NSNAPMIN;
  nPk = NPOWSIM;

  // allocate memory arrays

  num=nrun*(nsnapMAX+1)*nPk;

  if(!(PkVecFid1 = (double *) malloc(sizeof(double)*num)))
    {
      printf("failed to allocate memory for PkVecFid1\n");
      endrun(11);
    }

  if(!(PkVecFid2 = (double *) malloc(sizeof(double)*num)))
    {
      printf("failed to allocate memory for PkVecFid1\n");
      endrun(12);
    }
      
  //%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

  ivar = 0;
  for (irun=0; irun < nrun; irun++)
    {

      sprintf(prefile,"DATADaemmerungPowerSpec/Planck2013-Npart_2048_Box_3000-Fiducial/run%d/",irun+1);
      
      printf("opening:= %s\n",prefile);
	  
      for (isnap=nsnapMIN ; isnap<=nsnapMAX ; isnap++)
	{

	  if(isnap<10)
	    sprintf(infile,"%spowerspec_00%d.txt",prefile,isnap);
	  else
	    sprintf(infile,"%spowerspec_0%d.txt",prefile,isnap);

	  if((fPk = fopen(infile, "r"))==NULL)
	    {
	      printf("\nPower spectrum file not found %s.\n", infile);
	      endrun(2);	    
	    }

	  // get the first chaining mesh
	  fscanf(fPk,"%le",&atime_BigBox[ivar][isnap]);
	  fscanf(fPk,"%d",&nPk);
	  fscanf(fPk,"%le",&mass);
	  fscanf(fPk,"%lld",&npartTOT);
	  
	  //printf("%14.7e  %3d  %14.7e  %lld\n",atime[ivar][isnap],nPk,mass,npartTOT);
	  
	  for (iPk=0; iPk<nPk; iPk++)
	    {
	      
	      fscanf(fPk,"%lf %lf %lf %le %le %lf %le %le %le %le",
		     &rk,&d2,&shot,&ModePk,&ModeCount,&D2PowUnCorr,&ModePowUnCorr,
		     &SpecShape,&SumPower,&ConvFac);

	      rkVec_BigBox[iPk]=rk;

	      dfac = 4.0*pi*rk*rk*rk/(2.0*pi)/(2.0*pi)/(2.0*pi);

	      Plin = d2/dfac;
	      
	      index=irun*(nsnapMAX+1)*nPk+isnap*nPk+iPk;

	      PkVecFid1[index]=Plin;

	    }

	  // get the second chaining mesh
	  fscanf(fPk,"%le",&atime_BigBox[ivar][isnap]);
	  fscanf(fPk,"%d",&nPk2);
	  fscanf(fPk,"%le",&mass2);
	  fscanf(fPk,"%lld",&npartTOT);

	  for (iPk=0; iPk<nPk2; iPk++)
	    {
	      
	      fscanf(fPk,"%lf %lf %lf %le %le %lf %le %le %le %le",
		     &rk,&d2,&shot,&ModePk,&ModeCount,&D2PowUnCorr,&ModePowUnCorr,
		     &SpecShape,&SumPower,&ConvFac);

	      rkVec_BigBox[iPk]=rk;

	      dfac = 4.0*pi*rk*rk*rk/(2.0*pi)/(2.0*pi)/(2.0*pi);

	      Plin = d2/dfac;

	      index=irun*(nsnapMAX+1)*nPk+isnap*nPk+iPk;
	      PkVecFid2[index]=Plin;

	    }
	  
	  fclose(fPk);

	}
    }
  
  //%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  // rescale the large-scale modes to remove the cosmic variance
  
#ifdef NOCOSMICVARIANCE

  ivar=0;
  for (irun=0; irun<nrun; irun++)
    for (isnap=nsnapMIN+1;isnap<=nsnapMAX;isnap++)
      for (iPk=0;iPk<nPk;iPk++)
	{
	  
	  // set the expansion factor of the currnet snapshot and the initial file
	  aa = atime_BigBox[ivar][isnap];
	  aaINIT = atime_BigBox[ivar][nsnapMIN];

	  // wavenumber
	  rk = rkVec_BigBox[iPk];
	  
	  // compute the linear theory spectra
	  cp.aexp=aa;
	  cp.zout=1.0/cp.aexp-1.0;
	  
	  // calculate the growth factor for the power spectrum at aaINIT
	  DenGrowVar(&DampINIT,aaINIT,1.0,ivar);

	  pkVar.Damp=DampINIT;	  	  

	  // get the linear power spectrum
	  PlinINIT = PlinCDMVar(rk,ivar);

	  printf("aa, aaINIT, D^2 %14.7e %14.7e %14.7e\n",aa,aaINIT,pow(DampINIT,2));

	  index=irun*(nsnapMAX+1)*nPk+isnap*nPk+iPk;
	  indexINIT=irun*(nsnapMAX+1)*nPk+nsnapMIN*nPk+iPk;

	  Pk1INIT = PkVecFid1[indexINIT];
	  Pk2INIT = PkVecFid2[indexINIT];

	  Pk1 = PkVecFid1[index] ;
	  Pk2 = PkVecFid2[index] ;

	  printf("k, pk1ini, pk2ini, plinini %14.7e %14.7e %14.7e %14.7e\n",rk,Pk1INIT,Pk2INIT,PlinINIT);
	  printf("k, pk1, pk2, %14.7e %14.7e %14.7e\n",rk,Pk1,Pk2);

	  // Use a transition function Wf 	  

	  rkcut=0.2;
	  sigma=0.05;

	  Wf = Wfilter(log10(rk),log10(rkcut),sigma);

	  if(rk<0.3)
	    {
	      printf("rk:= %14.7e ; Pk1INIT:= %14.7e ; Pk2INIT:= %14.7e ; PlinINIT:= %14.7e\n",rk,Pk1INIT,Pk2INIT,PlinINIT);
	  // rescale the power spectra	    
	  if (Pk1INIT<=0.0) Pk1INIT=PlinINIT;
	  //if(Pk1INIT>0.0 && Pk1>0.0) 
	  {
	    Pk1NEW = Pk1 * (PlinINIT/Pk1INIT) * Wf + Pk1 * (1.0-Wf);
	    PkVecFid1[index]=Pk1NEW;
	    printf("irun:= %3d , isnap:= %3d , iPk:= %3d, rk:= %14.7e , Pk1OLD:= %14.7e, Correction:= %14.7e\n",
		   irun,isnap,iPk,rk,Pk1,PlinINIT/Pk1INIT);
	  }
	  
	  // rescale the power spectra	    
	  if (Pk2INIT<=0.0) Pk2INIT=PlinINIT;
	  //if(Pk2INIT>0.0 && Pk2>0.0)
	  {
	    Pk2NEW = Pk2 * (PlinINIT/Pk2INIT) * Wf + Pk2 * (1.0-Wf);	  
	    PkVecFid2[index]=Pk2NEW;
	    printf("irun:= %3d , isnap:= %3d , iPk:= %3d, rk:= %14.7e , Pk2OLD:= %14.7e, Pk2NEW:= %14.7e\n\n",
	    irun,isnap,iPk,rk,Pk2,Pk2NEW);
	  }
	    }
	}

#endif

  //%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  // correct all of the vatiational spectra to the same output redshifts
  // in this case we use the run1 fiducial data as a reference

#ifdef SYNCSPECTRA
  
  ivar=0;
  for (isnap=nsnapMIN;isnap<=nsnapMAX;isnap++)
    for (iPk=0;iPk<nPk;iPk++)
      for (irun=0; irun<nrun; irun++)
	{
	  
	  aaFid = atime[0][isnap];
	  aaVar = atime_BigBox[irun][isnap];
	  
	  growFacCorr=CorrectPowGrowEvolveFac(aaFid,aaVar,ivar);
	  growFacCorr2 = growFacCorr*growFacCorr;
	  
	  index=irun*(nsnapMAX+1)*nPk+isnap*nPk+iPk;

	  PkVecFid1[index] = growFacCorr2*PkVecFid1[index];
	  PkVecFid2[index] = growFacCorr2*PkVecFid2[index];
	  
	}

#endif
  
  //%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

  // compute the ensemble average of the power spectra

  for (isnap=nsnapMIN;isnap<=nsnapMAX;isnap++)
  for (iPk=0;iPk<nPk;iPk++)
    {
      PkVecFidBar1[isnap][iPk]=0;
      PkVecFidBar2[isnap][iPk]=0;
      PkVecFidSumSq1[isnap][iPk]=0;
      PkVecFidSumSq2[isnap][iPk]=0;
      PkVecFidVar1[isnap][iPk]=0;
      PkVecFidVar2[isnap][iPk]=0;
    }
  
  for (isnap=nsnapMIN;isnap<=nsnapMAX;isnap++)
  for (iPk=0;iPk<nPk;iPk++)
  for (irun=0; irun<nrun; irun++)
    {
      index=irun*(nsnapMAX+1)*nPk+isnap*nPk+iPk;
      // mean
      PkVecFidBar1[isnap][iPk]=PkVecFidBar1[isnap][iPk]+PkVecFid1[index];
      PkVecFidBar2[isnap][iPk]=PkVecFidBar2[isnap][iPk]+PkVecFid2[index];
      // sum of squares
      PkVecFidSumSq1[isnap][iPk]=PkVecFidSumSq1[isnap][iPk]+pow(PkVecFid1[index],2.0);
      PkVecFidSumSq2[isnap][iPk]=PkVecFidSumSq2[isnap][iPk]+pow(PkVecFid2[index],2.0);
    }
  
  for (isnap=nsnapMIN;isnap<=nsnapMAX;isnap++)
  for (iPk=0;iPk<nPk;iPk++)
    {
      PkVecFidBar1[isnap][iPk]=PkVecFidBar1[isnap][iPk]/(1.0*nrun);
      PkVecFidBar2[isnap][iPk]=PkVecFidBar2[isnap][iPk]/(1.0*nrun);
      // sum of squares
      PkVecFidSumSq1[isnap][iPk]=PkVecFidSumSq1[isnap][iPk]/(1.0*nrun);
      PkVecFidSumSq2[isnap][iPk]=PkVecFidSumSq2[isnap][iPk]/(1.0*nrun);
      // variance 
      PkVecFidVar1[isnap][iPk]=PkVecFidSumSq1[isnap][iPk]-pow(PkVecFidBar1[isnap][iPk],2.0);
      PkVecFidVar2[isnap][iPk]=PkVecFidSumSq2[isnap][iPk]-pow(PkVecFidBar2[isnap][iPk],2.0);
    }

  //%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  // create the composite spectrum

  printf("Generating fiducial L=3000Mpc/h Composite Spectrum\n");

  rkNy = pi*2048/BoxSize;
  rksplit= rkNy/2.0;      // scale where aliasing is an issue
  rksplit= 1000.0*rkNy;   // scale where aliasing is an issue

  rkf = 2.0*pi/BoxSize;   // fundamental wavemode
  nbar = pow((2048/BoxSize),3.0);
    
  ivar=0;
  irun = 1;
  
  iPk2=0;
  for (isnap=nsnapMIN;isnap<=nsnapMAX;isnap++)
    {
      for (iPk=0;iPk<nPk;iPk++)
	{

	  rk = rkVec_BigBox[iPk];
	  Pk1 = PkVecFidBar1[isnap][iPk];
	  Pk2 = PkVecFidBar2[isnap][iPk];
	  
	  if( (Pk2>0) && (rk<rkmaxSIM))
	    {

	      rkVecComp_BigBox[iPk2]=rk;
	      
	      if(rk<rksplit)
		{

		  PkVecVar_BigBox[ivar][isnap][iPk2]=Pk2;		  
		  
		  if(PkVecFidVar2[isnap][iPk]>0.0)
		    PkVecVarSig_BigBox[ivar][isnap][iPk2]=sqrt(PkVecFidVar2[isnap][iPk]);
		  else
		    PkVecVarSig_BigBox[ivar][isnap][iPk2]=0.0;

		}
	      else
		{

		  PkVecVar_BigBox[ivar][isnap][iPk2]=Pk1;

		  if(PkVecFidVar1[isnap][iPk]>0.0)
		    PkVecVarSig_BigBox[ivar][isnap][iPk2]=sqrt(PkVecFidVar1[isnap][iPk]);
		  else
		    PkVecVarSig_BigBox[ivar][isnap][iPk2]=0.0;
		}

	      // compute the errors on the BigBox spectrum assuming Gaussian errors
	      // Gaussian errors in Pk   

	      Pk = PkVecVar_BigBox[ivar][isnap][iPk2];		  
	      
	      DeltaLog10k = log10(rkVec_BigBox[1]/rkVec_BigBox[0]);
	      
	      rkLo = rk*pow(10.0,-1.0*DeltaLog10k/2.0);
	      rkHi = rk*pow(10.0,1.0*DeltaLog10k/2.0);

	      Deltak = rkHi-rkLo;

	      yy = Deltak/rk;
		
	      Vk = 4.0*pi*pow(rk,3.0)*yy*(1.0+yy*yy/12.0);
	      Nk = ((Vk/rkf)/rkf)/rkf;

	      PkVecVarSig_BigBox[ivar][isnap][iPk2]=sqrt((2.0/Nk))*(Pk+1.0/nbar);
	      
	      iPk2=iPk2+1;
	      
	    }
	}
      nPkComp_BigBox=iPk2;
      iPk2=0;

    }

  free(PkVecFid2);
  free(PkVecFid1);

  //%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

#ifdef WRITEALLDATA
  // Dump the fiducial composite spectra to files:
  sprintf(outfile,"%s/PkFidComp_BigBox.dat",All.OutputDir);
  printf("%s\n",outfile);

  ivar=0;
  if((fout = fopen(outfile,"w")))
    {
      fprintf(fout,"%d\t%d\t%d\n",nsnapMIN,nsnapMAX,nPkComp);
      for (isnap=nsnapMIN;isnap<=nsnapMAX;isnap++)
      for (iPk=0;iPk<nPkComp_BigBox;iPk++)
	{
	  fprintf(fout,"%+14.7e\t%+14.7e\t%+14.7e\n",
		  rkVecComp_BigBox[iPk],PkVecVar_BigBox[ivar][isnap][iPk],PkVecVarSig_BigBox[ivar][isnap][iPk]);
	}
    }
  else
    {
      printf("\noutfile not found:= %s.\n", outfile);
      endrun(212);	    
    }
  fclose(fout);
#endif
  
  //%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

#ifdef WRITEALLDATA
  // write out isnap, atime[ivar][isnap], zout[isnap]
  sprintf(outfile,"%s/SNAP_a_z.BigBox.dat",All.OutputDir);
  printf("%s\n",outfile);
  
  if((fout = fopen(outfile,"w")))
    {
      fprintf(fout,"%d\t%d\n",nsnapMIN,nsnapMAX);
      for (isnap=nsnapMIN;isnap<=nsnapMAX;isnap++)
	{
	  //printf("%4d\t",isnap);
	  fprintf(fout,"%4d\t",isnap);
	  for (ivar=0;ivar<=0;ivar++)
	    {
	      aa = atime_BigBox[ivar][isnap];
	      zz = 1.0/aa-1.0;
	      //printf("%+14.7e\t",aa);
      	      fprintf(fout,"%+14.7e\t",aa);
	    }
	  //printf("\n");
	  fprintf(fout,"\n");
	}
    }
  else
    {
      printf("\noutfile not found:= %s.\n", outfile);
      endrun(211);	    
    }
  fclose(fout);
#endif

}

//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

void generate_super_composite(void)
{

  FILE *fout;

  char outfile[180];
  
  int ivar, ii, iPk, isnap, nsnapMIN, nsnapMAX;

  double rkB, PkB, sigB, rkS, PkS, sigS;
  double rkc, sig, Wf;
  
  nsnapMAX = NSNAPMAX;
  nsnapMIN = NSNAPMIN;
  ivar = 0;

  for (isnap=nsnapMIN;isnap<=nsnapMAX;isnap++)
    {

      ii=0;
      for (iPk=0;iPk<nPkComp_BigBox;iPk++)
	{
	  rkB=rkVecComp_BigBox[iPk];
	  PkB=PkVecVar_BigBox[ivar][isnap][iPk];
	  sigB=PkVecVarSig_BigBox[ivar][isnap][iPk];
	  
	  if(rkB<rkBigBoxCut)
	    {
	      rkVecSuperComp[ii]=rkB;
	      PkVecSuperComp[isnap][ii]=PkB;
	      PkVecSigSuperComp[isnap][ii]=sigB;

#ifdef SMOOTHJOIN
	      rkc=0.5;
	      sig=0.06;
	      Wf = 1.0-Wfilter(log10(rkB),log10(rkc),sig);
	      PkVecSigSuperComp[isnap][ii]+=Wf*0.05*PkB;
#endif	  


	      ii++;
	    }	  
	}
      
      for (iPk=0;iPk<nPkComp;iPk++)
	{
	  rkS=rkVecComp[iPk];
	  PkS=PkVecVar[ivar][isnap][iPk];
	  sigS=PkVecVarSig[ivar][isnap][iPk];

	  if(rkS>=rkBigBoxCut)
	    {
	      rkVecSuperComp[ii]=rkS;
	      PkVecSuperComp[isnap][ii]=PkS;
	      PkVecSigSuperComp[isnap][ii]=sigS;

#ifdef SMOOTHJOIN
	      rkc=0.9;
	      sig=0.04;
	      Wf = Wfilter(log10(rkS),log10(rkc),sig);
	      PkVecSigSuperComp[isnap][ii]+=Wf*0.02*PkS;
#endif	  

	      ii++;
	    }
	}
      nPkSuperComp = ii;

    }

#ifdef WRITEALLDATA
  // output the super-composite file
  sprintf(outfile,"%s/PkSuperCompFiducial.dat",All.OutputDir);
  printf("%s\n",outfile);
  
  if((fout = fopen(outfile,"w")))
    {
      fprintf(fout,"%d\t%d\t%d\n",nsnapMIN,nsnapMAX,nPkSuperComp);
      for (isnap=nsnapMIN;isnap<=nsnapMAX;isnap++)
      for (iPk=0;iPk<nPkSuperComp;iPk++)
	{
	  fprintf(fout,"%+14.7e\t%+14.7e\t%+14.7e\n",
		  rkVecSuperComp[iPk],PkVecSuperComp[isnap][iPk],
		  PkVecSigSuperComp[isnap][iPk]);
	}
    }
  else
    {
      printf("\noutfile not found:= %s.\n", outfile);
      endrun(212);	    
    }
  fclose(fout);
#endif

}


//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

void rescale_daemmerung_nonlin_spec(void)
{
  
  FILE *fout;

  char  outfile[180];

  int nrun, irun, isnap, nsnapMAX, nsnapMIN;
  int nPk, iPk, ivar, nvar, num, index;

  double pi, aa, Damp;
  double rk, dfac, Plin, Pnl, Pq, Ph, yy;
  
  // rescale the measured nonlinear spectra by halofit

  pi = M_PI;

  nrun = NRUN;
  nsnapMAX = NSNAPMAX;
  nsnapMIN = NSNAPMIN;
  nPk = NPOWSIM;
  nvar = NVAR;
  
  for (ivar=0; ivar<nvar+1; ivar++)
    {

      // set the cosmological parameters for this model
      init_cospar_Daemmerung(ivar);

      for (isnap=nsnapMIN;isnap<=nsnapMAX;isnap++)
      for (iPk=0;iPk<nPkComp;iPk++)
	{

#ifdef SYNCSPECTRA	  
	  aa=atime[0][isnap];
#else
	  aa=atime[ivar][isnap];
#endif
	  cp.aexp=aa;
	  cp.zout=1.0/cp.aexp-1.0;
	      
	  // generater the new values of omega for this model
	  NewOmega(cp.zout);

	  // calculate the growth factor
	  DenGrowVar(&Damp,aa,1.0,ivar);
	      
	  pkVar.Damp=Damp;
	  
	  EffecSpecVarSP(aa,ivar);
	  
	  rk = rkVecComp[iPk];

	  yy=rk/(2.0*pi);
	  dfac = 4.0*pi*yy*yy*yy;
	  
	  Plin = PlinCDMVar(rk,ivar);
	  
#ifdef TAKAHASHI	  
	  // Takahashi et al 2012
	  halofit2012(rk,Plin*dfac,&Pnl,&Pq,&Ph);
#else
	  // Smith et al 2003
	  halofit2003(rk,Plin*dfac,&Pnl,&Pq,&Ph);
#endif
	  Pnl=Pnl/dfac;
	  
	  PkVecVarScale[ivar][isnap][iPk]=PkVecVar[ivar][isnap][iPk]/Pnl;
	  PkVecVarSigScale[ivar][isnap][iPk]=PkVecVarSig[ivar][isnap][iPk]/Pnl;
	  
	  PkVecVarScaleLin[ivar][isnap][iPk]=PkVecVar[ivar][isnap][iPk]/Plin;
	  PkVecVarSigScaleLin[ivar][isnap][iPk]=PkVecVarSig[ivar][isnap][iPk]/Plin;
	  
	}
    }

  //%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  
#ifdef WRITEALLDATA
  // Dump the composite spectra ratio to files:  
  for (ivar=0; ivar<=nvar;ivar++)
    {

      sprintf(outfile,"%s/PkVarCompRatio.%d.dat",All.OutputDir,ivar);
      printf("%s\n",outfile);
  
      if((fout = fopen(outfile,"w")))
	{
	  fprintf(fout,"%d\t%d\t%d\n",nsnapMIN,nsnapMAX,nPkComp);
	  for (isnap=nsnapMIN;isnap<=nsnapMAX;isnap++)
	  for (iPk=0;iPk<nPkComp;iPk++)
	    {
	      fprintf(fout,"%+14.7e\t%+14.7e\t%+14.7e\t%+14.7e\t%+14.7e\t%+14.7e\t%+14.7e\n",
		      rkVecComp[iPk],
		      PkVecVar[ivar][isnap][iPk],PkVecVarSig[ivar][isnap][iPk],
		      PkVecVarScale[ivar][isnap][iPk],PkVecVarSigScale[ivar][isnap][iPk],
		      PkVecVarScaleLin[ivar][isnap][iPk],PkVecVarSigScaleLin[ivar][isnap][iPk]);
	    }
	}
      else
	{
	  printf("\noutfile not found:= %s.\n", outfile);
	  endrun(212);	    
	}
      fclose(fout);
    }
#endif
}

//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

void rescale_daemmerung_nonlin_spec_BigBox(void)
{
  
  FILE *fout;

  char  outfile[180];

  int nrun, irun, isnap, nsnapMAX, nsnapMIN;
  int nPk, iPk, ivar, nvar, num, index;

  double pi, aa, Damp;
  double rk, dfac, Plin, Pnl, Pq, Ph, yy;
  
  // rescale the measured nonlinear spectra by halofit

  pi = M_PI;

  nrun = 1;
  nsnapMAX = NSNAPMAX;
  nsnapMIN = NSNAPMIN;
  nPk = NPOWSIM;
  nvar = 1;
  
  for (ivar=0; ivar<nvar; ivar++)
    {

      // set the cosmological parameters for this model
      init_cospar_Daemmerung(ivar);
      
      for (isnap=nsnapMIN;isnap<=nsnapMAX;isnap++)
      for (iPk=0;iPk<nPkComp_BigBox;iPk++)
	{

#ifdef SYNCSPECTRA	  
	  aa=atime[0][isnap];
#else
	  aa=atime_BigBox[ivar][isnap];
#endif

	  cp.aexp=aa;
	  cp.zout=1.0/cp.aexp-1.0;
	      
	  // generater the new values of omega for this model
	  NewOmega(cp.zout);

	  // calculate the growth factor
	  DenGrowVar(&Damp,aa,1.0,ivar);
	      
	  pkVar.Damp=Damp;
	  
	  EffecSpecVarSP(aa,ivar);
	  
	  rk = rkVecComp_BigBox[iPk];

	  yy=rk/(2.0*pi);
	  dfac = 4.0*pi*yy*yy*yy;
	  
	  Plin = PlinCDMVar(rk,ivar);
	  
#ifdef TAKAHASHI	  
	  // Takahashi et al. 2012
	  halofit2012(rk,Plin*dfac,&Pnl,&Pq,&Ph);
#else
	  // Smith et al. 2003
	  halofit2003(rk,Plin*dfac,&Pnl,&Pq,&Ph);
#endif
	  Pnl=Pnl/dfac;
	  
	  PkVecVarScale_BigBox[ivar][isnap][iPk]=PkVecVar_BigBox[ivar][isnap][iPk]/Pnl;
	  PkVecVarSigScale_BigBox[ivar][isnap][iPk]=PkVecVarSig_BigBox[ivar][isnap][iPk]/Pnl;
	  
	  PkVecVarScaleLin_BigBox[ivar][isnap][iPk]=PkVecVar_BigBox[ivar][isnap][iPk]/Plin;
	  PkVecVarSigScaleLin_BigBox[ivar][isnap][iPk]=PkVecVarSig_BigBox[ivar][isnap][iPk]/Plin;

	}
      

    }

  //%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  
#ifdef WRITEALLDATA

  // Dump the composite spectra ratio to files:
  for (ivar=0; ivar<nvar;ivar++)
    {

      sprintf(outfile,"%s/PkVarCompRatio_BigBox.%d.dat",All.OutputDir,ivar);
      printf("%s\n",outfile);
  
      if((fout = fopen(outfile,"w")))
	{
	  fprintf(fout,"%d\t%d\t%d\n",nsnapMIN,nsnapMAX,nPkComp_BigBox);
	  for (isnap=nsnapMIN;isnap<=nsnapMAX;isnap++)
	  for (iPk=0;iPk<nPkComp_BigBox;iPk++)
	    {
	      fprintf(fout,"%+14.7e\t%+14.7e\t%+14.7e\t%+14.7e\t%+14.7e\t%+14.7e\t%+14.7e\n",
		      rkVecComp_BigBox[iPk],
		      PkVecVar_BigBox[ivar][isnap][iPk],PkVecVarSig_BigBox[ivar][isnap][iPk],
		      PkVecVarScale_BigBox[ivar][isnap][iPk],PkVecVarSigScale_BigBox[ivar][isnap][iPk],
		      PkVecVarScaleLin_BigBox[ivar][isnap][iPk],PkVecVarSigScaleLin_BigBox[ivar][isnap][iPk]);
	    }
	}
      else
	{
	  printf("\noutfile not found:= %s.\n", outfile);
	  endrun(212);	    
	}
      fclose(fout);
    }

#endif

}


//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

void rescale_daemmerung_nonlin_spec_SuperComp(void)
{
  
  FILE *fout;

  char  outfile[180];

  int nrun, irun, isnap, nsnapMAX, nsnapMIN;
  int nPk, iPk, ivar, nvar, num, index;

  double pi, aa, aa1, aa2, Damp;
  double rk, dfac, Plin, Pnl, Pq, Ph, yy;
  
  // rescale the measured nonlinear spectra by halofit

  pi = M_PI;

  nrun = 1;
  nsnapMAX = NSNAPMAX;
  nsnapMIN = NSNAPMIN;
  nPk = NPOWSIM;
  nvar = 1;

  ivar=0;

  // set the cosmological parameters for this model
  init_cospar_Daemmerung(ivar);
      
  for (isnap=nsnapMIN;isnap<=nsnapMAX;isnap++)
    for (iPk=0;iPk<nPkSuperComp;iPk++)
      {
	
#ifdef SYNCSPECTRA	  
	aa=atime[0][isnap];
#else
	aa=atime_BigBox[ivar][isnap];
#endif

	cp.aexp=aa;
	cp.zout=1.0/cp.aexp-1.0;
	
	// generater the new values of omega for this model
	NewOmega(cp.zout);

	// calculate the growth factor
	DenGrowVar(&Damp,aa,1.0,ivar);
	
	pkVar.Damp=Damp;
	  
	EffecSpecVarSP(aa,ivar);
	  
	rk = rkVecSuperComp[iPk];

	yy=rk/(2.0*pi);
	dfac = 4.0*pi*yy*yy*yy;
	  
	Plin = PlinCDMVar(rk,ivar);
	  
#ifdef TAKAHASHI	  
	// Takahashi et al. 2012
	halofit2012(rk,Plin*dfac,&Pnl,&Pq,&Ph);
#else
	// Smith et al. 2003
	halofit2003(rk,Plin*dfac,&Pnl,&Pq,&Ph);
#endif
	Pnl=Pnl/dfac;
	  
	PkVecScaleSuperComp[isnap][iPk]=PkVecSuperComp[isnap][iPk]/Pnl;
	PkVecSigScaleSuperComp[isnap][iPk]=PkVecSigSuperComp[isnap][iPk]/Pnl;
	  
	PkVecScaleLinSuperComp[isnap][iPk]=PkVecSuperComp[isnap][iPk]/Plin;
	PkVecSigScaleLinSuperComp[isnap][iPk]=PkVecSigSuperComp[isnap][iPk]/Plin;

      }

  //%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  
#ifdef WRITEALLDATA

  // Dump the composite spectra ratio to files:  
  sprintf(outfile,"%s/PkSuperCompRatio.dat",All.OutputDir);
  printf("%s\n",outfile);
  
  if((fout = fopen(outfile,"w")))
    {
      fprintf(fout,"%d\t%d\t%d\n",nsnapMIN,nsnapMAX,nPkSuperComp);
      for (isnap=nsnapMIN;isnap<=nsnapMAX;isnap++)
	{
	  for (iPk=0;iPk<nPkSuperComp;iPk++)
	    {
	      fprintf(fout,"%+14.7e\t%+14.7e\t%+14.7e\t%+14.7e\t%+14.7e\t%+14.7e\t%+14.7e\n",
		      rkVecSuperComp[iPk],
		      PkVecSuperComp[isnap][iPk],PkVecSigSuperComp[isnap][iPk],
		      PkVecScaleSuperComp[isnap][iPk],PkVecSigScaleSuperComp[isnap][iPk],
		      PkVecScaleLinSuperComp[isnap][iPk],PkVecSigScaleLinSuperComp[isnap][iPk]);
	    }
	}
    }
  else
    {
      printf("\noutfile not found:= %s.\n", outfile);
      endrun(212);	    
    }
  fclose(fout);
#endif

}



//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

void nonlin_deriv_matrix(void)
{

  FILE *fout;

  int nrun, irun, isnap, nsnapMAX, nsnapMIN;
  int nPk, iPk, ivar, nvar, npar, ipar, ivar0, ivar1, ivar2;

  double y0, y1, y2;
  double aa, rk, Diff;
  
  char outfile[180];

  double corr[NVAR][NSNAPMAX];

  // rescale the measured nonlinear spectra by halofit

  nsnapMAX = NSNAPMAX;
  nsnapMIN = NSNAPMIN;
  nPk = NPOWSIM;
  nvar = NVAR;
  npar = nvar/2;

  //%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  
  // parameter derivative matrix

  for (ipar=0; ipar<npar ; ipar++)
    {
      for (isnap=nsnapMIN;isnap<=nsnapMAX;isnap++)
	{
	  for (iPk=0;iPk<nPkComp;iPk++)
	    {
	  
	      ivar0=2*ipar+1;
	      ivar1=0;
	      ivar2=2*ipar+2;
	      
	      aa=atime[ivar1][isnap];
	      
	      rk = rkVecComp[iPk];
	      
	      y0 = PkVecVar[ivar0][isnap][iPk];
	      y1 = PkVecVar[ivar1][isnap][iPk];
	      y2 = PkVecVar[ivar2][isnap][iPk];

	      Diff = (y2-y0)/(2.0*ParDelta[ipar]*y1);
	      DiffPkVec[ipar][isnap][iPk] = Diff;

	      y0 = PkVecVarScale[ivar0][isnap][iPk];
	      y1 = PkVecVarScale[ivar1][isnap][iPk];
	      y2 = PkVecVarScale[ivar2][isnap][iPk];
	      
	      Diff = (y2-y0)/(2.0*ParDelta[ipar]*y1);
	      DiffPkVecScale[ipar][isnap][iPk] = Diff;
	      

	    }
	}
    }

  //%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  

#ifdef WRITEALLDATA
  // Dump the derivative data to files
  for (ipar=0; ipar<npar;ipar++)
    {

      sprintf(outfile,"%s/PkDeriv_par_%d.dat",All.OutputDir,ipar);
      //printf("%s\n",outfile);
      
      if((fout = fopen(outfile,"w")))
	{
	  fprintf(fout,"%d\t%d\t%d\n",nsnapMIN,nsnapMAX,nPkComp);
	  for (isnap=nsnapMIN;isnap<=nsnapMAX;isnap++)
	  for (iPk=0;iPk<nPkComp;iPk++)
	    {
	      fprintf(fout,"%+14.7e\t%+14.7e\n",
		      rkVecComp[iPk],DiffPkVec[ipar][isnap][iPk]);
	    }
	}
      else
	{
	  printf("\noutfile not found:= %s.\n", outfile);
	  endrun(213);	    
	}
      fclose(fout);
    }


  for (ipar=0; ipar<npar;ipar++)
    {

      sprintf(outfile,"%s/PkDerivScale_par_%d.dat",All.OutputDir,ipar);
      printf("%s\n",outfile);
  
      if((fout = fopen(outfile,"w")))
	{
	  fprintf(fout,"%d\t%d\t%d\n",nsnapMIN,nsnapMAX,nPkComp);
	  for (isnap=nsnapMIN;isnap<=nsnapMAX;isnap++)
	  for (iPk=0;iPk<nPkComp;iPk++)
	    {
	      fprintf(fout,"%+14.7e\t%+14.7e\n",
		      rkVecComp[iPk],DiffPkVecScale[ipar][isnap][iPk]);
	    }
	}
      else
	{
	  printf("\noutfile not found:= %s.\n", outfile);
	  endrun(213);	    
	}
      fclose(fout);
    }
#endif


}

//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

// second order derivatives d^2y/(dtheta_i dtheta_j)

// currently this will only compute the diagonal entries of the Hessian

void nonlin_deriv2_matrix(void)
{

  FILE *fout;

  int nrun, irun, isnap, nsnapMAX, nsnapMIN;
  int nPk, iPk, ivar, nvar, npar, ipar, jpar;
  int ivar0, ivar1, ivar2;
  int jvar0, jvar1, jvar2;

  double y0, y1, y2;
  double aa, rk, Diff2;
  
  char outfile[180];

  // rescale the measured nonlinear spectra by halofit

  nsnapMAX = NSNAPMAX;
  nsnapMIN = NSNAPMIN;
  nPk = NPOWSIM;
  nvar = NVAR;
  npar = nvar/2;

  //%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

  // parameter derivative matrix

  for (ipar=0; ipar<npar ; ipar++)
    {
      for (isnap=nsnapMIN;isnap<=nsnapMAX;isnap++)
	{
	  for (iPk=0;iPk<nPkComp;iPk++)
	    {
	  
	      ivar0=2*ipar+1;
	      ivar1=0;
	      ivar2=2*ipar+2;

	      aa=atime[ivar1][isnap];
	      
	      rk = rkVecComp[iPk];
	      
	      y0 = PkVecVar[ivar0][isnap][iPk];
	      y1 = PkVecFidRun0[isnap][iPk];
	      y2 = PkVecVar[ivar2][isnap][iPk];

	      Diff2 = (y2+y0-2.0*y1)/(ParDelta[ipar]*ParDelta[ipar]*y1);
	      Diff2PkVec[ipar][isnap][iPk] = Diff2;

	      y0 = PkVecVarScale[ivar0][isnap][iPk];
	      y1 = PkVecVarScale[ivar1][isnap][iPk];
	      y2 = PkVecVarScale[ivar2][isnap][iPk];
	      
	      Diff2 = (y2+y0-2.0*y1)/(ParDelta[ipar]*ParDelta[ipar]*y1);
	      Diff2PkVecScale[ipar][isnap][iPk] = Diff2;

	    }
	}
    }

  //%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  
#ifdef WRITEALLDATA

  // Dump the derivative data to files
  for (ipar=0; ipar<npar;ipar++)
    {

      sprintf(outfile,"%s/PkDeriv2_par_%d.dat",All.OutputDir,ipar);
      //printf("%s\n",outfile);
  
      if((fout = fopen(outfile,"w")))
	{
	  fprintf(fout,"%d\t%d\t%d\n",nsnapMIN,nsnapMAX,nPkComp);
	  for (isnap=nsnapMIN;isnap<=nsnapMAX;isnap++)
	  for (iPk=0;iPk<nPkComp;iPk++)
	    {
	      fprintf(fout,"%+14.7e\t%+14.7e\n",
		      rkVecComp[iPk],Diff2PkVec[ipar][isnap][iPk]);
	    }
	}
      else
	{
	  printf("\noutfile not found:= %s.\n", outfile);
	  endrun(213);	    
	}
      fclose(fout);
    }

  for (ipar=0; ipar<npar;ipar++)
    {

      sprintf(outfile,"%s/PkDeriv2Scale_par_%d.dat",All.OutputDir,ipar);
      printf("%s\n",outfile);
  
      if((fout = fopen(outfile,"w")))
	{
	  fprintf(fout,"%d\t%d\t%d\n",nsnapMIN,nsnapMAX,nPkComp);
	  for (isnap=nsnapMIN;isnap<=nsnapMAX;isnap++)
	  for (iPk=0;iPk<nPkComp;iPk++)
	    {
	      fprintf(fout,"%+14.7e\t%+14.7e\n",
		      rkVecComp[iPk],Diff2PkVecScale[ipar][isnap][iPk]);
	    }
	}
      else
	{
	  printf("\noutfile not found:= %s.\n", outfile);
	  endrun(213);	    
	}
      fclose(fout);
    }

#endif

}

//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

void setFidParAndDeltaPar(void)
{

  int i, npar;
  npar = NVAR/2;

  ParFid[0]=-1.0;          // w0
  ParFid[1]=0.0;           // wa
  ParFid[2]=0.6928849;     // om_DE0
  ParFid[3]=0.11889;       // omch2
  ParFid[4]=0.022161;      // ombh2
  ParFid[5]=0.9611;        // ns
  ParFid[6]=2.14818e-9;    // As
  ParFid[6]=1.0;           // As
  ParFid[7]=0.0;           // running
  
  ParDelta[0] = ParFid[0]*0.1;
  ParDelta[1] = 0.2;
  ParDelta[2] = 0.05;
  ParDelta[3] = ParFid[3]*0.05;
  ParDelta[4] = ParFid[4]*0.05;
  ParDelta[5] = ParFid[5]*0.05;
  ParDelta[6] = ParFid[6]*0.1;
  ParDelta[7] = 0.01;
  
  printf("\nCosmological parameters of the fiducial daemmerung run\n");
  for (i=0; i<npar; i++)
    {
      printf("ipar:= %d, ParFid[%d]:= %+14.7e, ParDelta[%d]:= %+14.7e\n",
	     i,i,ParFid[i],i,ParDelta[i]);
    }

}

//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

void correct_sim_spec(void)
{
  
  FILE *fout;

  char  outfile[180];

  int nrun, irun, isnap, nsnapMAX, nsnapMIN;
  int nPk, iPk, ivar, nvar, num, index;

  double pi, aa, Damp;
  double rk, dfac, PlinF, PlinV, PkF, PkV;
  
  // rescale the measured nonlinear spectra by halofit

  pi = M_PI;

  nrun = NRUN;
  nsnapMAX = NSNAPMAX;
  nsnapMIN = NSNAPMIN;
  nPk = NPOWSIM;
  nvar = NVAR;
  
  for (ivar=0; ivar<nvar+1; ivar++)
    {

      // set the cosmological parameters for this model
      init_cospar_Daemmerung(ivar);

      for (isnap=nsnapMIN;isnap<=nsnapMAX;isnap++)
	{

	  Corr[ivar][isnap] = 0.0;
	  NumCorr[ivar][isnap] = 0.0;

	  for (iPk=0;iPk<nPkComp;iPk++)

	    {
	  
	      rk = rkVecComp[iPk];
	      
	      if(rk<0.03)
		{

		  //---------------
		  // fiducial power
		  
		  aa=atime[0][isnap];
		  cp.aexp=aa;
		  cp.zout=1.0/cp.aexp-1.0;
		  
		  // calculate the growth factor
		  DenGrowVar(&Damp,aa,1.0,0);
		  pkVar.Damp=Damp;	  	  
		  PlinF = PlinCDMVar(rk,0);
		  
		  

		  //--------------
		  // variational linear power
#ifdef SYNCSPECTRA		  
		  aa=atime[0][isnap];
#else
		  aa=atime[ivar][isnap]; 
#endif		  		  
		  cp.aexp=aa;
		  cp.zout=1.0/cp.aexp-1.0;
		  
		  // calculate the growth factor
		  DenGrowVar(&Damp,aa,1.0,ivar);
		  
		  pkVar.Damp=Damp;
		  PlinV = PlinCDMVar(rk,ivar);
		  



		  // determine correction
		  PkF=PkVecVar[0][isnap][iPk];
		  PkV=PkVecVar[ivar][isnap][iPk];
		  
		  Corr[ivar][isnap] += (PlinV/PlinF)/(PkV/PkF);
		  NumCorr[ivar][isnap] += 1;
		  
		}
	    }
	}
    }      


  // normalise the corrections
  for (ivar=0; ivar<nvar+1; ivar++)
    for (isnap=nsnapMIN;isnap<=nsnapMAX;isnap++)
      Corr[ivar][isnap] /= NumCorr[ivar][isnap];
 
  // apply the corrections
  for (ivar=0; ivar<nvar+1; ivar++)
    for (isnap=nsnapMIN;isnap<=nsnapMAX;isnap++)
      for (iPk=0;iPk<nPkComp;iPk++)
	PkVecVar[ivar][isnap][iPk] = PkVecVar[ivar][isnap][iPk]*Corr[ivar][isnap];
 

  //%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  
#ifdef WRITEALLDATA

  // Dump the composite spectra ratio to files:  
  sprintf(outfile,"%s/PkCorrection.dat",All.OutputDir);
  printf("%s\n",outfile);
  
  if((fout = fopen(outfile,"w")))
    {
      fprintf(fout,"%d\t%d\t%d\n",nvar+1,nsnapMIN,nsnapMAX);      
      for (isnap=nsnapMIN;isnap<=nsnapMAX;isnap++)
	{
	  for (ivar=0; ivar<nvar+1; ivar++)
	    {
	      fprintf(fout,"%+14.7e\t",Corr[ivar][isnap]);
	    }
	  fprintf(fout,"\n");
	}
    }
  else
    {
      printf("\noutfile not found:= %s.\n", outfile);
      endrun(212);	    
    }
  fclose(fout);

#endif

}


//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
