#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <time.h>
#include <sys/time.h>
#include <sys/resource.h>
#include <sys/types.h>
#include <unistd.h>
#include <signal.h>
#include <gsl/gsl_errno.h>
#include <gsl/gsl_spline.h>


#include "allvars.h"
#include "proto.h"

//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

// READ LINEAR POWER SPECTRA DATA EXPECTED IN CAMB FORMAT

//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

void read_linear_powerspecVar(void)
{

  FILE *fd;

  int i, ivar, irk;
  int iDE1, iDE2, iomde, iomch2, iombh2, iScalAmp, iScalSpec, iScalRun;

  double l1, l2, l3, l4, l5, l6, l7, l8, length;
  double rk1, Pk1, Pk2, Pk3, Pk4, Pk5, Pk6;
  
  char Root[180], file[180];

  char rootname[120];
  char varsim[NVAR+1][10];

  //%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

  // generate the Plin file names

  strcpy(varsim[0],"22222222");
  strcpy(varsim[1],"12222222");
  strcpy(varsim[2],"32222222");
  strcpy(varsim[3],"21222222");
  strcpy(varsim[4],"23222222");
  strcpy(varsim[5],"22122222");
  strcpy(varsim[6],"22322222");
  strcpy(varsim[7],"22212222");
  strcpy(varsim[8],"22232222");
  strcpy(varsim[9],"22221222");
  strcpy(varsim[10],"22223222");
  strcpy(varsim[11],"22222122");
  strcpy(varsim[12],"22222322");
  strcpy(varsim[13],"22222212");
  strcpy(varsim[14],"22222232");
  strcpy(varsim[15],"22222221");
  strcpy(varsim[16],"22222223");

  printf("\nReading the camb linear power spectra for the daemmerung runs\n");

  for (i=0;i<=NVAR;i++)
    {
      sprintf(All.PowFileNameVar[i],"%s%s.%s.dat",All.PowDirVar,All.PowFileBaseVar,varsim[i]);
      printf("%s\n",All.PowFileNameVar[i]);
    }

  //%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

  // read the Plin data
  
  for (i=0; i<NVAR+1; i++)
    {

      strcpy(file,All.PowFileNameVar[i]);

      irk = 0;

      if((fd = fopen(file, "r")))
	{
	  while(!feof(fd))
	    {
	      
	      fscanf(fd,"%lf %lf",&xrkVar[i][irk],&xPkVar[i][irk]);
	      irk++;
	      if(irk>=NPOWVAR)
		{
		  printf("NPOWVAR too low. More entries in Pk table than assumed\n");
		  endrun(123);
		}
	      
	    }
	}
      else
	{
	  printf("\nPower spectrum file not found %s.\n", file);
	  endrun(2);
	}
      fclose(fd);
    }

  nPkLinVar=irk-1;

  //%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

  // set the max and nim wave number for interpolation/extrapolation
  // of variation spectra
  pkVar.rkmin=xrkVar[0][0];
  pkVar.rkmax=xrkVar[0][nPkLinVar-1];
    
  printf("Max/Min k modes for linear spectrum: %14.7e %14.7e\n",pkVar.rkmax,pkVar.rkmin);

  //%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

  // take the logarithm of all the rk and Pk data

  for(i=0;i<NVAR+1;i++)
  for(irk=0;irk<nPkLinVar;irk++)
    {
      xrkVar[i][irk]=log(xrkVar[i][irk]);
      xPkVar[i][irk]=log(xPkVar[i][irk]);
    }

  //%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

  // spline all of the Plin functions

  if(!(PkVar_spls=malloc(sizeof(struct Pk_splines)*(NVAR+1))))
    {
      printf("failed to allocate memory for `spls'.\n" );
      endrun(1);
    }
  
  // create spline functions for all of the data
  PkVar_acc = gsl_interp_accel_alloc();

  // generate the splines for the various cosmologies
  for (i=0;i<NVAR+1; i++)
    {
      PkVar_spls[i].spline = gsl_spline_alloc(gsl_interp_cspline, nPkLinVar);
      gsl_spline_init(PkVar_spls[i].spline, xrkVar[i], xPkVar[i], nPkLinVar);
    }

}

//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

double PlinCDMVar(double rk, int iVAR)
{

  double PlinCDMVar, xx, yy, x1, x2, y1, y2, mm, cc;
  
  if (rk<pkVar.rkmax && rk>pkVar.rkmin) 
    {
      xx = log(rk);
      yy = gsl_spline_eval(PkVar_spls[iVAR].spline, xx, PkVar_acc);         
      //printf("%14.7e\n",yy);
    }
  else if(rk<=pkVar.rkmin) 
    {
      // use the first two points of the Pk function to generate a power-law model
      xx= log(rk);
      x1=xrkVar[iVAR][0];
      x2=xrkVar[iVAR][1];
      y1=xPkVar[iVAR][0];
      y2=xPkVar[iVAR][1];
      mm=(y2-y1)/(x2-x1);
      cc=(y1*x2-y2*x1)/(x2-x1);      
      yy = mm*xx+cc;      
    }
  else if(rk>=pkVar.rkmax) 
    {
      // use the last two points of the Pk function to generate a power-law model
      xx = log(rk);
      x1=xrkVar[iVAR][nPkLinVar-2];
      x2=xrkVar[iVAR][nPkLinVar-1];
      y1=xPkVar[iVAR][nPkLinVar-2];
      y2=xPkVar[iVAR][nPkLinVar-1];
      mm=(y2-y1)/(x2-x1);
      cc=(y1*x2-y2*x1)/(x2-x1);            
      yy = mm*xx+cc;

    }

  PlinCDMVar=pkVar.Damp*pkVar.Damp*exp(yy);
  //printf("rk:= %14.7e , Pk:= %14.7e\n",rk,pkVar.Damp);
  return PlinCDMVar;
      
}

//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

void free_linear_powerspecVar(void)
{

  int i;

  // free the memory for the linear power spectrum splines
  for (i=0;i<NVAR+1; i++)
    gsl_spline_free(PkVar_spls[i].spline);    
  gsl_interp_accel_free(PkVar_acc);  
  free(PkVar_spls);

}

//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

// TARGET LINEAR POWER SPECTRA

//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

void read_linear_powerspecTarget(void)
{

  FILE *fd;

  int i, irk;

  double rk1, Pk1, Pk2, Pk3, Pk4, Pk5, Pk6;
  
  char TargetFile[180];

  //%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

  // generate the Plin file name

  printf("\nReading the linear power spectrum for the target run\n");

  sprintf(TargetFile,"%s/%s",All.PowDirTarget,All.PowFileTarget);
  
  printf("File:= %s\n",TargetFile);

  // read the Plin data
  
  irk = 0;
  if((fd = fopen(TargetFile, "r")))
    {
      while(!feof(fd))
	{
	  
	  fscanf(fd,"%lf %lf",&xrkTarget[irk],&xPkTarget[irk]);
	  irk++;
	  if(irk>=NPOWTARGET)
	    {
	      printf("NPOWTARGET too low. More entries in Pk table than assumed\n");
	      endrun(124);
	    }
	}
    }
  else
    {
      printf("\nPower spectrum file not found %s.\n", TargetFile);
      endrun(2);
    }
  fclose(fd);

  nPkLinTarget=irk-1;

  //%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

  // set the max and min wave number for interpolation/extrapolation
  pkTarget.rkmin=xrkTarget[0];
  pkTarget.rkmax=xrkTarget[nPkLinTarget-1];    
  
  printf("\nThe target power spectrum has bounds: rkmax:= %14.7e  rkmin:= %14.7e\n",
	 pkTarget.rkmin,pkTarget.rkmax);
    
  //%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

  // take the logarithm of all the rk and Pk data

  for(irk=0;irk<nPkLinTarget;irk++)
    {
      xrkTarget[irk]=log(xrkTarget[irk]);
      xPkTarget[irk]=log(xPkTarget[irk]);
    }

  //%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

  // spline the Plin function

  if(!(PkTarget_spls=malloc(sizeof(struct Pk_splines)*(1))))
    {
      printf("failed to allocate memory for `spls'.\n" );
      endrun(1);
    }
  
  // create spline functions for all of the data
  PkTarget_acc = gsl_interp_accel_alloc();

  // generate the splines for the various cosmologies
  PkTarget_spls[0].spline = gsl_spline_alloc(gsl_interp_cspline, nPkLinTarget);
  gsl_spline_init(PkTarget_spls[0].spline, xrkTarget, xPkTarget, nPkLinTarget);

}

//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

double PlinCDMTarget(double rk)
{
  
  double PlinCDMTarget, xx, yy, x1, x2, y1, y2, mm, cc;
  
  if (rk<pkTarget.rkmax && rk>pkTarget.rkmin) 
    {
      xx = log(rk);
      yy = gsl_spline_eval(PkTarget_spls[0].spline, xx, PkTarget_acc);         
    }
  else if(rk<=pkTarget.rkmin) 
    {
      // use the first two points of the Pk function to generate a power-law model
      xx= log(rk);
      x1=xrkTarget[0];
      x2=xrkTarget[1];
      y1=xPkTarget[0];
      y2=xPkTarget[1];
      mm=(y2-y1)/(x2-x1);
      cc=(y1*x2-y2*x1)/(x2-x1);      
      yy = mm*xx+cc;      
    }
  else if(rk>=pkTarget.rkmax) 
    {
      // use the last two points of the Pk function to generate a power-law model
      xx = log(rk);
      x1=xrkTarget[nPkLinTarget-2];
      x2=xrkTarget[nPkLinTarget-1];
      y1=xPkTarget[nPkLinTarget-2];
      y2=xPkTarget[nPkLinTarget-1];
      mm=(y2-y1)/(x2-x1);
      cc=(y1*x2-y2*x1)/(x2-x1);            
      yy = mm*xx+cc;

    }

  PlinCDMTarget=pkTarget.Damp*pkTarget.Damp*exp(yy);
  return PlinCDMTarget;
      
}

//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

void free_linear_powerspecTarget(void)
{

  gsl_spline_free(PkTarget_spls[0].spline);    
  gsl_interp_accel_free(PkTarget_acc);  
  free(PkTarget_spls);

}

//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
