#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <time.h>
#include <sys/time.h>
#include <sys/resource.h>
#include <sys/types.h>
#include <unistd.h>
#include <signal.h>
#include <gsl/gsl_rng.h>

#include "allvars.h"
#include "proto.h"

/*! This function parses the parameterfile in a simple way.  Each paramater
 *  is defined by a keyword (`tag'), and can be either of type double, int,
 *  or character string.  The routine makes sure that each parameter
 *  appears exactly once in the parameterfile, otherwise error messages are
 *  produced that complain about the missing parameters.
 */

void read_parameter_file(char *fname)
{
#define DOUBLE 1
#define STRING 2
#define INT 3
#define MAXTAGS 300

  FILE *fd, *fdout;
  char buf[400], buf1[400], buf2[400], buf3[400];
  char *buffer, *aPtr;
  int i, j, nt, icount;
  int id[MAXTAGS];
  void *addr[MAXTAGS];
  char tag[MAXTAGS][50];
  int  errorFlag = 0;
  int count;

  char TmpS1[100], TmpS2[100];

  if(sizeof(long long) != 8)
    {
      printf("\nType `long long' is not 64 bit on this platform. Stopping.\n\n");
      endrun(0);
    }

  if(sizeof(int) != 4)
    {
      printf("\nType `int' is not 32 bit on this platform. Stopping.\n\n");
      endrun(0);
    }

  if(sizeof(float) != 4)
    {
      printf("\nType `float' is not 32 bit on this platform. Stopping.\n\n");
      endrun(0);
    }

  if(sizeof(double) != 8)
    {
      printf("\nType `double' is not 64 bit on this platform. Stopping.\n\n");
      endrun(0);
    }


  //look for the tags
  nt = 0;
  
  strcpy(tag[nt], "OutputDir");
  addr[nt] = &All.OutputDir;
  id[nt++] = STRING;
  
  strcpy(tag[nt], "OutputFileBase");
  addr[nt] = &All.OutputFileBase;
  id[nt++] = STRING;
  
  strcpy(tag[nt], "OutputMPTFileBase");
  addr[nt] = &All.OutputMPTFileBase;
  id[nt++] = STRING;
  
  strcpy(tag[nt], "PowDirTarget");
  addr[nt] = &All.PowDirTarget;
  id[nt++] = STRING;

  strcpy(tag[nt], "PowFileTarget");
  addr[nt] = &All.PowFileTarget;
  id[nt++] = STRING;

  strcpy(tag[nt], "PowDirVar");
  addr[nt] = &All.PowDirVar;
  id[nt++] = STRING;

  strcpy(tag[nt], "PowFileBaseVar");
  addr[nt] = &All.PowFileBaseVar;
  id[nt++] = STRING;

  strcpy(tag[nt], "aexp");
  addr[nt] = &cp.aexp;
  id[nt++] = DOUBLE;

  strcpy(tag[nt], "w0");
  addr[nt] = &cp.w0;
  id[nt++] = DOUBLE;

  strcpy(tag[nt], "w1");
  addr[nt] = &cp.w1;
  id[nt++] = DOUBLE;

  strcpy(tag[nt], "om_DE0");
  addr[nt] = &cp.om_DE0;
  id[nt++] = DOUBLE;

  strcpy(tag[nt], "om_ch20");
  addr[nt] = &cp.om_ch20;
  id[nt++] = DOUBLE;

  strcpy(tag[nt], "om_bh20");
  addr[nt] = &cp.om_bh20;
  id[nt++] = DOUBLE;

  strcpy(tag[nt], "pindex");
  addr[nt] = &cp.pindex;
  id[nt++] = DOUBLE;

  strcpy(tag[nt], "As");
  addr[nt] = &cp.As;
  id[nt++] = DOUBLE;

  strcpy(tag[nt], "running");
  addr[nt] = &cp.running;
  id[nt++] = DOUBLE;

  strcpy(tag[nt], "nPkOut");
  addr[nt] = &hfit.nPkOut;
  id[nt++] = INT;

  strcpy(tag[nt], "rkOutMIN");
  addr[nt] = &hfit.rkOutMIN;
  id[nt++] = DOUBLE;

  strcpy(tag[nt], "rkOutMAX");
  addr[nt] = &hfit.rkOutMAX;
  id[nt++] = DOUBLE;

  strcpy(tag[nt], "iLogOrLin");
  addr[nt] = &hfit.iLogOrLin;
  id[nt++] = INT;

  strcpy(tag[nt], "iGenEffSpecVar");
  addr[nt] = &hfit.iGenEffSpecVar;
  id[nt++] = INT;
  
  strcpy(tag[nt], "iGenEffSpecTarget");
  addr[nt] = &hfit.iGenEffSpecTarget;
  id[nt++] = INT;
  
  printf("fname:= %s\n",fname);

  if((fd = fopen(fname, "r")))
    {
      sprintf(buf, "%s%s",fname, "-usedvalues");
      if(!(fdout = fopen(buf, "w")))
	{
	  printf("error opening file '%s' \n", buf);
	  errorFlag = 1;
	}
      else
	{
	  while(!feof(fd))
	    {
	      *buf = 0;
	      fgets(buf, 400, fd);
	      if(sscanf(buf, "%s%s%s", buf1, buf2, buf3) < 2)
		continue;
	      
	      if(buf1[0] == '%')
		continue;
	      
	      for(i = 0, j = -1; i < nt; i++)
		if(strcmp(buf1, tag[i]) == 0)
		  {
		    j = i;
		    tag[i][0] = 0;
		    break;
		  }
	      
	      if(j >= 0)
		{
		  switch (id[j])
		    {
		    case DOUBLE:
		      *((double *) addr[j]) = atof(buf2);
		      fprintf(fdout, "%-35s%g\n", buf1, *((double *) addr[j]));
		      break;
		    case STRING:
		      strcpy(addr[j], buf2);
		      fprintf(fdout, "%-35s%s\n", buf1, buf2);
		      break;
		    case INT:
		      *((int *) addr[j]) = atoi(buf2);
		      fprintf(fdout, "%-35s%d\n", buf1, *((int *) addr[j]));
		      break;
		    }
		}
	      else
		{
		  fprintf(stdout, "Error in file %s:   Tag '%s' not allowed or multiple defined.\n",
			  fname, buf1);
		  errorFlag = 1;
		}
	    }
	  fclose(fd);
	  fclose(fdout);
	  
	  
	  i = strlen(All.OutputDir);
	  if(i > 0)
	    if(All.OutputDir[i - 1] != '/')
	      strcat(All.OutputDir, "/");
	  
	  sprintf(buf1, "%s%s", fname, "-usedvalues");
	  sprintf(buf2, "%s%s", All.OutputDir, "parameters-usedvalues");
	  sprintf(buf3, "cp %s %s", buf1, buf2);
	  system(buf3);

	  printf("\nCheck target cosmological parameters read correctly...\n");
	  printf("   aexp := %+10.5e\n",cp.aexp);
	  printf("     w0 := %+10.5e\n",cp.w0);
	  printf("     w1 := %+10.5e\n",cp.w1);
	  printf(" om_DE0 := %+10.5e\n",cp.om_DE0);
	  printf("om_ch20 := %+10.5e\n",cp.om_ch20);
	  printf("om_bh20 := %+10.5e\n",cp.om_bh20);
	  printf(" pindex := %+10.5e\n",cp.pindex);
	  printf("     As := %+10.5e\n",cp.As);
	  printf("running := %+10.5e\n",cp.running);

	  if(hfit.nPkOut>NPKOUT) 
	    {
	      printf("nPkOut>NPKOUT: change allvars.h if you want that many k-bins");
	      endrun(31);
	    }

	  if( (hfit.iLogOrLin != 0) && (hfit.iLogOrLin != 1)) 
	    {
	      printf("iLogOrLin value should be 0 or 1");
	      endrun(32);
	    }
	  
	}
    }
  else
    {
      printf("\nParameter file %s not found.\n\n", fname);
      errorFlag = 2;
    }

  if(errorFlag != 2)
    for(i = 0; i < nt; i++)
      {
	if(*tag[i])
	  {
	    printf("Error. I miss a value for tag '%s' in parameter file '%s'.\n", tag[i], fname);
	    errorFlag = 1;
	  }
      }
  
  
#undef DOUBLE
#undef STRING
#undef INT
#undef MAXTAGS

}


//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

// read the expansion factors from the file


void read_expansion_factors(char *infile)
{

  FILE *faexp;  
  int i;
  double zout;
  
  printf("\nopening:= %s\n",infile);

  i = 0;
 
  if((faexp = fopen(infile, "r")))
    {
      while(!feof(faexp))
	{

	  fscanf(faexp,"%lf",&xaexpTarget[i]);
	  i++;

	  if(i>=NAEXPMAX)
	    {
	      printf("NAEXPMAX too low. More entries in expansion factor table than assumed\n");
	      endrun(123);
	    }
	}
    }
  else
    {
      printf("\nFile with expansion factors not found:= %s.\n", infile);
      endrun(21);	    
    }
  fclose(faexp);
  naexpTarget=i-1;  

  for (i=0;i<naexpTarget;i++)
    {
      zout=1.0/xaexpTarget[i]-1.0;
      printf("i:= %3d , aexp:= %15.7e , zout:= %15.7e\n",i,xaexpTarget[i],zout);
    }
}

//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
