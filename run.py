#!/usr/bin/env python

import os
import sys
import numpy as np
import matplotlib.pyplot as plt

#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

argc = len(sys.argv[:])

if(argc != 3 ):
    print("\nIncorrect usage of run.py\n")
    print("To perform the initial set-up of the code call: run.py 1 0\n")
    print("   To run the code and produce test plots call: run.py 0 1\n")
    print("            To run the code without plots call: run.py 0 0\n")
    sys.exit()

isetup = int(sys.argv[1])
iplot = int(sys.argv[2])

if (isetup != 0) & (isetup != 1):
    print("Value error: isetup should be either 0 or 1")
    sys.exit()

if (iplot != 0) & (iplot != 1):
    print("Value error: iplot should be either 0 or 1")
    sys.exit()

#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

# code setup needs to be done only once. If you already
# have generated the data in DATAEffectiveSpectra/ you
# do not need to do this step

code = "./NGenHalofit.exe"

if isetup == 1:

    parfile = 'parameterfiles/setup.dat '
    print "Input parameters are in:",parfile

    aexpfile = 'parameterfiles/setup_expansion.dat'
    print "Expansion factors to compute are in:",aexpfile
    
    os.system(code+parfile+aexpfile)

#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

# run the code for the fiducial model

parfile = ' parameterfiles/input.Fid.dat'

aexpfile = ' parameterfiles/expansion_factors.dat'

os.system(code+parfile+aexpfile)

#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

# MAKE A PLOT OF THE DATA

#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

if (iplot==1):

    nout = 4
    nTH = 200
    
    rk=np.zeros(nTH)
    PLin=np.zeros((nout,nTH))
    PHalofit=np.zeros((nout,nTH))
    PNGenHalofit=np.zeros((nout,nTH))
    
    #path = '../c-code-V6/'
    path = './'
    
    datadir = path+'DATA/'
    
    infile1 = []
    
    for iout in range(nout):

        file=datadir+'PkNGenHalofit_PowSpec_Fiducial.'+str(iout)+'.dat'        
        infile1.append(file)
        
    for iout in range(nout):
        file = infile1[iout]
        print('Reading... '+file)
        f = open(file, 'r')
        i=0
        for line in f:
            line = line.strip()
            columns = line.split()
            rk[i] = float(columns[0])
            PLin[iout,i] = float(columns[1])
            PHalofit[iout,i] = float(columns[2])
            PNGenHalofit[iout,i] = float(columns[3])
            print("iout:= %3d , i:= %3d, rk:= %14.7e , PLin:= %14.7e , Phfit:= %14.7e , PNGenHfit %14.7e"%
                  (iout,i,rk[i],PLin[iout,i],PHalofit[iout,i],PNGenHalofit[iout,i]))
            i=i+1
        f.close()
    
    #%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    # allocate arrays for plotting

    x1 = np.zeros(nTH)
    y1 = np.zeros(nTH)
    y2 = np.zeros(nTH)
    y3 = np.zeros(nTH)
    
    label = np.zeros(nout)
    
    label = ["$z=0.0$" , "$z=0.5$", "$z=1.0$" , "$z=4$"]
    
    #%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%




    
    # PLOT THE DATA




    
    #%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    ThisPlot = 0

    iplot = 1
    if iplot == 1:
        
    # determine the max and min
    
        xmin=np.min(rk[:])
        xmax=np.max(rk[:])
        
        ymin1=1.0e20
        ymax1=-1.0e20
        for iout in range(nout):
            for i in range(nTH):
                ytmp=PNGenHalofit[iout,i]
                if ytmp > ymax1 :
                    ymax1 = ytmp
                if ytmp < ymin1 :
                    ymin1 = ytmp
                    
        ymin=np.min((ymin1,ymin1))
        ymax=np.max((ymax1,ymax1))
        
        delx=np.log10(xmax)-np.log10(xmin)
        dely=np.log10(ymax)-np.log10(ymin)
        
        ymin = np.log10(ymin)-0.05*dely
        ymin = 10**ymin
        ymax = np.log10(ymax)+0.05*dely
        ymax = 10**ymax
        
        delx=np.log10(xmax)-np.log10(xmin)
        dely=np.log10(ymax)-np.log10(ymin)
        
        print("PLOTTING DATA")
        
        # loop over plots
        for iout in range(nout):
            
            print("Max/Min data")                
            print(xmin,xmax,ymin,ymax)
            
            limits = [xmin,xmax,ymin,ymax]
            
            fig = plt.figure(ThisPlot,(6.5,7))
            
            plt.axes([0.13,0.43,0.8,0.5])
            
            plt.rc('text', usetex=True)
            plt.rc('font', family='serif')
            
            plt.axis(limits)
            
            plt.ylabel(r"$P(k)\, [h^{-3}\,{\rm Mpc}^{3}]$",size='x-large')
            #        plt.xlabel(r"$k\, [h{\rm Mpc}^{-1}]$",size='x-large')

            plt.xscale("log", basex=10)        
            plt.yscale("log", basex=10)
            plt.xticks([0.1,1.0,10.0], [])
            
            # write a label in the plot
            xloc = np.log10(xmin)+0.1*delx
            xloc = 10**xloc
            yloc = np.log10(ymin)+0.1*dely
            yloc = 10**yloc
            
            plt.text(xloc,yloc,label[iout], fontsize=15,
                     bbox={'facecolor':'white', 'alpha':0.5, 'pad':10})
            
            # plot the theoretical predictions
    
            x1[:] = rk[:]
            y1[:] = PLin[iout,:]
            y2[:] = PHalofit[iout,:]
            y3[:] = PNGenHalofit[iout,:]
            
            plt.plot(x1,y1, '--', linewidth=1,color='b',label=r'$P_{\rm Lin}(k)$')
            plt.plot(x1,y2, '-.', linewidth=1,color='g',label=r'$P_{\rm Halofit2012}(k)$')
            plt.plot(x1,y3, '-', linewidth=1,color='r',label=r'$P_{\rm NGenHalofit}(k)$')
            
            plt.legend(loc=0)
            
            #%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            # plot the ratio
        
            ymax2=1.1
            ymin2=0.9
            
            limits = [xmin,xmax,ymin2,ymax2]
            
            plt.axes([0.13,0.1,0.8,0.32])
            plt.axis(limits)
            
            plt.ylabel(r"$P/P_{\rm Takahashi}$",size='x-large')
            plt.xlabel(r"$k\, [h{\rm Mpc^{-1}}]$",size='x-large') 
            plt.xscale("log", basex=10)
            
            x1[:] = rk[:]
            y1[:] = PLin[iout,:]/PHalofit[iout,:]
            y2[:] = PHalofit[iout,:]/PHalofit[iout,:]
            y3[:] = PNGenHalofit[iout,:]/PHalofit[iout,:]        
            
            plt.plot(x1,y1, '--', linewidth=1,color='b')        
            plt.plot(x1,y2, '-.', linewidth=1,color='g')
            plt.plot(x1,y3, '-', linewidth=1,color='r')
            
            plotname = 'PLOTS/Pk.'+str(iout)+'.pdf'
            print(plotname,iout)
            
            plt.savefig(plotname, format='pdf')
            
            plt.close(fig)
            
            ThisPlot+=1


sys.exit()        


