#!/bin/csh

set EXE = NGenHalofit.exe

set iVAR = 1
set NVAR = 16    
    
# set up the code -- this generates key static data
./$EXE parameterfiles/setup.dat parameterfiles/setup_expansion.dat

# run the code for the fiducial model
./$EXE parameterfiles/input.Fid.dat parameterfiles/expansion_factors.dat

# run the code the 16 variation runs
while ($iVAR <= $NVAR )
      ./$EXE parameterfiles/input.Var$iVAR.dat parameterfiles/expansion_factors.dat
@ iVAR++
end
      


    
