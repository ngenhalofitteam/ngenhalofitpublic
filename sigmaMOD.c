#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <time.h>
#include <gsl/gsl_integration.h>

#include "allvars.h"
#include "proto.h"

// GAUSSIAN QUADRATURE 

#define WORKMAX 60000

//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

double compute_sigma(double Radius)
{

  gsl_integration_workspace *w = gsl_integration_workspace_alloc(WORKMAX);

  double aa, zout, Damp;
  double sig, result, error, yy1, yy2;
  double expected, alpha, sigR, sigR2;
  
  gsl_function F;

  int ikey, nkey;

  // set the expansion fator to compute
  aa=1.0;
  // redshift
  zout=1.0/aa-1.0;
  
  // generate the new values of omega for this model
  NewOmega(zout);
  
  // calculate the growth factor
  DenGrowTarget(&Damp,aa,1.0);
  pkTarget.Damp=Damp;

  printf("Damp:= %14.7e\n",Damp);
  
  alpha=Radius;
  
  // integral is done in the variable log y=log kR
  yy1=log(0.001*Radius); // lower limit
  yy2=log(10.0*Radius);  // upper limit

  F.function = &sigmaFunc;
  F.params = &alpha;  

  //gsl_integration_qags(&F, rkmin, rkmax, 1.0e-4, 1.0e-7, WORKMAX, w, &result, &error); 
  for (ikey=4;ikey<=4;ikey++)
    {
      gsl_integration_qag(&F,yy1,yy2,1.0e-6,1.0e-6,
			  WORKMAX, ikey , w, &result, &error);
      sigR2 = (4.0*M_PI/(Radius*2.0*M_PI)/(Radius*2.0*M_PI)/(Radius*2.0*M_PI))*result;
      sigR = sqrt(sigR2);
      //printf("sig:= %14.7e ; error:= %14.7e\n",sigR, error);

    }
  
  gsl_integration_workspace_free(w);
  
  return sigR;

}


//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

double sigmaFunc(double xx, void *par)
{
  double yy, Wf, func, rk, Radius;

  yy = exp(xx);

  Radius = *(double *) par;

  rk = yy/Radius;

  Wf = WTopHat(yy);

  func = pow(yy,3)*PlinCDMTarget(rk)*Wf*Wf;

  //printf("%14.7e %14.7e %14.7e %14.7e %14.7e\n",rk,Radius,Wf,PlinCDMTarget(rk),func);
  
  return func;
}

//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

double WTopHat(double yy)
{
  double WTH;
  if(yy<0.5)
    {
      WTH=1.0 - (yy*yy/10.0)*(1.0 - yy*yy/28.0);
    }
  else
    {
      WTH = (3.0/pow(yy,3)) * (sin(yy)-yy*cos(yy));
    }
  return WTH;
}

//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
