#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <time.h>
#include <sys/time.h>
#include <sys/resource.h>
#include <sys/types.h>
#include <unistd.h>
#include <signal.h>
#include <gsl/gsl_math.h>
#include <gsl/gsl_interp2d.h>
#include <gsl/gsl_spline2d.h>

#include "allvars.h"
#include "proto.h"

//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

void spline_spectra_fiducial(void)
{

  int naexp, iaexp, iPk, ivar;
  
  double logPk, Pij;

  double log10aa, aa, logaa, log10rk, rk, logrk;

  const gsl_interp2d_type *PkFid_bicubic = gsl_interp2d_bicubic;;

  int ia, na, ik, nk;
  
  // number of expansion factors
  naexp=NSNAPMAX-NSNAPMIN+1;

  // allocate memory for the 2D spline
  if( ! (PkFidData = malloc(naexp * nPkComp * sizeof(double))))  
    {
      printf("failed to allocate memory for `PkFidSpline'.\n" );
      endrun(12);
    }
  
  // allocate the spline object
  PkFid_bispl = gsl_spline2d_alloc(PkFid_bicubic, naexp, nPkComp);
  
  // accelleration for y-direction
  aexp_acc  = gsl_interp_accel_alloc();

  // accelleration for x-direction
  rkVec_acc = gsl_interp_accel_alloc();

  printf("setting the bicubic spline grid values\n");
  ivar = 0 ;
  for (iaexp=0;iaexp<naexp;iaexp++)
    for (iPk=0;iPk<nPkComp;iPk++)
      {
	logPk = log(PkVecVarScale[ivar][iaexp+3][iPk]);
	gsl_spline2d_set(PkFid_bispl, PkFidData, iaexp, iPk, logPk);
      }
  
  // set the values of the logrk 
  for (iPk=0; iPk<nPkComp; iPk++)
    logrkVec[iPk]=log(rkVecComp[iPk]);

  // set the values of the logaexp 
  for (iaexp=0; iaexp<naexp; iaexp++)
    logaexp[iaexp]=log(atime[0][iaexp+3]);

  // initialize interpolation //  
  gsl_spline2d_init(PkFid_bispl, logaexp, logrkVec, PkFidData, naexp, nPkComp);

}

//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

void spline_spectra_fiducial_SuperComp(void)
{

  int naexp, iaexp, iPk, ivar;
  
  double logPk, Pij;

  double log10aa, aa, logaa, log10rk, rk, logrk;

  const gsl_interp2d_type *PkFid_bicubic_SuperComp = gsl_interp2d_bicubic;;

  int ia, na, ik, nk;
  
  // number of expansion factors
  naexp=NSNAPMAX-NSNAPMIN+1;

  // allocate memory for the 2D spline
  if( ! (PkFidData_SuperComp = malloc(naexp * nPkSuperComp * sizeof(double))))  
    {
      printf("failed to allocate memory for `PkFidSpline'.\n" );
      endrun(12);
    }
  
  // allocate the spline object
  PkFid_bispl_SuperComp = gsl_spline2d_alloc(PkFid_bicubic_SuperComp, naexp, nPkSuperComp);
  
  // accelleration for y-direction
  aexp_acc_SuperComp  = gsl_interp_accel_alloc();

  // accelleration for x-direction
  rkVec_acc_SuperComp = gsl_interp_accel_alloc();

  printf("setting the bicubic spline grid values\n");
  ivar = 0 ;
  for (iaexp=0;iaexp<naexp;iaexp++)
    for (iPk=0;iPk<nPkSuperComp;iPk++)
      {
	logPk = log(PkVecScaleSuperComp[iaexp+3][iPk]);
	gsl_spline2d_set(PkFid_bispl_SuperComp, PkFidData_SuperComp, iaexp, iPk, logPk);
	//printf("%3d  %3d  %+14.7e %14.7e\n",iaexp,iPk,logPk,PkVecScaleSuperComp[iaexp+3][iPk]);
      }

  // set the values of the logrk 
  for (iPk=0; iPk<nPkSuperComp; iPk++)
    logrkVec_SuperComp[iPk]=log(rkVecSuperComp[iPk]);

  // set the values of the logaexp 
  for (iaexp=0; iaexp<naexp; iaexp++)
    logaexp_SuperComp[iaexp]=log(atime_BigBox[0][iaexp+3]);

  // initialize interpolation //  
  gsl_spline2d_init(PkFid_bispl_SuperComp, logaexp_SuperComp,
		    logrkVec_SuperComp, PkFidData_SuperComp, naexp, nPkSuperComp);

}


//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

// bicubic spline to the 8 derivatives 

void spline_spectra_derivatives(void)
{

  FILE *fout;

  char outfile[180];

  int ipar, npar, naexp, iaexp, iPk, ivar;
  int isnap, nsnapMAX, nsnapMIN, ivar0, ivar1, ivar2;

  double PkDeriv;

  double log10aa, aa, logaa, log10rk, rk, logrk;

  int ia, na, ik, nk;

  const gsl_interp2d_type *PkDeriv_bicubic = gsl_interp2d_bicubic;
  
  // number of parameter
  npar=NVAR/2;
  
  // number of expansion factors
  naexp=NSNAPMAX-NSNAPMIN+1;

  // allocate memory for the npar 2D spline
  if( ! (sp_deriv1 = malloc(npar*sizeof(struct spline_deriv))))
    {
      printf("failed to allocate memory for `sp_deriv1'.\n" );
      endrun(121);
    }

  // allocate memory for the data associated with each spline
  for (ipar=0; ipar<npar;ipar++)
    {

      ivar0=ipar*2+1;
      ivar1=0;
      ivar2=ipar*2+2;

      if( ! (sp_deriv1[ipar].PkDerivData = malloc(naexp * nPkComp * sizeof(double))))  
	{
	  printf("failed to allocate memory for `sp_deriv1[ipar].PkDerivData'.\n" );
	  endrun(122);
	}
      
      // allocate the spline object
      sp_deriv1[ipar].PkDeriv_bispl = gsl_spline2d_alloc(PkDeriv_bicubic, naexp, nPkComp);
      
      // accelleration for x-direction
      sp_deriv1[ipar].aexp_acc  = gsl_interp_accel_alloc();

      // accelleration for y-direction
      sp_deriv1[ipar].rkVec_acc = gsl_interp_accel_alloc();

      //printf("setting the bicubic spline grid values for ipar:= %d\n",ipar);
      ivar = 0 ;
      for (iaexp=0;iaexp<naexp;iaexp++)
	for (iPk=0;iPk<nPkComp;iPk++)
	  {
	    PkDeriv = DiffPkVecScale[ipar][iaexp+3][iPk];
	    gsl_spline2d_set(sp_deriv1[ipar].PkDeriv_bispl, sp_deriv1[ipar].PkDerivData, iaexp, iPk, PkDeriv);
	    //printf("%3d  %3d  %+14.7e\n",iaexp,iPk,PkDeriv);
	  }
      
      // set the values of the logrk 
      for (iPk=0; iPk<nPkComp; iPk++)
	logrkVec[iPk]=log(rkVecComp[iPk]);
      
      // set the values of the logaexp 
      for (iaexp=0; iaexp<naexp; iaexp++)
	logaexp[iaexp]=log(atime[ivar0][iaexp+3]);
      
      // initialize interpolation //  
      gsl_spline2d_init(sp_deriv1[ipar].PkDeriv_bispl, logaexp,
			logrkVec, sp_deriv1[ipar].PkDerivData, naexp, nPkComp);

    }



#ifdef WRITEALLDATA

  // test the 2D splines
  nk=200;
  nsnapMAX = NSNAPMAX;
  nsnapMIN = NSNAPMIN;

  for (ipar=0;ipar<npar;ipar++)
    {
      
      ivar0=ipar*2+1;
      ivar1=0;
      ivar2=ipar*2+2;

      sprintf(outfile,"%s/PkDerivBSpline_par_%d.dat",All.OutputDir,ipar);
  
      if((fout = fopen(outfile,"w")))
	{

	  fprintf(fout,"%d\t%d\t%d\n",nsnapMIN,nsnapMAX,nk);
	  
	  for (isnap=nsnapMIN;isnap<=nsnapMAX;isnap++)
	    for (ik = 0; ik < nk; ++ik)
	      {

		aa=atime[ivar0][isnap];
		logaa=log(aa);
		
		log10rk=log10(rkVecComp[0]*1.01)+(log10(rkVecComp[nPkComp-1]/rkVecComp[0]))*0.98*ik/(nk-1.0);
		rk=pow(10.0,log10rk);
		
		logrk=log(rk);
		
		PkDeriv = gsl_spline2d_eval(sp_deriv1[ipar].PkDeriv_bispl, logaa, 
					    logrk, sp_deriv1[ipar].aexp_acc, sp_deriv1[ipar].rkVec_acc);
		
		fprintf(fout,"%+14.7e\t%+14.7e\n",rk,PkDeriv);
		
	      }
	}
      else
	{
	  printf("\noutfile not found:= %s.\n", outfile);
	  endrun(213);	    
	}
      fclose(fout);
    }

#endif

}

//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

// bicubic spline to the diagonal components for the Hessian 

void spline_spectra_derivatives2(void)
{

  FILE *fout;
  char outfile[180];

  int ipar, npar, naexp, iaexp, iPk, ivar;
  int isnap, nsnapMAX, nsnapMIN, ivar0, ivar1, ivar2;

  double PkDeriv2;

  double log10aa, aa, logaa, log10rk, rk, logrk;

  int ia, na, ik, nk;

  const gsl_interp2d_type *PkDeriv2_bicubic = gsl_interp2d_bicubic;
  
  // number of parameter
  npar=NVAR/2;
  
  // number of expansion factors
  naexp=NSNAPMAX-NSNAPMIN+1;

  // allocate memory for the npar 2D spline
  if( ! (sp_deriv2 = malloc(npar*sizeof(struct spline_deriv))))
    {
      printf("failed to allocate memory for `sp_deriv2'.\n" );
      endrun(121);
    }

  // allocate memory for the data associated with each spline
  for (ipar=0; ipar<npar;ipar++)
    {

      ivar0=ipar*2+1;
      ivar1=0;
      ivar2=ipar*2+2;

      if( ! (sp_deriv2[ipar].PkDerivData = malloc(naexp * nPkComp * sizeof(double))))  
	{
	  printf("failed to allocate memory for `sp_deriv2[ipar].PkDerivData'.\n" );
	  endrun(122);
	}
      
      // allocate the spline object
      sp_deriv2[ipar].PkDeriv_bispl = gsl_spline2d_alloc(PkDeriv2_bicubic, naexp, nPkComp);
      
      // accelleration for x-direction
      sp_deriv2[ipar].aexp_acc  = gsl_interp_accel_alloc();

      // accelleration for y-direction
      sp_deriv2[ipar].rkVec_acc = gsl_interp_accel_alloc();

      //printf("setting the bicubic spline grid values for ipar:= %d\n",ipar);
      ivar = 0 ;
      for (iaexp=0;iaexp<naexp;iaexp++)
	for (iPk=0;iPk<nPkComp;iPk++)
	  {
	    PkDeriv2 = Diff2PkVecScale[ipar][iaexp+3][iPk];
	    gsl_spline2d_set(sp_deriv2[ipar].PkDeriv_bispl, sp_deriv2[ipar].PkDerivData, iaexp, iPk, PkDeriv2);
	    //printf("%3d  %3d  %+14.7e\n",iaexp,iPk,PkDeriv);
	  }
      
      // set the values of the logrk 
      for (iPk=0; iPk<nPkComp; iPk++)
	logrkVec[iPk]=log(rkVecComp[iPk]);
      
      // set the values of the logaexp 
      for (iaexp=0; iaexp<naexp; iaexp++)
	logaexp[iaexp]=log(atime[ivar0][iaexp+3]);
      
      // initialize interpolation //  
      gsl_spline2d_init(sp_deriv2[ipar].PkDeriv_bispl, logaexp,
			logrkVec, sp_deriv2[ipar].PkDerivData, naexp, nPkComp);

    }

#ifdef WRITEALLDATA

  // test the 2D splines
  nk=200;
  nsnapMAX = NSNAPMAX;
  nsnapMIN = NSNAPMIN;

  for (ipar=0;ipar<npar;ipar++)
    {
      
      ivar0=ipar*2+1;
      ivar1=0;
      ivar2=ipar*2+2;

      sprintf(outfile,"%s/PkDeriv2BSpline_par_%d.dat",All.OutputDir,ipar);
      //printf("%s\n",outfile);
  
      if((fout = fopen(outfile,"w")))
	{

	  fprintf(fout,"%d\t%d\t%d\n",nsnapMIN,nsnapMAX,nk);
	  
	  for (isnap=nsnapMIN;isnap<=nsnapMAX;isnap++)
	    for (ik = 0; ik < nk; ++ik)
	      {

		aa=atime[ivar0][isnap];
		logaa=log(aa);
		
		log10rk=log10(rkVecComp[0]*1.01)+(log10(rkVecComp[nPkComp-1]/rkVecComp[0]))*0.98*ik/(nk-1.0);
		rk=pow(10.0,log10rk);
		
		logrk=log(rk);
		
		PkDeriv2 = gsl_spline2d_eval(sp_deriv2[ipar].PkDeriv_bispl, logaa, 
					    logrk, sp_deriv2[ipar].aexp_acc, sp_deriv2[ipar].rkVec_acc);
		
		fprintf(fout,"%+14.7e\t%+14.7e\n",rk,PkDeriv2);
		
	      }
	}
      else
	{
	  printf("\noutfile not found:= %s.\n", outfile);
	  endrun(213);	    
	}
      fclose(fout);
    }

#endif

}


//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

void free_PkFidSpline(void)
{
  gsl_spline2d_free(PkFid_bispl);
  gsl_interp_accel_free(aexp_acc);
  gsl_interp_accel_free(rkVec_acc);
  free(PkFidData);
}

//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

void free_PkFidSpline_SuperComp(void)
{
  gsl_spline2d_free(PkFid_bispl_SuperComp);
  gsl_interp_accel_free(aexp_acc_SuperComp);
  gsl_interp_accel_free(rkVec_acc_SuperComp);
  free(PkFidData_SuperComp);
}

//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

void free_PkDerivSpline(void)
{

  int ipar, npar;
  npar=NVAR/2;
  
  for (ipar=0;ipar<npar;ipar++)
    {
      gsl_spline2d_free(sp_deriv1[ipar].PkDeriv_bispl);
      gsl_interp_accel_free(sp_deriv1[ipar].aexp_acc);
      gsl_interp_accel_free(sp_deriv1[ipar].rkVec_acc);
      free(sp_deriv1[ipar].PkDerivData);
    }
  free(sp_deriv1);

}

//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

void free_PkDeriv2Spline(void)
{

  int ipar, npar;
  npar=NVAR/2;
  
  for (ipar=0;ipar<npar;ipar++)
    {
      gsl_spline2d_free(sp_deriv2[ipar].PkDeriv_bispl);
      gsl_interp_accel_free(sp_deriv2[ipar].aexp_acc);
      gsl_interp_accel_free(sp_deriv2[ipar].rkVec_acc);
      free(sp_deriv2[ipar].PkDerivData);
    }
  free(sp_deriv2);

}

//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


