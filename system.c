#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <time.h>
#include <sys/time.h>
#include <sys/resource.h>
#include <sys/types.h>
#include <unistd.h>
#include <signal.h>
#include <gsl/gsl_rng.h>

#include "allvars.h"
#include "proto.h"


/*! \file system.c
 *  \brief contains miscellaneous routines, e.g. elapsed time measurements
 */

//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

/*! returns the time difference between two measurements obtained with
 *  second(). The routine takes care of the possible overflow of the tick
 *  counter on 32bit systems, but depending on the system, this may not always
 *  work properly. 
 */
double timediff(clock_t t0, clock_t t1)
{
  double dt;

  dt = (double)(t1 - t0)/CLOCKS_PER_SEC;

  return dt;
}

//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

// This function aborts the program with messages.

void endrun(int ierr)
{
  if(ierr)
    {
      printf("endrun called with an error level of %d\n\n\n", ierr);
      fflush(stdout);
      exit(0);
    }
}

//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

